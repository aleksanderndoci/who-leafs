/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.MissingPeriodException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.AnnualLeavePeriod;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import com.demo.wholeafs.core.repository.AnnualLeavePeriodRepository;
import com.demo.wholeafs.core.service.AnnualLeavePeriodService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class AnnualLeaveServiceImpl extends CommonCrudServiceImpl<AnnualLeavePeriodModel, AnnualLeavePeriod>
        implements AnnualLeavePeriodService {

    private final AnnualLeavePeriodRepository repository;
    private Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> mapper;

    public AnnualLeaveServiceImpl(AnnualLeavePeriodRepository repository,
                                  Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> mapper) {
        super(repository, mapper);
        this.repository = repository;
        this.mapper = mapper;
    }


    @Override
    public AnnualLeavePeriodModel getActualPeriod() {
        return repository.getActualLeavePeriod()
                .map(mapper::toModel)
                .orElseThrow(MissingPeriodException::new);
    }

    @Override
    public AnnualLeavePeriodModel createPeriod(AnnualLeavePeriodModel periodModel) {
        log.info("Creating AnnualLeavePeriod: {}", periodModel);
        return super.save(periodModel);
    }
}
