/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityDateUser;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "leave_approver")
@SQLDelete(sql = "UPDATE leave_approver SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
public class LeaveApprover extends CommonEntityDateUser {
    @ManyToOne
    @JoinColumn(name = "approver_id",
            nullable = false)
    private User approver;

    @ManyToOne
    @JoinColumn(name = "leave_id",
            nullable = false)
    private Leave leave;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private LeaveStatusEnum status;

    @Column(name = "reason")
    private String reason;
}
