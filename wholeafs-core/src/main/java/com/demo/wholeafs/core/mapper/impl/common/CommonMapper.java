/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl.common;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.common.CommonEntityData;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;

public abstract class CommonMapper<M extends CommonModel, E extends CommonEntityData> implements Mapper<M, E> {

    @Override
    public M toModel(E entity) {
        if (entity == null) return null;
        M model = this.mapUniqueFields(entity);
        model.setGuid(entity.getGuid());
        model.setId(entity.getId());
        model.setInsertedAt(entity.getInsertedAt());
        model.setLastUpdated(entity.getLastUpdated());
        return model;
    }

    @Override
    public E toEntity(M model) {
        if (model == null) return null;
        // No common fields
        // (id, date_created, last_updated, isDeleted
        // are managed by entities)
        E entity = this.mapUniqueFields(model);
        entity.setId(model.getId());
        entity.setGuid(model.getGuid());
        return entity;
    }

    protected abstract M mapUniqueFields(E entity);

    protected abstract E mapUniqueFields(M model);
}
