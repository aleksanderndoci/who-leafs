/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.user.password;

import lombok.Data;

@Data
public class ResetPasswordDto {
    private String email;
    private String baseUrl;
    private String password;
    private Integer resetPin;
}
