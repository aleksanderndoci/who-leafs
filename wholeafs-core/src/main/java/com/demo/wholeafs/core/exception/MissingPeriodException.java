/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBusinessException;

public class MissingPeriodException extends WhoLeafsBusinessException {

    public static final String ERROR_CODE = "error.missingPeriod";

    public MissingPeriodException() {
        this("No period found");
    }

    public MissingPeriodException(String s, Object... params) {
        super(ERROR_CODE, ErrorType.BUSINESS, s, params);
    }
}
