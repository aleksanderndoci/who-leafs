/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository.util;

import com.demo.wholeafs.core.model.entity.*;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LeaveSpecificationBuilder {
    public static Specification<Leave> betweenDates(Date fromDate, Date toDate) {
        return ((root, query, cb) -> {
            List<Predicate> whereConditions = new ArrayList<>();
            whereConditions.add(cb.greaterThanOrEqualTo(root.get(Leave_.endDate), fromDate));
            whereConditions.add(cb.greaterThanOrEqualTo(root.get(Leave_.endDate), toDate));
            return cb.or(whereConditions.toArray(new Predicate[0]));
        });
    }

    public static Specification<Leave> statusIn(List<LeaveStatusEnum> status) {
        return ((root, query, cb) -> root.get(Leave_.leaveStatus).in(status));
    }

    public static Specification<Leave> from(LeaveSearchDto dto) {
        return ((root, query, cb) -> {
            List<Predicate> whereConditions = new ArrayList<>();
            if (dto.getStartDate() != null) {
                whereConditions.add(cb.greaterThanOrEqualTo(root.get(Leave_.startDate), dto.getStartDate()));
            }
            if (dto.getEndDate() != null) {
                whereConditions.add(cb.lessThanOrEqualTo(root.get(Leave_.startDate), dto.getEndDate()));
            }
            if (dto.getLeaveStatus() != null) {
                whereConditions.add(root.get(Leave_.leaveStatus).in(dto.getLeaveStatus()));
            }
            if (dto.getLeaveType() != null) {
                whereConditions.add(root.get(Leave_.leaveType).in(dto.getLeaveType()));
            }
            if (dto.getUserGuid() != null) {
                whereConditions.add(cb.equal(root.get(Leave_.userLeavePeriod)
                        .get(UserLeavePeriod_.user)
                        .get(User_.guid), dto.getUserGuid()));
            }
            if (dto.getPeriodGuid() != null) {
                whereConditions.add(cb.equal(root.get(Leave_.userLeavePeriod)
                        .get(UserLeavePeriod_.period)
                        .get(AnnualLeavePeriod_.guid), dto.getPeriodGuid()));
            }
            return cb.and(whereConditions.toArray(new Predicate[0]));
        });
    }
}
