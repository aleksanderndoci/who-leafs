/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(callSuper = true)
public class LeaveModel extends CommonModel {
    private Date startDate;

    private Date endDate;

    private BigDecimal hours;

    private LeaveTypeEnum leaveType;

    private LeaveStatusEnum leaveStatus;

    private String reason;

    private UserLeavePeriodModel userLeavePeriod;
}
