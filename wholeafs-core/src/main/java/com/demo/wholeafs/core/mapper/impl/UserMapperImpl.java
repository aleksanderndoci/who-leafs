/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.Role;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl extends CommonMapper<UserModel, User> {

    private final Mapper<RoleModel, Role> roleMapper;

    @Autowired
    public UserMapperImpl(Mapper<RoleModel, Role> roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Override
    protected UserModel mapUniqueFields(User entity) {
        if (entity == null) return null;
        UserModel model = new UserModel();
        model.setName(entity.getName());
        model.setSurname(entity.getSurname());
        model.setEmail(entity.getEmail());
        model.setUsername(entity.getUsername());
        model.setId(entity.getId());
        model.setInsertedAt(entity.getInsertedAt());
        model.setLastUpdated(entity.getLastUpdated());
        model.setAccountStatus(entity.getAccountStatus());
        model.setPassword(entity.getPassword());
        model.setRole(roleMapper.toModel(entity.getRole()));
        return model;
    }

    @Override
    protected User mapUniqueFields(UserModel model) {
        if (model == null) return null;
        User entity = new User();
        entity.setName(model.getName());
        entity.setSurname(model.getSurname());
        entity.setEmail(model.getEmail());
        entity.setUsername(model.getUsername());
        entity.setPassword(model.getPassword());
        entity.setId(model.getId());
        entity.setAccountStatus(model.getAccountStatus());
        entity.setRole(roleMapper.toEntity(model.getRole()));
        return entity;
    }
}
