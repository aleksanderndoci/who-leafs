/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl.util;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.*;
import com.demo.wholeafs.core.model.pojo.*;
import com.demo.wholeafs.core.model.pojo.user.UserModel;

public interface MapperFactory {
    Mapper<UserModel, User> userMapper();

    Mapper<LeaveModel, Leave> leaveMapper();

    Mapper<UserLeavePeriodModel, UserLeavePeriod> userLeavePeriodMapper();

    Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper();

    Mapper<RoleModel, Role> roleMapper();

    Mapper<LeaveApproverModel, LeaveApprover> leaveApproverMapper();

    Mapper<EmailOperationModel, EmailOperation> emailOperationMapper();
}
