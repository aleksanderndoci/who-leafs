/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.EmailOperation;
import com.demo.wholeafs.core.model.pojo.EmailOperationModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.service.common.CommonCrudService;

public interface EmailOperationService extends CommonCrudService<EmailOperationModel, EmailOperation> {
    void emailConfirmationRequest(UserModel userModel, String baseUrl);

    String passwordResetRequest(UserModel userModel, String baseUrl);
}
