/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.Role;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import com.demo.wholeafs.core.repository.RoleRepository;
import com.demo.wholeafs.core.service.RoleService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleServiceImpl extends CommonCrudServiceImpl<RoleModel, Role> implements RoleService {

    private final RoleRepository roleRepository;
    private final Mapper<RoleModel, Role> roleMapper;

    public RoleServiceImpl(RoleRepository roleRepository, Mapper<RoleModel, Role> roleMapper) {
        super(roleRepository, roleMapper);
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
    }

    @Override
    public RoleModel findByRole(RoleEnum role) {
        return roleMapper.toModel(roleRepository.findByRole(role));
    }
}
