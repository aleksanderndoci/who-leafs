/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

import lombok.Getter;

@Getter
public enum LeaveTypeEnum {
    SICK_LEAVE_PAYED        (Category.DAILY),
    NOT_PAYED_LEAVE         (Category.ANY),
    MATERNITY               (Category.DAILY),
    PARENTAL_LEAVE          (Category.DAILY),
    EDUCATIONAL_OR_STUDY    (Category.ANY),
    DEPENDENT_CARE          (Category.ANY),
    HOURLY_LEAVE            (Category.HOURLY),
    DAILY_LEAVE             (Category.DAILY),
    HOLIDAY_LEAVE           (Category.DAILY);

    private Category category;

    LeaveTypeEnum(Category category) {
        this.category = category;
    }

    enum Category {
        HOURLY,
        DAILY,
        ANY
    }
}
