/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalField;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {
    public static long findDaysBetween(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            throw new IllegalArgumentException("Date cannot be null at this point");
        }
        LocalDate firstDate = toLocalDate(d1);
        LocalDate otherDate = toLocalDate(d2);
        return Duration.between(firstDate.atStartOfDay(), otherDate.atStartOfDay())
                .toDays();
    }

    public static Double findHoursBetween(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            throw new IllegalArgumentException("Date cannot be null at this point");
        }
        long diffInMillis = d1.getTime() - d2.getTime();
        BigDecimal hours = BigDecimal.valueOf(diffInMillis)
                .divide(BigDecimal.valueOf(1000 * 60 * 60), 2, RoundingMode.HALF_UP);
        return hours.doubleValue();
    }

    public static LocalDate toLocalDate(Date d) {
        return d == null ? null : d.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Date d) {
        return d == null ? null : d.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static int getFromDate(Date date, TemporalField temporalField) {
        LocalDateTime ld = toLocalDateTime(date);
        return ld.get(temporalField);
    }
}
