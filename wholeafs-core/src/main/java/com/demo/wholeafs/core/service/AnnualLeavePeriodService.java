/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.AnnualLeavePeriod;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import com.demo.wholeafs.core.service.common.CommonCrudService;

public interface AnnualLeavePeriodService extends CommonCrudService<AnnualLeavePeriodModel, AnnualLeavePeriod> {
    AnnualLeavePeriodModel getActualPeriod();

    AnnualLeavePeriodModel createPeriod(AnnualLeavePeriodModel periodModel);
}
