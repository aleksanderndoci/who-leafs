/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBusinessException;

public class MissingPageException extends WhoLeafsBusinessException {
    private static final String ERROR_CODE = "error.missingPage";

    public MissingPageException() {
        this("Page was null");
    }

    public MissingPageException(String s, Object... params) {
        super(s, ErrorType.INVALID_REQUEST, s, params);
    }
}
