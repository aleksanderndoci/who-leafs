/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBaseRuntimeException;

import static com.demo.wholeafs.core.exception.common.ErrorType.UNEXPECTED;

public class EmailNotFoundException extends WhoLeafsBaseRuntimeException {
    private static String GENERIC_ERROR = "error.emailNotFound";

    public EmailNotFoundException(String s, Object... params) {
        super(GENERIC_ERROR, UNEXPECTED, s, params);
    }

    public EmailNotFoundException(String errorCode, ErrorType errorType, String s, Object... params) {
        super(errorCode, errorType, s, params);
    }
}
