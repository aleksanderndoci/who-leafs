/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.projection.UserRoleProjection;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CommonCrudRepository<User> {
    Optional<User> findByUsernameOrEmail(String username, String email);

    @Query("SELECT COUNT(u.id) as count, r.role as role " +
            "FROM User u " +
            "INNER JOIN Role r ON u.role.id =r.id " +
            "GROUP BY (r.role)")
    List<UserRoleProjection> getUsersGrouppedByRole();
}
