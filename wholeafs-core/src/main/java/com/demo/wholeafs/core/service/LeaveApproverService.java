/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.LeaveApprover;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.view.LeaveApproverView;
import com.demo.wholeafs.core.model.view.LeaveDetailedApproverView;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.service.common.CommonCrudService;

import java.util.List;

public interface LeaveApproverService extends CommonCrudService<LeaveApproverModel, LeaveApprover> {
    List<LeaveApproverModel> addApprovers(List<String> userGuids, LeaveModel leaveModel);

    PageableResponse<LeaveApproverView> getAvailableApprovers(UserSearchDto userSearchDto);

    List<LeaveApproverStatusProjection> getApproversStatus(String leaveGuid, String excludedApproverGuid);

    PageableResponse<LeaveDetailedApproverView> searchLeaveApprover(LeaveSearchDto leaveSearchDto);
}
