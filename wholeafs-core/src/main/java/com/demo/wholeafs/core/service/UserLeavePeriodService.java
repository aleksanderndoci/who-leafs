/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.UserLeavePeriod;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.service.common.CommonCrudService;

public interface UserLeavePeriodService extends CommonCrudService<UserLeavePeriodModel, UserLeavePeriod> {
    UserLeavePeriodModel getCurrentPeriod(Integer userId);
}
