/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request;

import lombok.Data;

import java.util.Date;

@Data
public class PeriodDto {
    private Date from;
    private Date to;
}
