/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.projection;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;

public interface LeaveApproverStatusProjection {
    LeaveStatusEnum getStatus();
}
