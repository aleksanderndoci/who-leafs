/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.AnnualLeavePeriod;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.entity.UserLeavePeriod;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class UserLeavePeriodMapper extends CommonMapper<UserLeavePeriodModel, UserLeavePeriod> {

    private final Mapper<UserModel, User> userMapper;
    private final Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper;

    public UserLeavePeriodMapper(Mapper<UserModel, User> userMapper,
                                 Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper) {
        this.userMapper = userMapper;
        this.annualLeavePeriodMapper = annualLeavePeriodMapper;
    }

    @Override
    protected UserLeavePeriodModel mapUniqueFields(UserLeavePeriod entity) {
        UserLeavePeriodModel model = new UserLeavePeriodModel();
        BeanUtils.copyProperties(entity, model);
        return model;
    }

    @Override
    protected UserLeavePeriod mapUniqueFields(UserLeavePeriodModel model) {
        UserLeavePeriod entity = new UserLeavePeriod();
        BeanUtils.copyProperties(model, entity);
        entity.setUser(userMapper.toEntity(model.getUser()));
        entity.setPeriod(annualLeavePeriodMapper.toEntity(model.getPeriod()));
        return entity;
    }
}
