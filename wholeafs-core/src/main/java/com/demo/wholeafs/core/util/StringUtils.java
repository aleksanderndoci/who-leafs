/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {
    public static String concatenateWithSeparator(String separator, String... values) {
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            sb.append(value);
            if (separator != null) {
                sb.append(separator);
            }
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static String concatenate(String... values) {
        return concatenateWithSeparator(null, values);
    }

    public static Map<String, Object> matrixToMap(String[]... matrix) {
        return Stream.of(matrix)
                .collect(Collectors.toMap(data -> data[0], data -> data[1]));
    }
}
