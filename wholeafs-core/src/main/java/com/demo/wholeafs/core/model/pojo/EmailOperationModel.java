/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.enums.EmailRequestStatusEnum;
import com.demo.wholeafs.core.model.enums.EmailRequestType;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailOperationModel extends CommonModel {
    private UserModel user;

    private EmailRequestType requestType;

    private EmailRequestStatusEnum requestStatus;

    private Integer resetPin;
}
