/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityData;
import com.demo.wholeafs.core.model.enums.EmailRequestStatusEnum;
import com.demo.wholeafs.core.model.enums.EmailRequestType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "user_email_request")
@Data
@EqualsAndHashCode(callSuper = false)
@SQLDelete(sql = "UPDATE user_email_request SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
public class EmailOperation extends CommonEntityData {

    @ManyToOne
    @JoinColumn(name = "user_id",
            nullable = false)
    private User user;

    @Column(name = "request_type",
            nullable = false)
    @Enumerated(EnumType.STRING)
    private EmailRequestType requestType;

    @Column(name = "request_status",
            nullable = false)
    @Enumerated(EnumType.STRING)
    private EmailRequestStatusEnum requestStatus;

    @Column(name = "reset_pin")
    private Integer resetPin;
}
