/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.user.password;

import lombok.Data;

@Data
public class ChangePasswordDto {
    private String password;
    private String newPassword;
}
