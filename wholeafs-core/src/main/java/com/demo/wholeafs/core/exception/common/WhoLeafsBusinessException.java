/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception.common;

public class WhoLeafsBusinessException extends WhoLeafsBaseRuntimeException {
    private static final String GENERIC_ERROR = "error.businessLogic";

    public WhoLeafsBusinessException(String s, Object... params) {
        super(GENERIC_ERROR, ErrorType.BUSINESS, s, params);
    }

    public WhoLeafsBusinessException(String errorCode, ErrorType errorType, String s, Object... params) {
        super(errorCode, errorType, s, params);
    }
}
