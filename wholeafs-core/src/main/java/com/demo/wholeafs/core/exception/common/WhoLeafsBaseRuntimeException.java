/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception.common;

import lombok.Getter;

public abstract class WhoLeafsBaseRuntimeException extends RuntimeException {

    @Getter
    private String errorCode;

    @Getter
    private ErrorType errorType;

    public WhoLeafsBaseRuntimeException(String errorCode, String s, Object... params) {
        super(String.format(s != null ? s.replace("{}", "%s") : "", params));
        this.errorType = ErrorType.UNSPECIFIED;
        this.errorCode = errorCode;
    }

    public WhoLeafsBaseRuntimeException(String errorCode, ErrorType errorType, String s, Object... params) {
        this(errorCode, s, params);
        this.errorType = errorType;
    }


    public WhoLeafsBaseRuntimeException(String s, Throwable e) {
        super(s, e);
    }
}
