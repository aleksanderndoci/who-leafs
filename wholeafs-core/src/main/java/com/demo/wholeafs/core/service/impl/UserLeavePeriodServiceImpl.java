/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.exception.MissingPeriodException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.UserLeavePeriod;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.repository.AnnualLeavePeriodRepository;
import com.demo.wholeafs.core.repository.UserLeavePeriodRepository;
import com.demo.wholeafs.core.service.UserLeavePeriodService;
import com.demo.wholeafs.core.service.UserService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class UserLeavePeriodServiceImpl extends CommonCrudServiceImpl<UserLeavePeriodModel, UserLeavePeriod>
        implements UserLeavePeriodService {


    private final UserLeavePeriodRepository repository;
    private final Mapper<UserLeavePeriodModel, UserLeavePeriod> mapper;
    private final AnnualLeavePeriodRepository annualLeavePeriodRepository;
    private final UserService userService;

    public UserLeavePeriodServiceImpl(UserLeavePeriodRepository repository,
                                      Mapper<UserLeavePeriodModel, UserLeavePeriod> mapper,
                                      AnnualLeavePeriodRepository annualLeavePeriodRepository,
                                      UserService userService) {
        super(repository, mapper);
        this.repository = repository;
        this.mapper = mapper;
        this.annualLeavePeriodRepository = annualLeavePeriodRepository;
        this.userService = userService;
    }

    @Override
    public UserLeavePeriodModel getCurrentPeriod(Integer userId) {
        log.info("Fetching UserLeavePeriod for user: {}", userId);
        return repository.getCurrentPeriod(userId)
                .map(userLeavePeriod -> mapper.toModel(userLeavePeriod,
                        (model, entity) -> {
                            model.setPeriod(mapperFactory.annualLeavePeriodMapper().toModel(entity.getPeriod()));
                            model.setUser(mapperFactory.userMapper().toModel(userLeavePeriod.getUser()));
                        })
                ).orElseGet(() -> this.createUserLeavePeriod(userId));
    }

    private UserLeavePeriodModel createUserLeavePeriod(Integer userId) {
        log.info("Creating UserLeavePeriod for user: {}", userId);
        AnnualLeavePeriodModel annualLeavePeriod = annualLeavePeriodRepository.getActualLeavePeriod()
                .map(period -> mapperFactory.annualLeavePeriodMapper().toModel(period))
                .orElseThrow(() -> new MissingPeriodException("No active AnnualPeriod found"));
        UserLeavePeriodModel userLeavePeriodModel = new UserLeavePeriodModel();
        userLeavePeriodModel.setPeriod(annualLeavePeriod);
        UserModel userModel = userService.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User ID: {} not found", userId));
        userLeavePeriodModel.setUser(userModel);
        userLeavePeriodModel.setNotes("Generated automatically by system.");
        userLeavePeriodModel.setRemainingHours(annualLeavePeriod.getDefaultTotalHours());
        userLeavePeriodModel.setTotalHours(annualLeavePeriod.getDefaultTotalHours());
        super.save(userLeavePeriodModel);
        userLeavePeriodModel.setUser(userModel);
        userLeavePeriodModel.setPeriod(annualLeavePeriod);
        return userLeavePeriodModel;
    }
}
