/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository.util;

import com.demo.wholeafs.core.model.entity.Role_;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.entity.User_;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserSpecificationBuilder {

    public static Specification<User> searchUserSpec(UserSearchDto userSearchDto) {
        return ((root, query, cb) -> {
            List<Predicate> whereConditions = new ArrayList<>();
            if (userSearchDto.getFullName() != null) {
                Expression<String> fullNameExp = cb.concat(root.get(User_.name), " ");
                fullNameExp = cb.concat(fullNameExp, root.get(User_.surname));
                fullNameExp = cb.lower(fullNameExp);
                whereConditions.add(cb.like(fullNameExp, '%' + userSearchDto.getFullName().toLowerCase() + '%'));
            }
            if (userSearchDto.getAccountStatus() != null) {
                whereConditions.add(cb.equal(root.get(User_.accountStatus), userSearchDto.getAccountStatus()));
            }
            if (userSearchDto.getEmail() != null) {
                whereConditions.add(cb.equal(root.get(User_.email), userSearchDto.getEmail()));
            }
            if (userSearchDto.getUsername() != null) {
                Expression<String> userNameExp = cb.lower(root.get(User_.username));
                whereConditions.add(cb.like(userNameExp, '%' + userSearchDto.getUsername().toLowerCase() + '%'));
            }
            if (userSearchDto.getRole() != null) {
                whereConditions.add(cb.equal(root.get(User_.role)
                        .get(Role_.role), userSearchDto.getRole()));
            }
            return cb.and(whereConditions.toArray(new Predicate[0]));
        });
    }
}
