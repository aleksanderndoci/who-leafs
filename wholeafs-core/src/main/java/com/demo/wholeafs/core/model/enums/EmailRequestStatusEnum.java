/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

public enum EmailRequestStatusEnum {
    SENT,
    CONFIRMED,
    CANCELED
}
