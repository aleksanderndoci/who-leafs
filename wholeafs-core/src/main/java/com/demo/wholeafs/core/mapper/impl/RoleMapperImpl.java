/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.Role;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import org.springframework.stereotype.Component;

@Component
public class RoleMapperImpl extends CommonMapper<RoleModel, Role> {

    @Override
    protected RoleModel mapUniqueFields(Role entity) {
        RoleModel roleModel = new RoleModel();
        roleModel.setRole(entity.getRole());
        roleModel.setId(entity.getId());
        return roleModel;
    }

    @Override
    protected Role mapUniqueFields(RoleModel model) {
        Role role = new Role();
        role.setRole(model.getRole());
        role.setId(model.getId());
        return role;
    }
}
