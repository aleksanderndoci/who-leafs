/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper;

import com.demo.wholeafs.core.util.func.BiUpdater;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.util.CollectionUtils.isEmpty;

public interface Mapper<M, E> {
    M toModel(E entity);

    E toEntity(M model);

    default E toEntity(M model, BiUpdater<M, E> customMappings) {
        E entity = this.toEntity(model);
        customMappings.apply(model, entity);
        return entity;
    }

    default M toModel(E entity, BiUpdater<M, E> customMappings) {
        M model = this.toModel(entity);
        customMappings.apply(model, entity);
        return model;
    }

    default List<E> toEntityList(List<M> modelList) {
        if (isEmpty(modelList)) {
            return Collections.emptyList();
        }
        return modelList.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    default List<M> toModelList(List<E> entityList) {
        if (isEmpty(entityList)) {
            return Collections.emptyList();
        }
        return entityList.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }

    default List<M> toModelList(Iterable<E> iterable) {
        if (iterable == null) {
            return Collections.emptyList();
        }
        return StreamSupport
                .stream(iterable.spliterator(), false)
                .map(this::toModel)
                .collect(Collectors.toList());
    }
}
