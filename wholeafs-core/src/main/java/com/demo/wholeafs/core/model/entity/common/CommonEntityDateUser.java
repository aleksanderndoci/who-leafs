/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity.common;

import com.demo.wholeafs.core.model.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
public class CommonEntityDateUser extends CommonEntityData {
    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "updated_by")
    private User updatedBy;
}
