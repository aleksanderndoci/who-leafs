/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository.util;

import com.demo.wholeafs.core.model.entity.*;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LeaveApproverSpecificationBuilder {

    public static Specification<LeaveApprover> from(LeaveSearchDto dto) {
        return ((root, query, cb) -> {
            List<Predicate> whereConditions = new ArrayList<>();
            if (dto.getStartDate() != null) {
                whereConditions.add(cb.greaterThanOrEqualTo(root.get(LeaveApprover_.leave)
                        .get(Leave_.startDate), dto.getStartDate()));
            }
            if (dto.getEndDate() != null) {
                whereConditions.add(cb.lessThanOrEqualTo(root.get(LeaveApprover_.leave)
                        .get(Leave_.startDate), dto.getEndDate()));
            }
            if (dto.getLeaveStatus() != null) {
                whereConditions.add(root.get(LeaveApprover_.leave)
                        .get(Leave_.leaveStatus).in(dto.getLeaveStatus()));
            }
            if (dto.getLeaveType() != null) {
                whereConditions.add(root.get(LeaveApprover_.leave)
                        .get(Leave_.leaveType).in(dto.getLeaveType()));
            }
            if (dto.getUserGuid() != null) {
                whereConditions.add(cb.equal(root.get(LeaveApprover_.leave)
                        .get(Leave_.userLeavePeriod)
                        .get(UserLeavePeriod_.user)
                        .get(User_.guid), dto.getUserGuid()));
            }
            if (dto.getPeriodGuid() != null) {
                whereConditions.add(cb.equal(root.get(LeaveApprover_.leave)
                        .get(Leave_.userLeavePeriod)
                        .get(UserLeavePeriod_.period)
                        .get(AnnualLeavePeriod_.guid), dto.getPeriodGuid()));
            }
            if (dto.getApproverGuid() != null) {
                whereConditions.add(cb.equal(root.get(LeaveApprover_.approver)
                        .get(User_.guid), dto.getApproverGuid()));
            }
            return cb.and(whereConditions.toArray(new Predicate[0]));
        });
    }
}
