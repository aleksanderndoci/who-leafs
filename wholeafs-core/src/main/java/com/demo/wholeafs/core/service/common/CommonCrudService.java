/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.common;

import com.demo.wholeafs.core.util.func.Updater;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface CommonCrudService<M, E> {
    Optional<M> findById(Integer id);

    <T> Optional<T> findById(Integer id, Class<T> returnClass);

    Optional<M> findByGuid(String guid);

    <T> Optional<T> findByGuid(String guid, Class<T> returnClass);

    List<M> findAll();

    void deleteByGuid(String guid);

    void deleteById(Integer id);

    void delete(M model);

    M save(M model);

    List<M> saveAll(List<M> mList);

    void saveAsync(List<M> mList);

    List<M> saveAll(List<M> mList, Updater<M> updaterFunc);

    void saveAsync(List<M> mList, Updater<M> updaterFunc);
}
