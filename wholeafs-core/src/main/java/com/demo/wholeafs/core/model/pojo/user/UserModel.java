/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.user;

import com.demo.wholeafs.core.model.enums.AccountStatusEnum;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserModel extends CommonModel {
    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private AccountStatusEnum accountStatus;

    private RoleModel role;
}
