/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserLeavePeriodModel extends CommonModel {
    private BigDecimal totalHours;

    private BigDecimal remainingHours;

    private String notes;

    private UserModel user;

    private AnnualLeavePeriodModel period;
}
