/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.projection;

import com.demo.wholeafs.core.model.enums.RoleEnum;

public interface UserRoleProjection {
    RoleEnum getRole();

    long getCount();
}
