/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.user.password;

import lombok.Data;

@Data
public class PasswordResetResponse {
    private boolean wasReset;
    private String reason;
}
