/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LeaveStatusEnum {
    DRAFT           (0),
    SENT            (1),
    PENDING         (2),
    ACCEPTED        (3),
    CANCELLED       (3),
    REFUSED         (3);

    private int step;
}
