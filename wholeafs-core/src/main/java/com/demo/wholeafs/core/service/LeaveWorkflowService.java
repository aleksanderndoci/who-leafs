/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.model.request.LeaveStatusUpdateDto;

import java.util.List;

public interface LeaveWorkflowService {
    boolean validateLeaveApplication(Integer applierId, LeaveModel leaveModel);

    void validateCancellationRequest(LeaveModel leaveModel);

    void validateStatusChange(LeaveApproverModel leaveApproverModel, LeaveStatusUpdateDto dto,
                              List<LeaveApproverStatusProjection> otherApproversStatus);
}
