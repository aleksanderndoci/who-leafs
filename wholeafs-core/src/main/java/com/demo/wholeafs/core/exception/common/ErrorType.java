/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception.common;

public enum ErrorType {
    BUSINESS,
    UNSPECIFIED,
    UNEXPECTED,
    INVALID_REQUEST,
    FATAL
}
