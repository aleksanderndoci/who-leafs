/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;
import com.demo.wholeafs.core.model.request.common.CommonPageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class LeaveSearchDto extends CommonPageRequest {
    private Date startDate;

    private Date endDate;

    private List<LeaveTypeEnum> leaveType;

    private List<LeaveStatusEnum> leaveStatus;

    private String userGuid;

    private String periodGuid;

    private String approverGuid;
}
