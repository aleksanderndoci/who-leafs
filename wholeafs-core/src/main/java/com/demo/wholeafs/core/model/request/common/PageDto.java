/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request.common;

import lombok.Data;

@Data
public class PageDto {
    private Integer page;
    private Integer size;
}
