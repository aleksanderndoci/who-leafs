/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.projection;

public interface IdProjection {
    Integer getId();
}
