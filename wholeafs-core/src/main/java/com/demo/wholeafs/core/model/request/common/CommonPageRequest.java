/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request.common;

import lombok.Data;

@Data
public class CommonPageRequest {
    private PageDto page;
    private SortDto sort;
}
