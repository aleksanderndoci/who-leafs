/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RoleModel extends CommonModel {
    private RoleEnum role;
}
