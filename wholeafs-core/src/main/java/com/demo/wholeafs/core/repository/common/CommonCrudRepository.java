/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface CommonCrudRepository<T> extends JpaRepository<T, Integer>, JpaSpecificationExecutor<T> {
    <P> Optional<P> findById(Integer id, Class<P> returnClass);

    Optional<T> findByGuid(String guid);

    <P> Optional<P> findByGuid(String guid, Class<P> returnClass);

    void deleteByGuid(String guid);
}
