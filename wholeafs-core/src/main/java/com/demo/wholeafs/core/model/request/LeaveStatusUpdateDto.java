/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import lombok.Data;

@Data
public class LeaveStatusUpdateDto {
    private LeaveStatusEnum leaveStatus;
    private String reason;
}
