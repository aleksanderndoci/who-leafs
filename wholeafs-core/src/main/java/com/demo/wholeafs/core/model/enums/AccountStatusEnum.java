/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

import lombok.Getter;

@Getter
public enum AccountStatusEnum {
    ACTIVE,
    CANCELED,
    NOT_CONFIRMED
}
