/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.pojo.user.UserExistsModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.model.pojo.user.password.ChangePasswordDto;
import com.demo.wholeafs.core.model.pojo.user.password.ResetPasswordDto;
import com.demo.wholeafs.core.model.pojo.user.password.PasswordResetResponse;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.view.LeavePieChartView;
import com.demo.wholeafs.core.model.view.PieChartView;
import com.demo.wholeafs.core.service.common.CommonCrudService;

import java.util.List;
import java.util.function.Predicate;

public interface UserService extends CommonCrudService<UserModel, User> {
    UserExistsModel checkIfUserExists(String usernameOrEmail);

    UserModel registerUser(UserModel userModel);

    UserModel findByUsernameOrEmail(String usernameOrEmail);

    void sendConfirmationEmail(String email, String baseUrl);

    void confirmEmail(String emailConfirmationGuid);

    String sendPasswordResetRequest(ResetPasswordDto request);

    PasswordResetResponse changePassword(String passResetGuid, ResetPasswordDto request);

    PageableResponse<UserModel> searchUsers(UserSearchDto userSearchDto);

    PasswordResetResponse changePassword(Integer userId, ChangePasswordDto request, Predicate<UserModel> canChange);

    UserModel editAccount(UserModel userModel, boolean isAdmin);

    List<PieChartView> getUsersByRole();

    void deleteByGuid(String  guid, Integer deletedBy);

}
