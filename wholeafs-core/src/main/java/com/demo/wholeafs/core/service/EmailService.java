/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.pojo.EmailRequestModel;
import com.demo.wholeafs.core.util.func.Composer;
import org.springframework.mail.javamail.MimeMessageHelper;

public interface EmailService {
    void sendAsyncMimeEmail(EmailRequestModel emailRequestModel, Composer<MimeMessageHelper, Void> composer);
}
