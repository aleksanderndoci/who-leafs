/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

public enum RoleEnum {
    USER,
    SUPERVISOR,
    FINANCE,
    ADMIN
}
