/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class AnnualLeavePeriodModel extends CommonModel {
    private String periodTitle;

    private Date startDate;

    private Date endDate;

    private BigDecimal defaultTotalHours;
}
