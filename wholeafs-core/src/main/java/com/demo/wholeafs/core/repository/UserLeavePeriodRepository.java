/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.UserLeavePeriod;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserLeavePeriodRepository extends CommonCrudRepository<UserLeavePeriod> {

    @Query("SELECT l FROM UserLeavePeriod l " +
            "WHERE l.period.startDate<=CURRENT_DATE() " +
            "AND l.period.endDate>=CURRENT_DATE() " +
            "AND l.user.id=:userId")
    Optional<UserLeavePeriod> getCurrentPeriod(Integer userId);
}
