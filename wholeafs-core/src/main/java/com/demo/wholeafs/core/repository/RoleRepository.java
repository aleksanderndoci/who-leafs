/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.Role;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;

public interface RoleRepository extends CommonCrudRepository<Role> {
    Role findByRole(RoleEnum role);
}
