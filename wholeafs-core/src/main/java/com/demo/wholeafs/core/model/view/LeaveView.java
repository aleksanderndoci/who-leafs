/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.view;

import com.demo.wholeafs.core.model.pojo.LeaveModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class LeaveView extends LeaveModel {
    private String periodTitle;
    private List<LeaveApproverView> leaveApprovers;

    public LeaveView(LeaveModel leaveModel) {
        this();
        BeanUtils.copyProperties(leaveModel, this);
    }
}
