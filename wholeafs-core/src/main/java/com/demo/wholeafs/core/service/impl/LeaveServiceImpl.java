/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.exception.LeaveWorkflowException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.entity.Leave_;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.model.projection.LeaveTypeProjection;
import com.demo.wholeafs.core.model.request.LeaveRequestDto;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.request.LeaveStatusUpdateDto;
import com.demo.wholeafs.core.model.request.PeriodDto;
import com.demo.wholeafs.core.model.request.common.PageDto;
import com.demo.wholeafs.core.model.request.common.SortDto;
import com.demo.wholeafs.core.model.view.*;
import com.demo.wholeafs.core.repository.LeaveRepository;
import com.demo.wholeafs.core.service.LeaveApproverService;
import com.demo.wholeafs.core.service.LeaveService;
import com.demo.wholeafs.core.service.LeaveWorkflowService;
import com.demo.wholeafs.core.service.UserLeavePeriodService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.demo.wholeafs.core.repository.util.LeaveSpecificationBuilder.from;
import static com.demo.wholeafs.core.util.StringUtils.concatenateWithSeparator;
import static java.lang.String.format;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Slf4j
@Transactional
public class LeaveServiceImpl extends CommonCrudServiceImpl<LeaveModel, Leave> implements LeaveService {

    private final LeaveRepository leaveRepository;
    private final Mapper<LeaveModel, Leave> mapper;
    private final UserLeavePeriodService userLeavePeriodService;
    private final LeaveApproverService leaveApproverService;
    private final LeaveWorkflowService leaveWorkflowService;

    @Autowired
    public LeaveServiceImpl(LeaveRepository leaveRepository,
                            Mapper<LeaveModel, Leave> mapper,
                            UserLeavePeriodService userLeavePeriodService,
                            LeaveApproverService leaveApproverService,
                            LeaveWorkflowService leaveWorkflowService) {
        super(leaveRepository, mapper);
        this.leaveRepository = leaveRepository;
        this.mapper = mapper;
        this.userLeavePeriodService = userLeavePeriodService;
        this.leaveApproverService = leaveApproverService;
        this.leaveWorkflowService = leaveWorkflowService;
    }

    @Override
    public PageableResponse<LeaveModel> getUserLeave(Integer userId, PageDto pageDto) {
        log.info("Finding leave for user: {}. Request: {}", userId, pageDto);
        SortDto sortDto = new SortDto(Leave_.INSERTED_AT, Sort.Direction.DESC);
        Page<Leave> leavePage = leaveRepository.findAllByUserLeavePeriodUserId(userId, pageRequest(pageDto, sortDto));
        return super.toCustomPageable(leavePage, leave -> {
            LeaveView leaveView = new LeaveView(mapper.toModel(leave));
            leaveView.setPeriodTitle(leave.getUserLeavePeriod().getPeriod().getPeriodTitle());
            leaveView.setLeaveApprovers(leave.getLeaveApprovers().stream()
                    .map(leaveApprover -> {
                        LeaveApproverView leaveApproverView = new LeaveApproverView();
                        leaveApproverView.setName(leaveApprover.getApprover().getName());
                        leaveApproverView.setSurname(leaveApprover.getApprover().getSurname());
                        leaveApproverView.setStatus(leaveApprover.getStatus());
                        leaveApproverView.setReason(leaveApprover.getReason());
                        return leaveApproverView;
                    }).collect(Collectors.toList()));
            return leaveView;
        });
    }

    @Override
    public LeavePieChartView getLeavePieChart(Integer userId) {
        UserLeavePeriodModel annualLeavePeriodModel = userLeavePeriodService.getCurrentPeriod(userId);
        LeavePieChartView leavePieChartView = new LeavePieChartView();
        leavePieChartView.setPeriodTitle(annualLeavePeriodModel.getPeriod().getPeriodTitle());
        List<PieChartView> values = leaveRepository.getUserLeaveStatus(annualLeavePeriodModel.getId())
                .stream()
                .map(status -> {
                    PieChartView pieChartView = new PieChartView();
                    pieChartView.setKey(status.getStatus().name());
                    pieChartView.setValue(status.getHours());
                    return pieChartView;
                })
                .collect(Collectors.toList());
        PieChartView availableHours = new PieChartView();
        availableHours.setKey("AVAILABLE");
        availableHours.setValue(annualLeavePeriodModel.getRemainingHours().doubleValue());
        leavePieChartView.setValues(values);
        values.add(availableHours);
        return leavePieChartView;
    }

    @Override
    public LeaveModel applyForLeave(Integer userId, LeaveRequestDto requestDto) {
        log.info("Applying for new leave. User: {}, request: {}", userId, requestDto);
        LeaveModel leaveModel = new LeaveModel();
        leaveModel.setLeaveType(requestDto.getLeaveType());
        leaveModel.setStartDate(requestDto.getStartDate());
        leaveModel.setEndDate(requestDto.getEndDate());
        leaveModel.setReason(requestDto.getReason());
        leaveModel.setLeaveStatus(LeaveStatusEnum.SENT);
        if (leaveWorkflowService.validateLeaveApplication(userId, leaveModel)) {
            super.save(leaveModel);
            log.info("Leave: {} created. Adding approvers...", leaveModel.getGuid());
            leaveApproverService.addApprovers(requestDto.getApproverGuids(), leaveModel);
            return leaveModel;
        }
        throw new LeaveWorkflowException("Couldn't apply for leave - Rejected by system");
    }

    @Override
    public LeaveModel cancelLeave(String leaveGuid) {
        LeaveModel leaveModel = super.findByGuid(leaveGuid)
                .orElseThrow(() -> new EntityNotFoundException("Leave not found"));
        leaveWorkflowService.validateCancellationRequest(leaveModel);
        leaveModel.setLeaveStatus(LeaveStatusEnum.CANCELLED);
        super.save(leaveModel);
        return leaveModel;
    }

    @Override
    public LeaveApproverModel changeStatus(String leaveApproverGuid, LeaveStatusUpdateDto dto) {
        log.info("Changing status for leave: {}, request:{}", leaveApproverGuid, dto);
        LeaveApproverModel leaveApproverModel = leaveApproverService.findByGuid(leaveApproverGuid)
                .orElseThrow(() -> new EntityNotFoundException("LeaveApprover not found"));
        List<LeaveApproverStatusProjection> otherApprovers = leaveApproverService
                .getApproversStatus(leaveApproverModel.getLeave().getGuid(), leaveApproverGuid);
        leaveWorkflowService.validateStatusChange(leaveApproverModel, dto, otherApprovers);
        super.save(leaveApproverModel.getLeave());
        leaveApproverService.save(leaveApproverModel);

        // Hide this data from response
        leaveApproverModel.setApprover(null);
        leaveApproverModel.setLeave(null);
        return leaveApproverModel;
    }

    @Override
    public PageableResponse<LeaveDetailedApplierView> searchLeaveApplier(LeaveSearchDto leaveSearchDto) {
        Page<Leave> leavePage = leaveRepository.findAll(from(leaveSearchDto), pageRequest(leaveSearchDto));
        return super.toCustomPageable(leavePage, leave -> {
            LeaveDetailedApplierView view = new LeaveDetailedApplierView();
            view.setLeave(leave);
            view.setOverallStatus(leave.getLeaveStatus());
            view.setPeriodTitle(leave.getUserLeavePeriod().getPeriod().getPeriodTitle());
            String name = leave.getUserLeavePeriod().getUser().getName();
            String surname = leave.getUserLeavePeriod().getUser().getSurname();
            view.setFullName(concatenateWithSeparator(" ", name, surname));
            return view;
        });
    }

    @Override
    public LeavePieChartView getLeaveChartGroupByType(PeriodDto dto) {
        List<LeaveTypeProjection> leaveTypes = leaveRepository.getLeaveByPeriodGroupByType(dto.getFrom(), dto.getTo());
        LeavePieChartView pieChartView = new LeavePieChartView();
        DateFormat sdf = new SimpleDateFormat("dd.MM.yyy");
        pieChartView.setPeriodTitle(format("%s - %s", sdf.format(dto.getFrom()), sdf.format(dto.getTo())));
        pieChartView.setValues(leaveTypes.stream()
                .map(value -> {
                    PieChartView pieChartValue = new PieChartView();
                    pieChartValue.setKey(value.getLeaveType().name());
                    pieChartValue.setValue(value.getHours());
                    return pieChartValue;
                }).collect(Collectors.toList()));
        if (isEmpty(pieChartView.getValues())) {
            pieChartView.setValues(Collections.singletonList(new PieChartView("-", 0)));
        }
        return pieChartView;
    }
}
