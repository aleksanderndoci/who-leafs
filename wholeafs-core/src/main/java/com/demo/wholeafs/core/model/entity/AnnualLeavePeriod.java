/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityDateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@SQLDelete(sql = "UPDATE annual_leave_period SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
@Table(name = "annual_leave_period")
public class AnnualLeavePeriod extends CommonEntityDateUser {
    @Column(name = "period_title",
            nullable = false)
    private String periodTitle;

    @Column(name = "start_date",
            nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "end_date",
            nullable = false)
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "default_total_hours")
    private BigDecimal defaultTotalHours;
}
