/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.exception.common.WhoLeafsBusinessException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.constant.EmailTemplates;
import com.demo.wholeafs.core.model.entity.LeaveApprover;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.pojo.EmailRequestModel;
import com.demo.wholeafs.core.model.pojo.EmailRequestModel.EmailType;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.view.LeaveApproverView;
import com.demo.wholeafs.core.model.view.LeaveDetailedApproverView;
import com.demo.wholeafs.core.repository.LeaveApproverRepository;
import com.demo.wholeafs.core.service.EmailService;
import com.demo.wholeafs.core.service.LeaveApproverService;
import com.demo.wholeafs.core.service.UserService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import com.demo.wholeafs.core.util.EmailUtil;
import com.demo.wholeafs.core.util.func.Composer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.demo.wholeafs.core.repository.util.LeaveApproverSpecificationBuilder.from;
import static com.demo.wholeafs.core.util.StringUtils.concatenateWithSeparator;
import static java.util.Collections.singletonList;

@Service
@Transactional
@Slf4j
public class LeaveApproverServiceImpl extends CommonCrudServiceImpl<LeaveApproverModel, LeaveApprover>
        implements LeaveApproverService {

    private final LeaveApproverRepository repository;
    private final UserService userService;
    private final EmailService emailService;


    public LeaveApproverServiceImpl(LeaveApproverRepository repository,
                                    Mapper<LeaveApproverModel, LeaveApprover> mapper,
                                    UserService userService, EmailService emailService) {
        super(repository, mapper);
        this.repository = repository;
        this.userService = userService;
        this.emailService = emailService;
    }

    @Override
    public List<LeaveApproverModel> addApprovers(List<String> userGuids, LeaveModel leaveModel) {
        if (CollectionUtils.isEmpty(userGuids))
            throw new WhoLeafsBusinessException("No approvers provided for leave");
        log.info("Adding: {} approvers to Leave: {}", userGuids.size(), leaveModel.getGuid());
        List<LeaveApproverModel> leaveApprovers = userGuids.stream()
                .map(guid -> this.addApprover(leaveModel, guid))
                .collect(Collectors.toList());
        this.sendLeaveAlertEmail(leaveApprovers, leaveModel);
        return leaveApprovers;
    }

    @Override
    public PageableResponse<LeaveApproverView> getAvailableApprovers(UserSearchDto userSearchDto) {
        userSearchDto.setRole(RoleEnum.SUPERVISOR);
        PageableResponse<UserModel> users = userService.searchUsers(userSearchDto);
        PageableResponse<LeaveApproverView> leaveApprovers = new PageableResponse<>();
        leaveApprovers.setPage(users.getPage());
        leaveApprovers.setSize(users.getSize());
        leaveApprovers.setTotalElements(users.getTotalElements());
        leaveApprovers.setResults(users.getResults().stream()
                .map(userModel -> {
                    LeaveApproverView lav = new LeaveApproverView();
                    lav.setName(userModel.getName());
                    lav.setSurname(userModel.getSurname());
                    lav.setGuid(userModel.getGuid());
                    return lav;
                }).collect(Collectors.toList()));
        return leaveApprovers;
    }

    @Override
    public List<LeaveApproverStatusProjection> getApproversStatus(String leaveGuid, String excludedApproverGuid) {
        return repository.getOtherApproversStatus(leaveGuid, excludedApproverGuid);
    }

    @Override
    public PageableResponse<LeaveDetailedApproverView> searchLeaveApprover(LeaveSearchDto leaveSearchDto) {
        Page<LeaveApprover> leaveApproverPage = repository.findAll(from(leaveSearchDto), super.pageRequest(leaveSearchDto));
        return super.toCustomPageable(leaveApproverPage, leaveApprover -> {
            LeaveDetailedApproverView view = new LeaveDetailedApproverView();
            view.setLeave(leaveApprover.getLeave());
            view.setApproverStatus(leaveApprover.getStatus());
            view.setGuid(leaveApprover.getGuid());
            return view;
        });
    }

    private LeaveApproverModel addApprover(LeaveModel leaveModel, String approverGuid) {
        log.info("Creating a new approver for leave: {} | user: {}", leaveModel.getGuid(), approverGuid);
        UserModel approver = userService.findByGuid(approverGuid)
                .orElseThrow(() -> new EntityNotFoundException("User guid: {} not found", approverGuid));
        LeaveApproverModel leaveApproverModel = new LeaveApproverModel();
        leaveApproverModel.setApprover(approver);
        leaveApproverModel.setStatus(LeaveStatusEnum.PENDING);
        leaveApproverModel.setLeave(leaveModel);
        return super.save(leaveApproverModel);
    }

    private void sendLeaveAlertEmail(List<LeaveApproverModel> leaveApprovers, LeaveModel leaveModel) {
        UserModel leaveApplier = leaveModel.getUserLeavePeriod().getUser();
        Objects.requireNonNull(leaveApplier);
        List<String> approverEmails = leaveApprovers
                .stream()
                .map(approverModel -> approverModel.getApprover().getEmail())
                .collect(Collectors.toList());
        Map<String, Object> params = new HashMap<>();
        params.put("applierFullName", concatenateWithSeparator(" ", leaveApplier.getName(), leaveApplier.getSurname()));
        EmailRequestModel emailRequestModel = EmailRequestModel
                .builder()
                .emailType(EmailType.WITH_HTML_TEMPLATE)
                .subject(EmailUtil.getValue("email.leave_application_alert.subject"))
                .templateName(EmailTemplates.LEAVE_APPLICATION_ALERT)
                .templateValues(params)
                .receivers(approverEmails)
                .cc(singletonList(leaveApplier.getEmail()))
                .build();
        emailService.sendAsyncMimeEmail(emailRequestModel, Composer.none());
    }
}
