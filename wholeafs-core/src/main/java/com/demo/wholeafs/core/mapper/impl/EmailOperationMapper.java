/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.EmailOperation;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.pojo.EmailOperationModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class EmailOperationMapper extends CommonMapper<EmailOperationModel, EmailOperation>
        implements Mapper<EmailOperationModel, EmailOperation> {

    private final Mapper<UserModel, User> userMapper;

    public EmailOperationMapper(Mapper<UserModel, User> userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    protected EmailOperationModel mapUniqueFields(EmailOperation entity) {
        EmailOperationModel model = new EmailOperationModel();
        BeanUtils.copyProperties(entity, model);
        model.setUser(userMapper.toModel(entity.getUser()));
        return model;
    }

    @Override
    protected EmailOperation mapUniqueFields(EmailOperationModel model) {
        EmailOperation entity = new EmailOperation();
        BeanUtils.copyProperties(model, entity);
        entity.setUser(userMapper.toEntity(model.getUser()));
        return entity;
    }
}
