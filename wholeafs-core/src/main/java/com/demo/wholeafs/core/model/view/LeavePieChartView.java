/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.view;

import lombok.Data;

import java.util.List;

@Data
public class LeavePieChartView {
    private String periodTitle;
    private List<PieChartView> values;
}
