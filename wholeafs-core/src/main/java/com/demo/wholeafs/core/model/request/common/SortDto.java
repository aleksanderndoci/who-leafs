/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortDto {
    private String sortBy;
    private Sort.Direction sortDirection;
}
