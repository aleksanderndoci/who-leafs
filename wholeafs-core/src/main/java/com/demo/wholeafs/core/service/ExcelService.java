/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import org.springframework.core.io.Resource;

public interface ExcelService {
    Resource generateLeaveExcel(LeaveSearchDto searchDto);
}
