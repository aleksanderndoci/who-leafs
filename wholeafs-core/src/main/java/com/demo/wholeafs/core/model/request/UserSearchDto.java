/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request;

import com.demo.wholeafs.core.model.enums.AccountStatusEnum;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.request.common.CommonPageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserSearchDto extends CommonPageRequest {
    private String fullName;
    private String username;
    private String email;
    private RoleEnum role;
    private AccountStatusEnum accountStatus;
}
