/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util.func;

@FunctionalInterface
public interface Executable<T> {
    T execute();
}
