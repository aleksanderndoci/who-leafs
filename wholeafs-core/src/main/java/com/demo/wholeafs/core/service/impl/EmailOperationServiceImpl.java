/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.constant.EmailTemplates;
import com.demo.wholeafs.core.model.entity.EmailOperation;
import com.demo.wholeafs.core.model.enums.EmailRequestStatusEnum;
import com.demo.wholeafs.core.model.enums.EmailRequestType;
import com.demo.wholeafs.core.model.pojo.EmailOperationModel;
import com.demo.wholeafs.core.model.pojo.EmailRequestModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.repository.EmailOperationRepository;
import com.demo.wholeafs.core.service.EmailOperationService;
import com.demo.wholeafs.core.service.EmailService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import com.demo.wholeafs.core.util.EmailUtil;
import com.demo.wholeafs.core.util.func.Composer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Map;
import java.util.Random;

import static com.demo.wholeafs.core.util.StringUtils.matrixToMap;

@Service
@Transactional
@Slf4j
public class EmailOperationServiceImpl extends CommonCrudServiceImpl<EmailOperationModel, EmailOperation>
        implements EmailOperationService {


    private final EmailOperationRepository repository;
    private final Mapper<EmailOperationModel, EmailOperation> mapper;
    private final EmailService emailService;

    @Autowired
    public EmailOperationServiceImpl(EmailOperationRepository repository,
                                     Mapper<EmailOperationModel, EmailOperation> mapper,
                                     EmailService emailService) {
        super(repository, mapper);
        this.repository = repository;
        this.mapper = mapper;
        this.emailService = emailService;
    }

    @Override
    public void emailConfirmationRequest(UserModel user, String baseUrl) {
        log.info("Email confirmation request: {}", user.getEmail());
        EmailOperationModel emailOperation = this.repository.findByUserId(user.getId())
                .map(mapper::toModel)
                .orElse(new EmailOperationModel());
        emailOperation.setUser(user);
        emailOperation.setRequestType(EmailRequestType.CONFIRM_EMAIL);
        emailOperation.setRequestStatus(EmailRequestStatusEnum.SENT);
        emailOperation = super.save(emailOperation);
        log.info("Email confirmation sent. Guid: {}", emailOperation.getGuid());
        this.sendEmail(user, "email.confirm_account.subject",
                EmailTemplates.CONFIRMATION_EMAIL,
                matrixToMap(new String[][]{
                        {"confirmationLink", baseUrl.replace("{guid}", emailOperation.getGuid())}
                }));
    }

    @Override
    public String passwordResetRequest(UserModel user, String baseUrl) {
        log.info("Password reset request: {}", user.getEmail());
        EmailOperationModel emailOperation = this.repository.findByUserId(user.getId())
                .map(mapper::toModel)
                .orElse(new EmailOperationModel());
        emailOperation.setUser(user);
        emailOperation.setRequestType(EmailRequestType.PASSWORD_RESET);
        emailOperation.setRequestStatus(EmailRequestStatusEnum.SENT);
        Random random = new Random();
        //Generating a random number from 100000 to 999999 as resetPin
        emailOperation.setResetPin(100000 + random.nextInt(900000));
        emailOperation = super.save(emailOperation);
        log.info("Email confirmation sent. Guid: {}", emailOperation.getGuid());
        this.sendEmail(user, "email.reset_password.subject",
                EmailTemplates.RESET_PASSWORD_REQ,
                matrixToMap(new String[][]{
                        {"resetLink", baseUrl.replace("{guid}", emailOperation.getGuid())},
                        {"resetPin", String.valueOf(emailOperation.getResetPin())}
                }));
        return emailOperation.getGuid();
    }

    private void sendEmail(UserModel userModel,
                           String subjectKey,
                           String template,
                           Map<String, Object> templateValues) {
        templateValues.put("name", userModel.getName());
        EmailRequestModel emailRequestModel = EmailRequestModel.builder()
                .emailType(EmailRequestModel.EmailType.WITH_HTML_TEMPLATE)
                .receivers(Collections.singletonList(userModel.getEmail()))
                .subject(EmailUtil.getValue(subjectKey))
                .templateValues(templateValues)
                .templateName(template)
                .build();
        log.info("Email request: {}", emailRequestModel);
        this.emailService.sendAsyncMimeEmail(emailRequestModel, Composer.none());
    }
}
