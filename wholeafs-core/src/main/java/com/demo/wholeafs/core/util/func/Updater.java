/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util.func;

/**
 * Functional interface used to return a reference of <T> object (possibly updated).
 *
 * @param <T>
 */
public interface Updater<T> {
    T update(T model);

    static <S> Updater<S> none() {
        return null;
    }
}
