/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.view;

import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class LeaveDetailedApproverView extends CommonModel {
    private String periodTitle;

    private Date startDate;

    private Date endDate;

    private BigDecimal hours;

    private LeaveTypeEnum leaveType;

    private LeaveStatusEnum overallStatus;

    private LeaveStatusEnum approverStatus;

    private String reason;

    public void setLeave(Leave leave) {
        BeanUtils.copyProperties(leave, this);
        this.setOverallStatus(leave.getLeaveStatus());
        this.setPeriodTitle(leave.getUserLeavePeriod().getPeriod().getPeriodTitle());
    }
}
