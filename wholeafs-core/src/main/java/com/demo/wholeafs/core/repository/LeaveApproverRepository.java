/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.LeaveApprover;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LeaveApproverRepository extends CommonCrudRepository<LeaveApprover> {
    @Query("SELECT la.status as status, la.guid as guid " +
            "FROM LeaveApprover la " +
            "INNER JOIN Leave l on l.id = la.leave.id " +
            "WHERE la.guid <>:excludedApproverGuid " +
            "AND l.guid=:leaveGuid")
    List<LeaveApproverStatusProjection> getOtherApproversStatus(String leaveGuid, String excludedApproverGuid);
}
