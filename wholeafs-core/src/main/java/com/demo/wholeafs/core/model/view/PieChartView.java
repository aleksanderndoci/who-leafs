/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PieChartView {
    private String key;
    private double value;
}
