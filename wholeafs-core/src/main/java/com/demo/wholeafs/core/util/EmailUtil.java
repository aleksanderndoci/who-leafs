/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util;

import com.demo.wholeafs.core.exception.MissingTranslationException;

import java.util.Locale;
import java.util.ResourceBundle;

public class EmailUtil {

    public static String getValue(String key) {
        return getValue(key, Locale.getDefault());
    }

    public static String getValue(String key, Locale locale) {
        ResourceBundle emailBundle = ResourceBundle
                .getBundle("templates/email/translation", locale);
        if (!emailBundle.containsKey(key)) {
            throw new MissingTranslationException("Missing key: {} for translation in bundle: {}",
                    key, emailBundle.getBaseBundleName());
        }
        return emailBundle.getString(key);
    }
}
