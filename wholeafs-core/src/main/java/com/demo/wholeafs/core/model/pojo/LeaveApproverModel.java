/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LeaveApproverModel extends CommonModel {
    private UserModel approver;

    private LeaveModel leave;

    private LeaveStatusEnum status;

    private String reason;
}
