/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util.func;

@FunctionalInterface
public interface Composer<T, R> {
    R compose(T input) throws Exception;

    static <T, R> Composer<T, R> none() {
        return null;
    }
}
