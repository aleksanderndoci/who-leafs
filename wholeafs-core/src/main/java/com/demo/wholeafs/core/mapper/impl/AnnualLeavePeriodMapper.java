/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.AnnualLeavePeriod;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AnnualLeavePeriodMapper extends CommonMapper<AnnualLeavePeriodModel, AnnualLeavePeriod>
        implements Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> {
    @Override
    protected AnnualLeavePeriodModel mapUniqueFields(AnnualLeavePeriod entity) {
        AnnualLeavePeriodModel model = new AnnualLeavePeriodModel();
        // TODO map fields properly
        BeanUtils.copyProperties(entity, model);
        return model;
    }

    @Override
    protected AnnualLeavePeriod mapUniqueFields(AnnualLeavePeriodModel model) {
        AnnualLeavePeriod entity = new AnnualLeavePeriod();
        // TODO map fields properly
        BeanUtils.copyProperties(model, entity);
        return entity;
    }
}
