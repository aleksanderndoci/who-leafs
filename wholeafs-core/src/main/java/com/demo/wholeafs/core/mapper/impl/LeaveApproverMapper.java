/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.entity.LeaveApprover;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class LeaveApproverMapper extends CommonMapper<LeaveApproverModel, LeaveApprover> {

    private final Mapper<UserModel, User> userMapper;

    private final Mapper<LeaveModel, Leave> leaveMapper;

    public LeaveApproverMapper(Mapper<UserModel, User> userMapper,
                               Mapper<LeaveModel, Leave> leaveMapper) {
        this.userMapper = userMapper;
        this.leaveMapper = leaveMapper;
    }

    @Override

    protected LeaveApproverModel mapUniqueFields(LeaveApprover entity) {
        LeaveApproverModel model = new LeaveApproverModel();
        BeanUtils.copyProperties(entity, model);
        model.setApprover(userMapper.toModel(entity.getApprover()));
        model.setLeave(leaveMapper.toModel(entity.getLeave()));
        return model;
    }

    @Override
    protected LeaveApprover mapUniqueFields(LeaveApproverModel model) {
        LeaveApprover entity = new LeaveApprover();
        BeanUtils.copyProperties(model, entity);
        entity.setApprover(userMapper.toEntity(model.getApprover()));
        entity.setLeave(leaveMapper.toEntity(model.getLeave()));
        return entity;
    }
}
