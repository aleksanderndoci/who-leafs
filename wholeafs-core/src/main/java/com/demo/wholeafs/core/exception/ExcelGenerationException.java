/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBaseRuntimeException;

public class ExcelGenerationException extends WhoLeafsBaseRuntimeException {
    public static final String ERROR_CODE = "error.excelGen";

    public ExcelGenerationException(String msg, Object... params) {
        this(ERROR_CODE, ErrorType.FATAL, msg, params);
    }

    public ExcelGenerationException(String errorCode, ErrorType errorType, String s, Object... params) {
        super(errorCode, errorType, s, params);
    }
}
