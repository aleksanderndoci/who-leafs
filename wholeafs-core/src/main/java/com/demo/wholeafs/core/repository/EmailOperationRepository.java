/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.EmailOperation;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;

import java.util.Optional;

public interface EmailOperationRepository extends CommonCrudRepository<EmailOperation> {
    Optional<EmailOperation> findByUserId(Integer id);
}
