/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl.util;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.*;
import com.demo.wholeafs.core.model.pojo.*;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MapperFactoryImpl implements MapperFactory {
    private Mapper<UserModel, User> userMapper;

    private Mapper<LeaveModel, Leave> leaveMapper;

    private Mapper<UserLeavePeriodModel, UserLeavePeriod> userLeavePeriodMapper;

    private Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper;

    private Mapper<RoleModel, Role> roleMapper;

    private Mapper<LeaveApproverModel, LeaveApprover> leaveApproverMapper;

    private Mapper<EmailOperationModel, EmailOperation> emailOperationMapper;


    @Autowired
    public void setUserMapper(Mapper<UserModel, User> userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setLeaveMapper(Mapper<LeaveModel, Leave> leaveMapper) {
        this.leaveMapper = leaveMapper;
    }

    @Autowired
    public void setUserLeavePeriodMapper(Mapper<UserLeavePeriodModel, UserLeavePeriod> userLeavePeriodMapper) {
        this.userLeavePeriodMapper = userLeavePeriodMapper;
    }

    @Autowired
    public void setAnnualLeavePeriodMapper(Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper) {
        this.annualLeavePeriodMapper = annualLeavePeriodMapper;
    }

    @Autowired
    public void setRoleMapper(Mapper<RoleModel, Role> roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Autowired
    public void setLeaveApproverMapper(Mapper<LeaveApproverModel, LeaveApprover> leaveApproverMapper) {
        this.leaveApproverMapper = leaveApproverMapper;
    }

    @Autowired
    public void setEmailOperationMapper(Mapper<EmailOperationModel, EmailOperation> emailOperationMapper) {
        this.emailOperationMapper = emailOperationMapper;
    }

    @Override
    public Mapper<UserModel, User> userMapper() {
        return this.userMapper;
    }

    @Override
    public Mapper<LeaveModel, Leave> leaveMapper() {
        return this.leaveMapper;
    }

    @Override
    public Mapper<UserLeavePeriodModel, UserLeavePeriod> userLeavePeriodMapper() {
        return this.userLeavePeriodMapper;
    }

    @Override
    public Mapper<AnnualLeavePeriodModel, AnnualLeavePeriod> annualLeavePeriodMapper() {
        return this.annualLeavePeriodMapper;
    }

    @Override
    public Mapper<RoleModel, Role> roleMapper() {
        return this.roleMapper;
    }

    @Override
    public Mapper<LeaveApproverModel, LeaveApprover> leaveApproverMapper() {
        return this.leaveApproverMapper;
    }

    @Override
    public Mapper<EmailOperationModel, EmailOperation> emailOperationMapper() {
        return this.emailOperationMapper;
    }
}
