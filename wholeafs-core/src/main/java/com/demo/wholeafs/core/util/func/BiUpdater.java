/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.util.func;

@FunctionalInterface
public interface BiUpdater<M, E> {
    void apply(M model, E entity);
}
