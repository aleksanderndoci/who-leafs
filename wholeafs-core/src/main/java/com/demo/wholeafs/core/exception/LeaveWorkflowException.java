/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBusinessException;

public class LeaveWorkflowException extends WhoLeafsBusinessException {
    private static final String ERROR_CODE = "error.leaveWorkflow";

    public LeaveWorkflowException(String s, Object... params) {
        this(ERROR_CODE, ErrorType.BUSINESS, s, params);
    }

    public LeaveWorkflowException(String errorCode, ErrorType errorType, String s, Object... params) {
        super(errorCode, errorType, s, params);
    }
}
