/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityData;
import com.demo.wholeafs.core.model.enums.AccountStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user")
@SQLDelete(sql = "UPDATE user SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
public class User extends CommonEntityData {

    @Column(name = "name",
            nullable = false)
    private String name;

    @Column(name = "surname",
            nullable = false)
    private String surname;

    @Column(name = "username",
            nullable = false,
            unique = true)
    private String username;

    @Column(name = "email",
            unique = true)
    private String email;

    @Column(name = "password",
            nullable = false)
    private String password;

    @Column(name = "account_status",
            nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountStatusEnum accountStatus;

    @JoinColumn(name = "role_id",
            nullable = false)
    @ManyToOne
    private Role role;

}
