/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.projection.LeaveStatusProjection;
import com.demo.wholeafs.core.model.projection.LeaveTypeProjection;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface LeaveRepository extends CommonCrudRepository<Leave> {
    @Query("SELECT l FROM Leave l WHERE l.userLeavePeriod.user.id=:userId")
    Page<Leave> findAllByUserLeavePeriodUserId(Integer userId, Pageable pageable);

    @Query("SELECT l.leaveStatus as status, SUM(l.hours) as hours FROM Leave l " +
            "WHERE l.userLeavePeriod.id =:periodId " +
            "GROUP BY (l.leaveStatus)")
    List<LeaveStatusProjection> getUserLeaveStatus(Integer periodId);

    @Query("SELECT l.leaveType as leaveType, sum(l.hours) as hours " +
            "FROM Leave l " +
            "WHERE l.startDate>=:from AND l.endDate<=:to " +
            "GROUP BY (l.leaveType)")
    List<LeaveTypeProjection> getLeaveByPeriodGroupByType(Date from, Date to);
}
