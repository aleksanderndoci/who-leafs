/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.common;

import lombok.Data;

import java.util.Date;

@Data
public class CommonModel {
    private Integer id;

    private String guid;

    private Date insertedAt;

    private Date lastUpdated;
}
