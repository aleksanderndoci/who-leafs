/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.model.pojo.EmailRequestModel;
import com.demo.wholeafs.core.service.EmailService;
import com.demo.wholeafs.core.util.func.Composer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender mailSender;
    private final TemplateEngine emailTemplateEngine;

    public EmailServiceImpl(JavaMailSender mailSender,
                            TemplateEngine emailTemplateEngine) {
        this.mailSender = mailSender;
        this.emailTemplateEngine = emailTemplateEngine;
    }

    @Override
    @Async
    public void sendAsyncMimeEmail(EmailRequestModel emailRequestModel, Composer<MimeMessageHelper, Void> composer) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(emailRequestModel.getTemplateValues());
        this.sendMimeEmail((mHelper) -> {
            mHelper.setTo(this.toStrArray(emailRequestModel.getReceivers()));
            mHelper.setCc(this.toStrArray(emailRequestModel.getCc()));
            mHelper.setSubject(emailRequestModel.getSubject());
            String body = this.emailTemplateEngine.process(emailRequestModel.getTemplateName(), thymeleafContext);
            mHelper.setText(body, true);
            if (composer != null) {
                composer.compose(mHelper);
            }
            return null;
        });
    }


    @SneakyThrows
    private void sendMimeEmail(Composer<MimeMessageHelper, Void> composer) {
        MimeMessage mimeMailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMailMessage);
        composer.compose(helper);
        mailSender.send(helper.getMimeMessage());
        String recipents = Arrays.toString(helper.getMimeMessage().getAllRecipients());
        log.info("Email sent to: {}", recipents);
    }

    private String[] toStrArray(List<String> stringList) {
        return stringList == null ? new String[0] : stringList.toArray(new String[0]);
    }
}
