/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.exception;

import com.demo.wholeafs.core.exception.common.WhoLeafsBaseRuntimeException;

public class MissingTranslationException extends WhoLeafsBaseRuntimeException {

    private static final String GENERIC_ERROR = "error.missingTranslation";

    public MissingTranslationException(String s, Object... params) {
        super(GENERIC_ERROR, s, params);
    }
}
