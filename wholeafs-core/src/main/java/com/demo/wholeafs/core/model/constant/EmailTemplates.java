/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EmailTemplates {
    private static final String BASE_URL = "email/htmlTemplates/";
    public static final String CONFIRMATION_EMAIL = BASE_URL + "confirm_email.html";
    public static final String LEAVE_APPLICATION_ALERT = BASE_URL + "leave_application_alert.html";
    public static final String RESET_PASSWORD_REQ = BASE_URL + "reset_password.html";
}
