/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.common;

import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.exception.MissingPageException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.util.MapperFactory;
import com.demo.wholeafs.core.model.entity.common.CommonEntityData;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.projection.IdProjection;
import com.demo.wholeafs.core.model.request.common.CommonPageRequest;
import com.demo.wholeafs.core.model.request.common.PageDto;
import com.demo.wholeafs.core.model.request.common.SortDto;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import com.demo.wholeafs.core.util.func.Updater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Transactional
public class CommonCrudServiceImpl<M extends CommonModel, E extends CommonEntityData> implements CommonCrudService<M, E> {

    private CommonCrudRepository<E> commonCrudRepository;

    private Mapper<M, E> mapper;

    @Autowired
    protected MapperFactory mapperFactory;

    public CommonCrudServiceImpl(CommonCrudRepository<E> repository, Mapper<M, E> mapper) {
        this.commonCrudRepository = repository;
        this.mapper = mapper;
    }

    @Override
    public Optional<M> findById(Integer id) {
        return commonCrudRepository.findById(id)
                .map(mapper::toModel);
    }

    @Override
    public <T> Optional<T> findById(Integer id, Class<T> returnClass) {
        return commonCrudRepository.findById(id, returnClass);
    }

    @Override
    public Optional<M> findByGuid(String guid) {
        return commonCrudRepository.findByGuid(guid)
                .map(mapper::toModel);
    }

    @Override
    public <T> Optional<T> findByGuid(String guid, Class<T> returnClass) {
        return commonCrudRepository.findByGuid(guid, returnClass);
    }

    @Override
    public List<M> findAll() {
        return mapper.toModelList(commonCrudRepository.findAll());
    }

    @Override
    public void deleteByGuid(String guid) {
        commonCrudRepository.deleteByGuid(guid);
    }

    @Override
    public void deleteById(Integer id) {
        commonCrudRepository.deleteById(id);
    }

    @Override
    public void delete(M model) {
        commonCrudRepository.delete(mapper.toEntity(model));
    }

    @Override
    public M save(M model) {
        E entity = mapper.toEntity(model);
        commonCrudRepository.save(entity);
        model.setId(entity.getId());
        model.setGuid(entity.getGuid());
        model.setLastUpdated(entity.getLastUpdated());
        model.setInsertedAt(entity.getInsertedAt());
        return model;
    }

    protected Integer getPkByGuid(String guid) {
        return this.findByGuid(guid, IdProjection.class)
                .map(IdProjection::getId)
                .orElseThrow(() -> new EntityNotFoundException("Error in class: {}, Entity with guid: {} not found", getClass(), guid));
    }

    @Override
    public List<M> saveAll(List<M> mList) {
        Iterable<E> entities = commonCrudRepository.saveAll(mapper.toEntityList(mList));
        return mapper.toModelList(entities);
    }

    protected void nullifyId(M model) {
        model.setId(null);
    }

    @Override
    @Async
    public void saveAsync(List<M> mList) {
        this.saveAll(mList);
    }

    @Override
    public List<M> saveAll(List<M> mList, Updater<M> updaterFunc) {
        mList = mList.stream().map(updaterFunc::update)
                .collect(Collectors.toList());
        return this.saveAll(mList);
    }

    @Override
    public void saveAsync(List<M> mList, Updater<M> updaterFunc) {
        mList = mList.stream().map(updaterFunc::update)
                .collect(Collectors.toList());
        this.saveAsync(mList);
    }

    protected PageRequest pageRequest(PageDto pageDto, SortDto sortDto) {
        if (pageDto == null) {
            throw new MissingPageException();
        }
        PageRequest pageRequest;
        if (sortDto == null) {
            pageRequest = PageRequest.of(pageDto.getPage(), pageDto.getSize());
        } else {
            if (sortDto.getSortDirection() == null) {
                sortDto.setSortDirection(Sort.Direction.ASC);
            }
            pageRequest = PageRequest.of(pageDto.getPage(), pageDto.getSize(),
                    Sort.by(sortDto.getSortDirection(), sortDto.getSortBy()));
        }
        return pageRequest;
    }

    protected PageRequest pageRequest(CommonPageRequest commonPageRequest) {
        Objects.requireNonNull(commonPageRequest);
        return this.pageRequest(commonPageRequest.getPage(), commonPageRequest.getSort());
    }

    protected PageableResponse<M> toPageable(Page<E> page) {
        return this.toPageable(page, Updater.none());
    }

    protected PageableResponse<M> toPageable(Page<E> page, Updater<M> customMapper) {
        PageableResponse pageableResponse = new PageableResponse();
        pageableResponse.setPage(page.getPageable().getPageNumber());
        pageableResponse.setSize(page.getPageable().getPageSize());
        pageableResponse.setTotalElements(page.getTotalElements());
        List<M> resultList = page.get()
                .map(mapper::toModel)
                .map(m -> {
                    return customMapper == null ? m : customMapper.update(m);
                })
                .collect(Collectors.toList());
        pageableResponse.setResults(resultList);
        return pageableResponse;
    }

    protected <E, M> PageableResponse<M> toCustomPageable(Page<E> page, Function<E, M> customMapper) {
        PageableResponse pageableResponse = new PageableResponse();
        pageableResponse.setPage(page.getPageable().getPageNumber());
        pageableResponse.setSize(page.getPageable().getPageSize());
        pageableResponse.setTotalElements(page.getTotalElements());
        List<M> resultList = page.get()
                .map(e -> customMapper.apply(e))
                .collect(Collectors.toList());
        pageableResponse.setResults(resultList);
        return pageableResponse;
    }

    protected M toModel(E entity) {
        return mapper.toModel(entity);
    }
}
