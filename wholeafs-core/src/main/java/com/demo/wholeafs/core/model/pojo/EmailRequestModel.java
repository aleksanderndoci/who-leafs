/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class EmailRequestModel {
    private List<String> receivers;
    private List<String> cc;
    private String templateName;
    private Map<String, Object> templateValues;
    private EmailType emailType;
    private String body;
    private String subject;


    public enum EmailType {
        WITH_HTML_TEMPLATE,
        RAW_TEXT,
        WITH_TEXT_TEMPLATE;
    }
}
