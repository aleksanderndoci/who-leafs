/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.request;

import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class LeaveRequestDto {
    private Date startDate;

    private Date endDate;

    private LeaveTypeEnum leaveType;

    private String reason;

    private List<String> approverGuids;
}
