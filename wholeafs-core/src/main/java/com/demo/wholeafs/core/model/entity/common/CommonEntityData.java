/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity.common;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@MappedSuperclass
public class CommonEntityData {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "guid",
            nullable = false,
            updatable = false,
            unique = true)
    private String guid;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "inserted_at",
            updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedAt;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @PrePersist
    private void prePersist() {
        this.insertedAt = new Date();
        this.lastUpdated = new Date();
        this.deleted = false;
        this.guid = StringUtils.substring(UUID.randomUUID().toString(), 0, 32);
    }

    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
        if (this.deleted == null) {
            this.deleted = false;
        }
    }
}
