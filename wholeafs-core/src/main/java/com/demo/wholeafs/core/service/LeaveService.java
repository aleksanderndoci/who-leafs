/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.request.LeaveRequestDto;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.request.LeaveStatusUpdateDto;
import com.demo.wholeafs.core.model.request.PeriodDto;
import com.demo.wholeafs.core.model.request.common.PageDto;
import com.demo.wholeafs.core.model.view.LeaveDetailedApplierView;
import com.demo.wholeafs.core.model.view.LeavePieChartView;
import com.demo.wholeafs.core.service.common.CommonCrudService;

public interface LeaveService extends CommonCrudService<LeaveModel, Leave> {
    PageableResponse<LeaveModel> getUserLeave(Integer userId, PageDto pageDto);

    LeavePieChartView getLeavePieChart(Integer userId);

    LeaveModel applyForLeave(Integer userId, LeaveRequestDto leaveRequestDto);

    LeaveModel cancelLeave(String leaveGuid);

    LeaveApproverModel changeStatus(String leaveApproverGuid, LeaveStatusUpdateDto dto);

    PageableResponse<LeaveDetailedApplierView> searchLeaveApplier(LeaveSearchDto leaveSearchDto);

    LeavePieChartView getLeaveChartGroupByType(PeriodDto dto);
}
