/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.enums;

import lombok.Getter;

@Getter
public enum EmailRequestType {
    CONFIRM_EMAIL,
    PASSWORD_RESET
}
