/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service;

import com.demo.wholeafs.core.model.entity.Role;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import com.demo.wholeafs.core.service.common.CommonCrudService;

public interface RoleService extends CommonCrudService<RoleModel, Role> {
    RoleModel findByRole(RoleEnum role);
}
