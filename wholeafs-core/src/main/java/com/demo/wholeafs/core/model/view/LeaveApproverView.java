/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.view;

import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.pojo.common.CommonModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class LeaveApproverView {
    private String name;
    private String surname;
    private String guid;
    private LeaveStatusEnum status;
    private String reason;
}
