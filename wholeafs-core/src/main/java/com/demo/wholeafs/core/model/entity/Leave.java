/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityDateUser;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "`leave`")
@SQLDelete(sql = "UPDATE leave SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
@ToString(onlyExplicitlyIncluded = true)
public class Leave extends CommonEntityDateUser {

    @Column(name = "start_date",
            nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date",
            nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "hours",
            nullable = false)
    private BigDecimal hours;

    @Enumerated(EnumType.STRING)
    @Column(name = "leave_type")
    private LeaveTypeEnum leaveType;

    @Enumerated(EnumType.STRING)
    @Column(name = "leave_status")
    private LeaveStatusEnum leaveStatus;

    @Column(name = "reason")
    private String reason;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_period_id")
    private UserLeavePeriod userLeavePeriod;

    @OneToMany(mappedBy = "leave", fetch = FetchType.LAZY)
    private List<LeaveApprover> leaveApprovers = new ArrayList<>();
}
