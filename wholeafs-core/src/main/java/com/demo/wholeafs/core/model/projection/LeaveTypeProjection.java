/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.projection;

import com.demo.wholeafs.core.model.enums.LeaveTypeEnum;

public interface LeaveTypeProjection {
    LeaveTypeEnum getLeaveType();

    double getHours();
}
