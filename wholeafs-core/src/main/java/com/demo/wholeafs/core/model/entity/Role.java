/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityData;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "role")
@SQLDelete(sql = "UPDATE role SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
public class Role extends CommonEntityData {
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleEnum role;
}
