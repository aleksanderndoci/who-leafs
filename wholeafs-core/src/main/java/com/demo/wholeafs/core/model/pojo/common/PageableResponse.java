/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.pojo.common;


import lombok.Data;

import java.util.List;

@Data
public class PageableResponse<T> {
    private Integer page;
    private Integer size;
    private Long totalElements;
    private List<T> results;
}
