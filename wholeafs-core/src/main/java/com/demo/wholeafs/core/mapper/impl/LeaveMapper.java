/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.mapper.impl;

import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.mapper.impl.common.CommonMapper;
import com.demo.wholeafs.core.model.entity.Leave;
import com.demo.wholeafs.core.model.entity.UserLeavePeriod;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class LeaveMapper extends CommonMapper<LeaveModel, Leave> implements Mapper<LeaveModel, Leave> {

    private final Mapper<UserLeavePeriodModel, UserLeavePeriod> leavePeriodMapper;

    public LeaveMapper(Mapper<UserLeavePeriodModel, UserLeavePeriod> leavePeriodMapper) {
        this.leavePeriodMapper = leavePeriodMapper;
    }

    @Override
    protected LeaveModel mapUniqueFields(Leave entity) {
        LeaveModel model = new LeaveModel();
        BeanUtils.copyProperties(entity, model);
        model.setUserLeavePeriod(leavePeriodMapper.toModel(entity.getUserLeavePeriod()));
        return model;
    }

    @Override
    protected Leave mapUniqueFields(LeaveModel model) {
        Leave leave = new Leave();
        BeanUtils.copyProperties(model, leave);
        leave.setUserLeavePeriod(leavePeriodMapper.toEntity(model.getUserLeavePeriod()));
        return leave;
    }
}
