/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.EmailNotFoundException;
import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.exception.common.ErrorType;
import com.demo.wholeafs.core.exception.common.WhoLeafsBusinessException;
import com.demo.wholeafs.core.mapper.Mapper;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.model.enums.AccountStatusEnum;
import com.demo.wholeafs.core.model.enums.RoleEnum;
import com.demo.wholeafs.core.model.pojo.EmailOperationModel;
import com.demo.wholeafs.core.model.pojo.RoleModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.pojo.user.UserExistsModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.model.pojo.user.password.ChangePasswordDto;
import com.demo.wholeafs.core.model.pojo.user.password.PasswordResetResponse;
import com.demo.wholeafs.core.model.pojo.user.password.ResetPasswordDto;
import com.demo.wholeafs.core.model.projection.UserRoleProjection;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.view.PieChartView;
import com.demo.wholeafs.core.repository.UserRepository;
import com.demo.wholeafs.core.service.EmailOperationService;
import com.demo.wholeafs.core.service.RoleService;
import com.demo.wholeafs.core.service.UserService;
import com.demo.wholeafs.core.service.common.CommonCrudServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.demo.wholeafs.core.model.enums.EmailRequestStatusEnum.CONFIRMED;
import static com.demo.wholeafs.core.repository.util.UserSpecificationBuilder.searchUserSpec;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
@Transactional
@Slf4j
public class UserServiceImpl extends CommonCrudServiceImpl<UserModel, User> implements UserService {

    private final UserRepository userRepository;
    private final Mapper<UserModel, User> userMapper;
    private final EmailOperationService emailOperationService;

    private final RoleService roleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, Mapper<UserModel, User> userMapper,
                           EmailOperationService emailOperationService, RoleService roleService) {
        super(userRepository, userMapper);
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.emailOperationService = emailOperationService;
        this.roleService = roleService;
    }

    @Override
    public UserExistsModel checkIfUserExists(String usernameOrEmail) {
        log.info("checkIfUserExists()");
        User user = new User();
        user.setEmail(usernameOrEmail);
        user.setUsername(usernameOrEmail);
        boolean exists = userRepository.count(Example.of(user, ExampleMatcher.matchingAny())) > 0;
        log.info("Username/Email: {}, exists:{}", usernameOrEmail, exists);
        return new UserExistsModel(exists);
    }

    @Override
    public UserModel registerUser(UserModel userModel) {
        log.info("RegisterUser: {}", userModel);
        RoleModel userRole = roleService
                .findByRole(userModel.getRole() != null ? userModel.getRole().getRole() : RoleEnum.USER);
        userModel.setRole(userRole);
        if (userModel.getAccountStatus() == null) {
            userModel.setAccountStatus(AccountStatusEnum.NOT_CONFIRMED);
        }
        return super.save(userModel);
    }

    @Override
    public UserModel findByUsernameOrEmail(String usernameOrEmail) {
        return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .map(userMapper::toModel)
                .orElseThrow(() -> new EmailNotFoundException("Enail/Username: {} not found", usernameOrEmail));
    }

    @Override
    public void sendConfirmationEmail(String email, String baseUrl) {
        log.info("Sending email confirmation to: {}", email);
        UserModel userModel = this.findByUsernameOrEmail(email);
        AccountStatusEnum accountStatus = userModel.getAccountStatus();
        if (!AccountStatusEnum.NOT_CONFIRMED.equals(accountStatus)) {
            throw new WhoLeafsBusinessException("Invalid account status: {}. Cant send email.", accountStatus);
        }
        this.emailOperationService.emailConfirmationRequest(userModel, baseUrl);
    }

    @Override
    public void confirmEmail(String emailConfirmationGuid) {
        log.info("Confirming email. Guid: {}", emailConfirmationGuid);
        EmailOperationModel emailOperationModel = this.getUserValidEmailOperation(emailConfirmationGuid);
        emailOperationModel.setRequestStatus(CONFIRMED);
        UserModel userModel = emailOperationModel.getUser();
        userModel.setAccountStatus(AccountStatusEnum.ACTIVE);
        this.emailOperationService.save(emailOperationModel);
        super.save(userModel);
        log.info("Email {} confirmed", userModel.getEmail());
    }

    @Override
    public String sendPasswordResetRequest(ResetPasswordDto request) {
        log.info("Sending password reset request to: {}", request.getEmail());
        return this.emailOperationService.passwordResetRequest(this.findByUsernameOrEmail(request.getEmail()),
                request.getBaseUrl());
    }

    @Override
    public PasswordResetResponse changePassword(String passResetGuid, ResetPasswordDto request) {
        log.info("Resetting password. Guid: {}", passResetGuid);
        EmailOperationModel emailOperationModel = this.getUserValidEmailOperation(passResetGuid);
        PasswordResetResponse resetResponse = new PasswordResetResponse();
        UserModel userModel = emailOperationModel.getUser();
        if (!emailOperationModel.getResetPin().equals(request.getResetPin())) {
            resetResponse.setReason("Invalid reset pin");
        } else if (isBlank(request.getPassword())) {
            resetResponse.setReason("Provide a value for password");
        } else if (userModel.getPassword().equals(request.getPassword())) {
            resetResponse.setReason("Password must differ from the actual.");
        } else {
            userModel.setPassword(request.getPassword());
            resetResponse.setWasReset(true);
            emailOperationModel.setRequestStatus(CONFIRMED);
            emailOperationService.save(emailOperationModel);
            super.save(userModel);
            log.info("Password was reset for user: {}", userModel.getUsername());
        }
        return resetResponse;
    }

    @Override
    public PageableResponse<UserModel> searchUsers(UserSearchDto userSearchDto) {
        Objects.requireNonNull(userSearchDto);
        log.info("Searching users: {}", userSearchDto);
        return super.toPageable(this.userRepository
                .findAll(searchUserSpec(userSearchDto), super.pageRequest(userSearchDto)));
    }

    @Override
    public PasswordResetResponse changePassword(Integer userId, ChangePasswordDto request, Predicate<UserModel> canChange) {
        UserModel userModel = super.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User with id: {} not found", userId));
        PasswordResetResponse passwordResetResponse = new PasswordResetResponse();
        if (!canChange.test(userModel)) {
            passwordResetResponse.setReason("Wrong password!");
            passwordResetResponse.setWasReset(false);
        } else {
            userModel.setPassword(request.getNewPassword());
            super.save(userModel);
            passwordResetResponse.setWasReset(true);
        }
        return passwordResetResponse;
    }

    @Override
    public UserModel editAccount(UserModel userModel, boolean isAdmin) {
        log.info("Edit account: {}", userModel);
        Optional<UserModel> currentUserDataOptional;
        if (userModel.getId() != null) {
            currentUserDataOptional = super.findById(userModel.getId());
        } else {
            currentUserDataOptional = super.findByGuid(userModel.getGuid());
        }
        UserModel currentUserData = currentUserDataOptional
                .orElseThrow(() -> new EntityNotFoundException("User id: {}/guid:{} not found",
                        userModel.getId(), userModel.getGuid()));
        if (!isAdmin) {
            userModel.setRole(currentUserData.getRole());
            userModel.setPassword(currentUserData.getPassword());
        }
        super.save(userModel);
        log.info("Account edited successfully");
        return userModel;
    }

    @Override
    public List<PieChartView> getUsersByRole() {
        List<UserRoleProjection> usrRoleList = userRepository.getUsersGrouppedByRole();
        if (CollectionUtils.isEmpty(usrRoleList)) {
            return Collections.emptyList();
        }
        return usrRoleList.stream().map(usrRole -> {
            PieChartView pieChart = new PieChartView();
            pieChart.setValue(usrRole.getCount());
            pieChart.setKey(usrRole.getRole().name());
            return pieChart;
        }).collect(Collectors.toList());
    }

    @Override
    public void deleteByGuid(String guid, Integer deletedBy) {
        UserModel userModel = this.findByGuid(guid)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        if (deletedBy.equals(userModel.getId())) {
            throw new WhoLeafsBusinessException("error.deleteOwnAccount.disabled", ErrorType.BUSINESS, "Deleting own account is forbidden");
        }
        userRepository.deleteByGuid(guid);
    }

    private EmailOperationModel getUserValidEmailOperation(String guid) {
        return emailOperationService.findByGuid(guid)
                .filter(req -> !req.getRequestStatus().equals(CONFIRMED))
                .orElseThrow(() -> new WhoLeafsBusinessException("Operation not valid. Guid: {} not found", guid));
    }
}
