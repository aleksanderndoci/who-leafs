/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.repository;

import com.demo.wholeafs.core.model.entity.AnnualLeavePeriod;
import com.demo.wholeafs.core.repository.common.CommonCrudRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AnnualLeavePeriodRepository extends CommonCrudRepository<AnnualLeavePeriod> {

    @Query("SELECT l FROM AnnualLeavePeriod l " +
            "WHERE l.startDate<=CURRENT_DATE() " +
            "AND l.endDate>=CURRENT_DATE()")
    Optional<AnnualLeavePeriod> getActualLeavePeriod();
}
