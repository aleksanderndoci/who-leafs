/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.ExcelGenerationException;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.view.LeaveDetailedApplierView;
import com.demo.wholeafs.core.service.ExcelService;
import com.demo.wholeafs.core.service.LeaveService;
import com.demo.wholeafs.core.util.func.Composer;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Service
public class ExcelServiceImpl implements ExcelService {

    private final LeaveService leaveService;

    public ExcelServiceImpl(LeaveService leaveService) {
        this.leaveService = leaveService;
    }

    private Resource generateWorkbook(Composer<Workbook, Void> composer) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            try (Workbook workbook = new XSSFWorkbook()) {
                composer.compose(workbook);
                workbook.write(bos);
                return new ByteArrayResource(bos.toByteArray());
            }
        } catch (Exception e) {
            throw new ExcelGenerationException(e.getMessage());
        }
    }

    @Override
    public Resource generateLeaveExcel(LeaveSearchDto searchDto) {
        PageableResponse<LeaveDetailedApplierView> leaveView = leaveService.searchLeaveApplier(searchDto);
        return this.generateWorkbook(workbook -> {
            Sheet sheet = workbook.createSheet("Leave report");
            this.generateHeader(sheet, new String[]{
                    "Period title",
                    "Start date",
                    "End date",
                    "Total hours",
                    "Applier",
                    "Leave type",
                    "Leave Reason"
            });
            List<LeaveDetailedApplierView> resultList = leaveView.getResults();
            if (!isEmpty(resultList)) {
                resultList.forEach(result -> this.generateRow(sheet, new Object[]{
                        result.getPeriodTitle(),
                        result.getStartDate(),
                        result.getEndDate(),
                        result.getHours(),
                        result.getFullName(),
                        result.getLeaveType(),
                        result.getReason()
                }));
            }
            return null;
        });
    }

    private void generateHeader(Sheet sheet, String[] header) {
        Row row = sheet.createRow(0);
        int i = -1;
        while (++i < header.length) {
            Cell cell = row.createCell(i);
            cell.setCellValue(header[i]);
            CellStyle cellStyle = cell.getCellStyle();
            cellStyle.setShrinkToFit(true);
            cellStyle.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLUE_GREY.getIndex());
            cellStyle.setFillBackgroundColor(HSSFColor.HSSFColorPredefined.GREY_50_PERCENT.getIndex());
            cell.setCellStyle(cellStyle);
        }
    }

    private void generateRow(Sheet sheet, Object[] value) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        int i = -1;
        while (++i < value.length) {
            Cell cell = row.createCell(i);
            String strValue = this.stringify(value[i]);
            cell.setCellValue(strValue);
        }
    }

    private String stringify(Object o) {
        if (o == null) {
            return "-";
        }
        if (o instanceof Date) {
            return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(o);
        }
        return o.toString();
    }
}
