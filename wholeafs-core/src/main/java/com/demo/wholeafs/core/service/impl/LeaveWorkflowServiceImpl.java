/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.service.impl;

import com.demo.wholeafs.core.exception.LeaveWorkflowException;
import com.demo.wholeafs.core.model.enums.LeaveStatusEnum;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.UserLeavePeriodModel;
import com.demo.wholeafs.core.model.projection.LeaveApproverStatusProjection;
import com.demo.wholeafs.core.model.request.LeaveStatusUpdateDto;
import com.demo.wholeafs.core.repository.LeaveRepository;
import com.demo.wholeafs.core.service.LeaveWorkflowService;
import com.demo.wholeafs.core.service.UserLeavePeriodService;
import com.demo.wholeafs.core.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Objects;

import static com.demo.wholeafs.core.model.enums.LeaveStatusEnum.*;
import static com.demo.wholeafs.core.repository.util.LeaveSpecificationBuilder.betweenDates;
import static com.demo.wholeafs.core.repository.util.LeaveSpecificationBuilder.statusIn;
import static com.demo.wholeafs.core.util.DateUtils.findDaysBetween;
import static com.demo.wholeafs.core.util.DateUtils.findHoursBetween;
import static java.util.Arrays.asList;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Slf4j
public class LeaveWorkflowServiceImpl implements LeaveWorkflowService {

    private final LeaveRepository leaveRepository;
    private final UserLeavePeriodService userLeavePeriodService;

    public LeaveWorkflowServiceImpl(LeaveRepository leaveRepository,
                                    UserLeavePeriodService userLeavePeriodService) {
        this.leaveRepository = leaveRepository;
        this.userLeavePeriodService = userLeavePeriodService;
    }

    @Override
    public boolean validateLeaveApplication(Integer applierId, LeaveModel leaveModel) {
        if (leaveModel.getStartDate().after(leaveModel.getEndDate())) {
            throw new LeaveWorkflowException("End date has to be after start date");
        }
        int startHour = DateUtils.getFromDate(leaveModel.getStartDate(), ChronoField.HOUR_OF_DAY);
        int endHour = DateUtils.getFromDate(leaveModel.getEndDate(), ChronoField.HOUR_OF_DAY);
        if ((startHour < 8 || startHour > 18) || (endHour < 8 || endHour > 18)) {
            throw new LeaveWorkflowException("Time interval has to be from 8AM-18PM");
        }
        boolean hasPrevious = leaveRepository.count(betweenDates(leaveModel.getStartDate(), leaveModel.getEndDate())
                .and(statusIn(asList(PENDING, ACCEPTED, REFUSED)))) > 0;
        if (hasPrevious) {
            throw new LeaveWorkflowException("You already have active leave requests between StartDate-EndDate");
        }
        leaveModel.setHours(this.getLeaveDuration(leaveModel));
        log.info("Calculated hours={} for type:{} ", leaveModel.getHours(), leaveModel.getLeaveType());
        UserLeavePeriodModel period = userLeavePeriodService.getCurrentPeriod(applierId);
        if (period.getRemainingHours().compareTo(leaveModel.getHours()) < 0) {
            throw new LeaveWorkflowException("Not enough remaining hours: {}", period.getRemainingHours());
        }
        leaveModel.setUserLeavePeriod(period);
        return true;
    }

    @Override
    public void validateCancellationRequest(LeaveModel leaveModel) {
        if (ACCEPTED.equals(leaveModel.getLeaveStatus())) {
            throw new LeaveWorkflowException("Leave already accepted and cannot be refused");
        }
        if (REFUSED.equals(leaveModel.getLeaveStatus())) {
            throw new LeaveWorkflowException("Leave already refused and cannot be refused");
        }
    }

    @Override
    public void validateStatusChange(LeaveApproverModel leaveApproverModel, LeaveStatusUpdateDto dto,
                                     List<LeaveApproverStatusProjection> otherApproversStatus) {
        Objects.requireNonNull(dto);
        Objects.requireNonNull(dto.getLeaveStatus());
        LeaveStatusEnum leaveApproverStatus = leaveApproverModel.getStatus();
        LeaveStatusEnum leaveStatus = leaveApproverModel.getLeave().getLeaveStatus();
        if (dto.getLeaveStatus().getStep() <= leaveApproverStatus.getStep() ||
                dto.getLeaveStatus().getStep() <= leaveStatus.getStep()) {
            throw new LeaveWorkflowException("Invalid status: {}, actual status: {}", dto.getLeaveStatus(),
                    leaveApproverModel.getStatus());
        }
        if (REFUSED.equals(dto.getLeaveStatus())) {
            if (dto.getReason() == null) {
                throw new LeaveWorkflowException("Please specify refuse reason");
            }
            leaveApproverModel.setStatus(REFUSED);
            leaveApproverModel.getLeave().setLeaveStatus(REFUSED);
            leaveApproverModel.setReason(dto.getReason());
        } else {
            if (!ACCEPTED.equals(dto.getLeaveStatus())) {
                throw new LeaveWorkflowException("Invalid status: {}, actual status: {}", dto.getLeaveStatus(),
                        leaveApproverModel.getStatus());
            }
        }
        boolean acceptOverall = !isEmpty(otherApproversStatus) && otherApproversStatus.stream()
                .allMatch(sts -> ACCEPTED.equals(sts.getStatus()));
        if (acceptOverall) {
            leaveApproverModel.setStatus(ACCEPTED);
            leaveApproverModel.getLeave().setLeaveStatus(ACCEPTED);
        }
    }

    private BigDecimal getLeaveDuration(LeaveModel leaveModel) {
        switch (leaveModel.getLeaveType()) {
            case DAILY_LEAVE:
            case MATERNITY:
            case HOLIDAY_LEAVE:
            case PARENTAL_LEAVE:
            case SICK_LEAVE_PAYED:
                long days = findDaysBetween(leaveModel.getStartDate(), leaveModel.getEndDate()) + 1;
                return BigDecimal.valueOf(days * 8);
            case HOURLY_LEAVE:
                return BigDecimal.valueOf(findHoursBetween(leaveModel.getEndDate(), leaveModel.getStartDate()));
            //Other types of leave are not calculated as payed leave
            default:
                return BigDecimal.ZERO;
        }
    }
}
