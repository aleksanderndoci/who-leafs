/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.core.model.entity;

import com.demo.wholeafs.core.model.entity.common.CommonEntityDateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "user_leave_period")
@SQLDelete(sql = "UPDATE user_leave_period SET deleted = 1 WHERE ID = ?")
@Where(clause = "deleted <> '1'")
public class UserLeavePeriod extends CommonEntityDateUser {

    @Column(name = "total_hours",
            nullable = false)
    private BigDecimal totalHours;

    @Column(name = "remaining_hours",
            nullable = false)
    private BigDecimal remainingHours;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JoinColumn(name = "user_id",
            nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "period_id",
            nullable = false)
    private AnnualLeavePeriod period;
}
