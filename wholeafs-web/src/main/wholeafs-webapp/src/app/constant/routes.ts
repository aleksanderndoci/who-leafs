export enum routes {
  DASHBOARD = '/dashboard',
  LOGIN = '/login',
  PROFILE = '/profile',
  DASHBOARD_USERS = '/administrate/users',
  ADD_USERS = '/users/add',
  APPLY_LEAVE = '/leave/apply',
  LEAVE_HISTORY = '/leave/history',
  LEAVE_REPORTS = '/leave/reports',
  PENDING_LEAVE = '/leave/pending'
}
