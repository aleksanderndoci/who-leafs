/*
 * Copyright (c) Aleksander Ndoci
 */

export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER',
  FINANCE = 'FINANCE',
  SUPERVISOR = 'SUPERVISOR'
}

