/*
 * Copyright (c) Aleksander Ndoci
 */

export enum Configs {
  TOKEN = 'WHOLEAFS_TOKEN',
  AUTH_SCHEME = 'WHOLEAFS_AUTH_SCHEME',
  LOGGED_USER = "WHOLEAFS_LOGGED_USER"
}
