import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";
import {HttpRequestInterceptor} from "./shared/services/http/http-request-interceptor";
import {HttpResponseInterceptor} from "./shared/services/http/http-response-interceptor";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {LoginModule} from "./pages/login/login.module";
import {LoginRoutingModule} from "./pages/login/login-routing.module";
import {SharedModule} from "./shared/shared.module";
import {ToastrModule} from "ngx-toastr";
import {NotificationModule} from "./shared/components/notification/notification.module";
import {DashboardModule} from "./pages/dashboard/dashboard.module";
import {PagesModule} from "./pages/pages.module";
import {NgxMatNativeDateModule} from "@angular-material-components/datetime-picker";
import {UsernameValidator} from "./shared/forms/user-form/username-validator";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS} from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    FormsModule,
    MatToolbarModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    LoginModule,
    LoginRoutingModule,
    SharedModule,
    NotificationModule,
    DashboardModule,
    PagesModule,
    NgxMatNativeDateModule,
    ToastrModule.forRoot()
  ],
  providers: [
    CookieService,
    UsernameValidator,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpResponseInterceptor,
      multi: true
    },
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
      useValue: {useUtc: true}
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
