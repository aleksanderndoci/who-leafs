import {Component, OnInit} from '@angular/core';
import {Role} from "../../../../constant/role";
import {HttpService} from "../../../../shared/services/http/http.service";
import {Router} from "@angular/router";
import {routes} from "../../../../constant";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {
  public role: string;
  public _roles = Role;

  constructor(private httpService: HttpService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.httpService.getLoggedUser()
      .subscribe(user => {
        this.role = user.role;
      });
  }

  applyForLeave() {
    this.router.navigate([routes.APPLY_LEAVE]);
  }
}
