import {AfterViewInit, Component, OnInit} from '@angular/core';
import {
  DetailedLeaveView,
  LeaveSearchRequest,
  LeaveStatusChangeRequest,
  LeaveStatusEnum
} from "../../../../shared/model/leave-model";
import {BtnAction, TableBtn, TableCol} from "../../../../shared/components/table/table.component";
import {UserService} from "../../../../shared/services/user.service";
import {LeaveService} from "../../../../shared/services/leave-service";
import {UserModel} from "../../../../shared/model/user-model";
import {PagedResponse} from "../../../../shared/model/http/paged-response";
import {PageRequest} from "../../../../shared/model/http/page-request";
import {NotificationService} from "../../../../shared/components/notification";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {RefuseLeaveModalComponent} from "../../../../shared/components/refuse-leave-modal/refuse-leave-modal.component";

@Component({
  selector: 'app-supervisor-dashboard',
  templateUrl: './supervisor-dashboard.component.html',
  styleUrls: ['./supervisor-dashboard.component.scss']
})
export class SupervisorDashboardComponent implements OnInit, AfterViewInit {
  searchLeaveRequest: LeaveSearchRequest;
  leaveData: PagedResponse<DetailedLeaveView>;
  loggedUser: UserModel;
  showActiveOnly: boolean;
  operationBtns: TableBtn[] = [
    {
      id: 'approve',
      icon: 'check',
      label: 'Approve',
      color: 'primary'
    }, {
      id: 'refuse',
      icon: 'clear',
      label: 'Refuse',
      color: 'warn'
    }
  ];
  colsToDisplay: TableCol[] = [{
    key: 'periodTitle',
    displayAs: 'Period',
    sortKey: 'insertedAt'
  }, {
    key: 'startDate',
    displayAs: 'From',
    dataType: 'dateTime',
    sortKey: 'leave.startDate'
  }, {
    key: 'endDate',
    displayAs: 'To',
    dataType: 'dateTime',
    sortKey: 'leave.endDate'
  }, {
    key: 'hours',
    displayAs: 'Total hours',
    sortKey: 'leave.hours'
  }, {
    key: 'leaveType',
    displayAs: 'Leave type',
    sortKey: 'leave.leaveType'
  }, {
    key: 'reason',
    displayAs: 'Leave reason',
    sortKey: 'leave.reason'
  }, {
    key: 'overallStatus',
    displayAs: 'Leave status',
    sortKey: 'leave.leaveStatus'
  }, {
    key: 'approverStatus',
    displayAs: 'Your status',
    sortKey: 'status'
  }];

  constructor(private router: Router,
              private userService: UserService,
              private notificationService: NotificationService,
              private leaveService: LeaveService,
              public confirmRefuseDialog: MatDialog) {
  }

  openRefuseLeaveDialog(guid: string): void {
    const dialogRef = this.confirmRefuseDialog.open(RefuseLeaveModalComponent, {
      width: '250px',
      data: {
        guid: guid,
        reason: ""
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.changeLeaveStatus(result.guid, {
          leaveStatus: LeaveStatusEnum.REFUSED,
          reason: result.reason
        })
      }
    });
  }

  ngOnInit(): void {
    const url = this.router.url;
    this.showActiveOnly = url.includes('pending') || url.includes('dashboard');
    if (!this.showActiveOnly) {
      this.operationBtns = [];
    }
    this.userService.getCurrentUser()
      .subscribe(resp => {
        this.loggedUser = resp.data;
        this.searchLeaveRequest = {
          leaveStatus: this.showActiveOnly ? this.getActiveLeaveStatus() : this.getArchivedLeaveStatus(),
          approverGuid: this.loggedUser.guid,
          page: {
            page: 0,
            size: 10
          }
        };
        this.loadLeaveData();
      })
  }

  loadLeaveData() {
    this.leaveService.searchApproverLeaves(this.searchLeaveRequest)
      .subscribe(resp => {
        this.leaveData = resp;
      })
  }

  ngAfterViewInit(): void {
  }

  onPageSortChange(pageRequest: PageRequest) {
    this.searchLeaveRequest = {
      ...this.searchLeaveRequest,
      page: pageRequest.page,
      sort: pageRequest.sort
    };
    this.loadLeaveData();
  }

  operationBtnClick(btnAction: BtnAction<DetailedLeaveView>) {
    const data = btnAction.data;
    const id = btnAction.btnId;

    switch (id) {
      case 'approve':
        this.changeLeaveStatus(data.guid, {
          leaveStatus: LeaveStatusEnum.ACCEPTED
        });
        break;
      case 'refuse':
        this.openRefuseLeaveDialog(data.guid);
        break;
    }
  }

  changeLeaveStatus(guid: string, req: LeaveStatusChangeRequest) {
    this.leaveService.changeLeaveStatsu(guid, req)
      .subscribe(resp => {
        if (resp.data.status === LeaveStatusEnum.ACCEPTED) {
          this.notificationService.success({
            title: 'Done',
            message: 'Leave approved'
          })
        } else {
          this.notificationService.info({
            title: 'Done',
            message: 'Leave refused'
          })
        }
        const updatedData = this.leaveData.results
          .filter(leave => leave.guid !== guid);
        this.leaveData = {
          ...this.leaveData,
          results: updatedData
        }
      })
  }

  getActiveLeaveStatus(): Array<LeaveStatusEnum> {
    return [
      LeaveStatusEnum.PENDING,
      LeaveStatusEnum.SENT
    ];
  }

  getArchivedLeaveStatus(): Array<LeaveStatusEnum> {
    return [
      LeaveStatusEnum.REFUSED,
      LeaveStatusEnum.ACCEPTED,
      LeaveStatusEnum.CANCELED
    ];
  }
}
