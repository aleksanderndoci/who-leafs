import {Component, OnInit} from '@angular/core';
import {PieChartData} from "../../../../shared/model/charts/pie-chart-model";
import {UserService} from "../../../../shared/services/user.service";
import {AnnualPeriod} from "../../../../shared/model/leave-model";
import {LeaveService} from "../../../../shared/services/leave-service";
import {PagedResponse} from "../../../../shared/model/http/paged-response";
import {UserModel, UserSearchRequest} from "../../../../shared/model/user-model";

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  usersChart: PieChartData[];
  annualPeriod: AnnualPeriod;
  userData: PagedResponse<UserModel>;


  constructor(private userService: UserService,
              private leaveService: LeaveService) {
  }

  ngOnInit(): void {
    this.userService.getUserRolePieChart()
      .subscribe(resp => {
        this.usersChart = resp.data;
      });
    this.leaveService.getCurrentAnnualLeavePeriod()
      .subscribe(resp => {
        this.annualPeriod = resp.data;
      });
    this.onRequestChange({
      page: {
        page: 0,
        size: 10
      }
    })
  }

  onRequestChange(req: UserSearchRequest) {
    this.userService.searchUsers(req)
      .subscribe(resp => {
        this.userData = resp;
      });
  }
}
