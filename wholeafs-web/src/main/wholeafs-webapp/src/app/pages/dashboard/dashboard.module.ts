import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import {SharedModule} from "../../shared/shared.module";
import {DashboardPageComponent} from "./containers/dashboard-page/dashboard-page.component";
import {UserDashboardComponent} from './components/user-dashboard/user-dashboard.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {AdminDashboardComponent} from './components/admin-dashboard/admin-dashboard.component';
import {MatSelectModule} from "@angular/material/select";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {AdminUsersComponent} from './components/admin-users/admin-users.component';
import {SupervisorDashboardComponent} from './components/supervisor-dashboard/supervisor-dashboard.component';
import {FinanceDashboardComponent} from './components/finance-dashboard/finance-dashboard.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatMomentDateModule} from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    DashboardPageComponent,
    UserDashboardComponent,
    AdminDashboardComponent,
    AdminUsersComponent,
    SupervisorDashboardComponent,
    FinanceDashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatDividerModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
  ],
  exports: [
    AdminUsersComponent
  ],
  providers: []
})
export class DashboardModule {
}
