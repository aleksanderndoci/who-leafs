import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {UserModel, UserSearchRequest} from "../../../../shared/model/user-model";
import {PagedResponse} from "../../../../shared/model/http/paged-response";
import {Role} from "../../../../constant/role";
import {BtnAction, TableBtn, TableCol} from "../../../../shared/components/table/table.component";
import {PageRequest} from "../../../../shared/model/http/page-request";

export interface UserDataTable extends UserModel {
  role?: {
    role: string
  }
}

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit, OnChanges {
  userSearchRequest: UserSearchRequest;

  @Input() cardTitle: string;
  @Input() fullSearchForm = false;
  @Input() actionButtons = false;
  @Input() userData: PagedResponse<UserModel>;
  @Output() requestChanged = new EventEmitter<UserSearchRequest>();
  @Output() onEditUser = new EventEmitter<UserModel>();
  @Output() onRemoveUser = new EventEmitter<UserModel>();

  operationButtons: TableBtn[] = [{
    id: 'edit',
    color: 'primary',
    label: 'Edit',
    icon: 'dehaze',
    disabled: true
  }, {
    id: 'remove',
    color: 'warn',
    label: 'Delete',
    icon: 'clear',
    disabled: false
  }
  ];

  userDataTransformed: UserDataTable[];

  tableCols: TableCol[] = [
    {key: 'name', displayAs: 'Name'},
    {key: 'surname', displayAs: 'Surname'},
    {key: 'userRole', displayAs: 'Role', sortKey: "role.role"},
    {key: 'username', displayAs: 'Username'},
    {key: 'email', displayAs: 'Email'},
    {key: 'accountStatus', displayAs: 'Status'},
    {key: 'insertedAt', displayAs: 'Created At', dataType: 'dateTime'}
  ];

  constructor() {
  }

  ngOnInit(): void {
    this.userSearchRequest = {
      page: {
        page: 0,
        size: 10
      }
    };
    this.getUserData();
  }


  onPageRequestChange(request: PageRequest) {
    this.userSearchRequest = {
      ...this.userSearchRequest,
      page: request.page,
      sort: request.sort
    };
    this.getUserData();
  }

  getUserData() {
    this.requestChanged.emit(this.userSearchRequest);
  }

  searchUserForm(request: UserSearchRequest) {
    this.userSearchRequest = request;
    this.getUserData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.userData) {
      this.userDataTransformed = this.userData.results
        .map(value => {
          return {
            ...value,
            userRole: Role[value.role.role],
          }
        });
    }
  }

  onRemoveUserEmit(btnAction: BtnAction<UserModel>) {
    if (btnAction.btnId === 'remove') {
      this.onRemoveUser.emit(btnAction.data);
    }
  }

}
