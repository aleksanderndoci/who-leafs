import {Component, OnInit} from '@angular/core';
import {
  DetailedLeaveView,
  LeavePieChart,
  LeaveSearchRequest,
  PeriodRequest
} from "../../../../shared/model/leave-model";
import {colors} from "../../../../constant";
import {FinanceService} from "../../../../shared/services/finance.service";
import * as moment from 'moment';
import {TableCol} from "../../../../shared/components/table/table.component";
import {PagedResponse} from "../../../../shared/model/http/paged-response";
import {PageRequest} from "../../../../shared/model/http/page-request";

@Component({
  selector: 'app-finance-dashboard',
  templateUrl: './finance-dashboard.component.html',
  styleUrls: ['./finance-dashboard.component.scss']
})
export class FinanceDashboardComponent implements OnInit {
  leaveTypePieChart: LeavePieChart;
  leavePieChartColors: [colors.YELLOW, colors.PINK, colors.GREEN, colors.BLUE, colors.VIOLET];
  periodRequest: PeriodRequest;
  leavePageRequest: LeaveSearchRequest;
  displayTableCols: TableCol[];
  leaveData: PagedResponse<DetailedLeaveView>;

  constructor(private financeService: FinanceService) {
  }

  ngOnInit(): void {
    this.initMontLeaveData();
    const toDate = moment();
    const fromDate = moment();
    fromDate.set('year', toDate.get('year') - 1);
    this.periodRequest = {
      from: fromDate.toDate(),
      to: toDate.toDate()
    };
    this.getLeaveTypeChartData();
  }

  getLeaveTypeChartData() {
    this.financeService.getLeaveTypeChart(this.periodRequest)
      .subscribe(resp => {
        this.leaveTypePieChart = resp.data;
      });
  }


  onLeaveTypePieChartClick() {
    this.getLeaveTypeChartData();
  }

  private initMontLeaveData() {
    this.displayTableCols = [{
      key: 'periodTitle',
      displayAs: 'Period',
      sortKey: 'insertedAt'
    }, {
      key: 'startDate',
      displayAs: 'From',
      dataType: 'dateTime',
      sortKey: 'startDate'
    }, {
      key: 'endDate',
      displayAs: 'To',
      dataType: 'dateTime',
      sortKey: 'endDate'
    }, {
      key: 'fullName',
      displayAs: 'User',
      sortKey: 'userLeavePeriod.user.name'
    }, {
      key: 'hours',
      displayAs: 'Total hours',
      sortKey: 'hours'
    }, {
      key: 'leaveType',
      displayAs: 'Leave type',
      sortKey: 'leaveType'
    }, {
      key: 'reason',
      displayAs: 'Leave reason',
      sortKey: 'reason'
    }];

    let today = moment();
    let monthStart = moment().startOf('month');
    this.leavePageRequest = {
      startDate: monthStart.toDate(),
      endDate: today.toDate(),
      page: {
        page: 0,
        size: 10
      }
    };
    this.refreshTableData();
  }

  private refreshTableData() {
    this.financeService.searchLeave(this.leavePageRequest)
      .subscribe(resp => {
        this.leaveData = resp;
      })
  }

  onPageSortChange(pageRequest: PageRequest) {
    this.leavePageRequest = {
      ...this.leavePageRequest,
      ...pageRequest
    };
    this.refreshTableData();
  }
}
