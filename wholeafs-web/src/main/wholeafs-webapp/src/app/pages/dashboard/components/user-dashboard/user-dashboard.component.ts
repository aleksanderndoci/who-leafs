import {Component, OnInit} from '@angular/core';
import {colors} from "../../../../constant";
import {LeaveModel, LeaveStatusEnum} from "../../../../shared/model/leave-model";
import {LeaveService} from "../../../../shared/services/leave-service";
import {PieChartData} from "../../../../shared/model/charts/pie-chart-model";

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {
  chartColors: Array<string>;
  pieChartLabel: string;
  pieChartData: PieChartData[];

  viewMoreDisabled = false;

  ongoingLeave: LeaveModel[];

  constructor(private leaveService: LeaveService) {
  }

  ngOnInit(): void {
    this.leaveService.getLoggedUserLeave({
      page: 0,
      size: 1
    }).subscribe(resp => {
      this.ongoingLeave = resp.results;
    });
    this.chartColors = [colors.PINK, colors.GREEN, colors.VIOLET];
    this.leaveService.getStatusChart()
      .subscribe(resp => {
        const leavePieChart = resp.data;
        this.pieChartLabel = leavePieChart.periodTitle;
        this.pieChartData = leavePieChart.values;
        this.chartColors = this.generateColors(this.pieChartData);
      })
  }

  private generateColors(values: Array<PieChartData>): Array<string> {
    if (!values) return [];
    return values.map(value => {
      return this.getColor(value.key);
    })
  }

  getColor(leaveStatus: string): string {
    switch (leaveStatus) {
      case LeaveStatusEnum.SENT:
        return colors.VIOLET;
      case LeaveStatusEnum.DRAFT:
        return colors.LIGHT_BLUE;
      case LeaveStatusEnum.ACCEPTED:
        return colors.BLUE;
      case LeaveStatusEnum.PENDING:
        return colors.YELLOW;
      case LeaveStatusEnum.CANCELED:
      case LeaveStatusEnum.REFUSED:
        return colors.PINK;
      default:
        return colors.GREEN;
    }
  }

  viewMoreLeaves() {
    this.leaveService.getLoggedUserLeave({
      page: 0,
      size: 3
    }).subscribe(resp => {
      this.ongoingLeave = resp.results;
      this.viewMoreDisabled = true;
    });
  }

  cancelLeave(guid: string) {
    this.leaveService.cancelLeave(guid)
      .subscribe(resp => {
        this.ongoingLeave = this.ongoingLeave
          .map(leave => {
            if (leave.guid === resp.data.guid) {
              leave.leaveStatus = LeaveStatusEnum.CANCELED
            }
            return leave;
          })
      })
  }
}
