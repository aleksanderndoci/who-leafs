import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuard} from "./login/guards";
import {ApplyLeavePageComponent} from "./apply-leave-page/apply-leave-page.component";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {routes as routesConst} from "../constant";
import {AdminUsersPageComponent} from "./admin-users-page/admin-users-page.component";
import {AddUserPageComponent} from "./add-user-page/add-user-page.component";
import {DashboardPageComponent} from "./dashboard/containers/dashboard-page/dashboard-page.component";
import {FinanceReportsComponent} from "./finance-reports/finance-reports.component";


const getRoute = (route: string) => {
  return route.substring(1);
};


const routes: Routes = [
  {
    path: getRoute(routesConst.APPLY_LEAVE),
    canActivate: [AuthGuard],
    component: ApplyLeavePageComponent
  }, {
    path: getRoute(routesConst.PROFILE),
    canActivate: [AuthGuard],
    component: UserProfileComponent
  }, {
    path: getRoute(routesConst.DASHBOARD_USERS),
    canActivate: [AuthGuard],
    component: AdminUsersPageComponent
  },
  {
    path: getRoute(routesConst.ADD_USERS),
    canActivate: [AuthGuard],
    component: AddUserPageComponent
  },
  {
    path: getRoute(routesConst.PENDING_LEAVE),
    canActivate: [AuthGuard],
    component: DashboardPageComponent
  }, {
    path: getRoute(routesConst.LEAVE_HISTORY),
    canActivate: [AuthGuard],
    component: DashboardPageComponent
  }, {
    path: getRoute(routesConst.LEAVE_REPORTS),
    canActivate: [AuthGuard],
    component: FinanceReportsComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class PagesRoutingModule {
}
