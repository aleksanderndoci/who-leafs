import {AfterViewInit, Component} from '@angular/core';
import {Router} from "@angular/router";
import {routes} from "../../constant";
import {UserService} from "../../shared/services/user.service";
import {ChangePasswordRequest, UserModel} from "../../shared/model/user-model";
import {NotificationService} from "../../shared/components/notification";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements AfterViewInit {
  loggedUser: UserModel;

  constructor(private router: Router,
              private userService: UserService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.userService.getCurrentUser()
      .subscribe(resp => {
        this.loggedUser = resp.data;
      });
  }

  goToHome() {
    this.router.navigate([routes.DASHBOARD])
  }

  onChangePassword(event: ChangePasswordRequest) {
    this.userService.changePassword(event)
      .subscribe(resp => {
        const pswResp = resp.data;
        if (!pswResp.wasReset) {
          this.notificationService.error({
            title: 'Oops!',
            message: pswResp.reason
          })
        } else {
          this.notificationService.success({
            title: 'Success!',
            message: 'Password changed successfully'
          });
          this.router.navigate([routes.DASHBOARD]);
        }
      })
  }

  onProfileEdit(model: UserModel) {
    this.userService.editProfileData(model)
      .subscribe(() => {
        this.notificationService.success({
          title: '',
          message: 'Profile updated successfully'
        });
        this.router.navigate([routes.DASHBOARD]);
      })
  }

  ngAfterViewInit(): void {
  }
}
