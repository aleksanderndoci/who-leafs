/*
 * Copyright (c) Aleksander Ndoci
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesRoutingModule} from "./pages-routing.module";
import {ApplyLeavePageComponent} from "./apply-leave-page/apply-leave-page.component";
import {SharedModule} from "../shared/shared.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {UserProfileComponent} from './user-profile/user-profile.component';
import {MatCardModule} from "@angular/material/card";
import {MatTabsModule} from "@angular/material/tabs";
import { AdminUsersPageComponent } from './admin-users-page/admin-users-page.component';
import {DashboardModule} from "./dashboard/dashboard.module";
import { AddUserPageComponent } from './add-user-page/add-user-page.component';
import { FinanceReportsComponent } from './finance-reports/finance-reports.component';
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [
    ApplyLeavePageComponent,
    UserProfileComponent,
    AdminUsersPageComponent,
    AddUserPageComponent,
    FinanceReportsComponent
  ],
    imports: [
        CommonModule,
        PagesRoutingModule,
        SharedModule,
        MatToolbarModule,
        MatButtonModule,
        MatDividerModule,
        MatCardModule,
        MatTabsModule,
        DashboardModule,
        MatIconModule
    ]
})
export class PagesModule {
}
