import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {routes} from "../../constant";
import {UserModel} from "../../shared/model/user-model";
import {UserService} from "../../shared/services/user.service";
import {NotificationService} from "../../shared/components/notification";

@Component({
  selector: 'app-add-user-page',
  templateUrl: './add-user-page.component.html',
  styleUrls: ['./add-user-page.component.scss']
})
export class AddUserPageComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate([routes.DASHBOARD_USERS]);
  }

  createUser(user: UserModel) {
    this.userService.createUser(user)
      .subscribe(resp => {
        this.notificationService.success({
          title: 'Success',
          message: `User ${resp.data.username} created`
        });
        this.router.navigate([routes.DASHBOARD_USERS]);
      })
  }
}
