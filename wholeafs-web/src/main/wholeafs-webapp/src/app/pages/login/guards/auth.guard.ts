import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {routes} from "../../../constant";
import {HttpService} from "../../../shared/services/http/http.service";
import {NotificationService} from "../../../shared/components/notification";

@Injectable()
export class AuthGuard implements CanActivate {
  public routers: typeof routes = routes;

  constructor(private router: Router,
              private httpService: HttpService,
              private notificationService: NotificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.httpService.isAuthenticated()) {
      return true;
    } else {
      this.notificationService.error({
        title: 'Please log in!',
        message: 'Full authentication required to view this page.'
      });
      this.router.navigate([this.routers.LOGIN]);
    }
  }
}
