import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Configs, routes} from '../../../../constant';
import {LoginService} from "../../services";
import {LoginRequestModel} from "../../../../shared/model/login-model";
import {ResetPasswordRequestModel, UserModel} from "../../../../shared/model/user-model";
import {UserService} from "../../../../shared/services/user.service";
import {NotificationService} from "../../../../shared/components/notification";
import {HttpUtils} from "../../../../shared/utils/http-utils";
import {HttpService} from "../../../../shared/services/http/http.service";

@Component({
  selector: 'app-auth-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public todayDate: Date = new Date();
  public routers: typeof routes = routes;
  selectedTabIndex = 0;

  resetPassword: {
    show?: boolean,
    sentEmail?: boolean
  } = {};

  resetEmailGuid: string;
  greeting: string;

  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private httpService: HttpService,
    private notificationService: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
  }

  public sendLoginForm(loginRequestModel: LoginRequestModel): void {
    this.loginService.login(loginRequestModel)
      .subscribe(response => {
        this.httpService.storeCookie(Configs.TOKEN, response.token);
        this.httpService.storeCookie(Configs.AUTH_SCHEME, response.schema);
        this.router.navigate([this.routers.DASHBOARD]).then();
      });
  }

  register(userModel: UserModel) {
    this.userService.registerUser(userModel)
      .subscribe(resp => {
        this.notificationService.success({
          title: 'Yey!',
          message: 'You successfully registered. Check your email for confirmation'
        });
        this.selectedTabIndex = 0;
      });
  }

  onForgotPasswordClick() {
    this.resetPassword = {
      show: true
    }
  }

  onForgotPasswordSubmit(req: ResetPasswordRequestModel) {
    if (!this.resetPassword.sentEmail) {
      req = {
        ...req,
        baseUrl: `${HttpUtils.getHostBaseUrl()}/${this.routers.LOGIN}?resetPassword=true&reqGuid={guid}`
      };
      this.userService.resetPasswordRequest(req)
        .subscribe(response => {
          this.resetEmailGuid = response.data;
          this.resetPassword.sentEmail = true;
          this.notificationService.success({
            title: '',
            message: 'Password reset request was with details was sent to your email inbox!'
          })
        })
    } else {
      this.userService.resetPassword(this.resetEmailGuid, req)
        .subscribe(resp => {
          if (resp.data.wasReset) {
            this.resetPassword = {};
            this.notificationService.success({
              title: 'Did it!',
              message: 'Password was sucessfully changed. Please log in!'
            })
          } else {
            this.notificationService.error({
              title: 'Ouch!',
              message: resp.data.reason
            })
          }
        });
    }
  }

  ngOnInit(): void {
    this.setGreeting();
    this.activatedRoute.queryParamMap.subscribe(params => {
      if (params.get('resetPassword')) {
        this.resetEmailGuid = params.get('reqGuid');
        this.resetPassword = {
          show: true,
          sentEmail: true
        }
      }
    })
  }

  private setGreeting() {
    const hour = new Date().getUTCHours();
    let greeting = '';
    if (hour >= 4 && hour <= 10) {
      greeting = 'Good Morning';
    } else {
      if (hour < 18) {
        greeting = 'Good afternoon';
      } else {
        greeting = 'Good Evening';
      }
    }
    this.greeting = greeting + ', User';
  }

  goToLogin() {
    this.resetPassword = {};
  }
}
