export interface LoggedUser {
  name: string;
  surname: string;
  username: string,
  email: string,
  enabled: boolean,
  role: string
}
