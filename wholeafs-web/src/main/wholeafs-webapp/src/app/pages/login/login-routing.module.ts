import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {LoginPageComponent} from './containers';

const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  }, {
    path: '**',
    redirectTo: '/dashboard'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class LoginRoutingModule {
}
