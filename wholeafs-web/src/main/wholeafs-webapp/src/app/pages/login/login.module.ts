import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LoginPageComponent} from './containers';
import {LoginRoutingModule} from './login-routing.module';
import {YearPipe} from './pipes';
import {LoginService} from './services';
import {LoginFormComponent} from './components';
import {AuthGuard} from './guards';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import {SharedModule} from "../../shared/shared.module";
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';

@NgModule({
    declarations: [
        LoginPageComponent,
        YearPipe,
        LoginFormComponent,
        ResetPasswordComponent
    ],
    imports: [
        CommonModule,
        LoginRoutingModule,
        MatTabsModule,
        MatButtonModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule,
        MatCheckboxModule,
        MatDividerModule,
        SharedModule
    ],
    exports: [
        ResetPasswordComponent
    ],
    providers: [
        LoginService,
        AuthGuard
    ]
})
export class LoginModule {
}
