import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginRequestModel} from "../../../../shared/model/login-model";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() sendLoginForm = new EventEmitter<LoginRequestModel>();
  @Output() forgotPasswordClick = new EventEmitter<any>();
  public form: FormGroup;

  loginRequest: LoginRequestModel = {
    password: "",
    username: "",
    rememberMe: false
  };

  public ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl(this.loginRequest.username, [Validators.required]),
      password: new FormControl(this.loginRequest.password, [Validators.required]),
      rememberMe: new FormControl(this.loginRequest.rememberMe)
    });
  }

  public login(): void {
    if (this.form.valid) {
      this.sendLoginForm.emit({
        ...this.loginRequest,
        ...this.form.value
      })
    }
  }

  onForgotPasswordClick() {
    this.forgotPasswordClick.emit({});
  }
}
