import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ResetPasswordRequestModel} from "../../../../shared/model/user-model";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  @Input() sentEmail = false;
  @Output() formSubmit = new EventEmitter<ResetPasswordRequestModel>();
  @Output() onCancel = new EventEmitter<any>();

  public form: FormGroup;
  passwordResetRequest: ResetPasswordRequestModel = {};

  public ngOnInit(): void {
    this.form = new FormGroup({
        email: new FormControl(this.passwordResetRequest.email,
          !this.sentEmail ? [Validators.required, Validators.email] : []),
        password: new FormControl(this.passwordResetRequest.password,
          this.sentEmail ? [Validators.required] : []),
        confirmPassword: new FormControl(this.passwordResetRequest.confirmPassword,
          this.sentEmail ? [Validators.required] : []),
        resetPin: new FormControl(this.passwordResetRequest.resetPin,
          this.sentEmail ? [Validators.required] : [])
      },
      {validators: this.sentEmail ? checkPasswordsMatch : []});
  }

  onFormSubmit() {
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      this.formSubmit.emit({
        ...this.passwordResetRequest,
        ...this.form.value
      });
    }
  }

  onLoginClick() {
    this.onCancel.emit({});
  }
}

export function checkPasswordsMatch(c: AbstractControl) {
  const pwd = c.get('password').value;
  const confPwd = c.get('confirmPassword').value;
  if (!pwd || !confPwd || pwd !== confPwd) {
    return {passwordsDontMatch: true}
  }
  return {};
}
