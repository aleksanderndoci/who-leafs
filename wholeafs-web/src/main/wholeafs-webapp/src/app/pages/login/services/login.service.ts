import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LoginRequestModel, LoginResponseModel} from "../../../shared/model/login-model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) {
  }

  login(login: LoginRequestModel): Observable<LoginResponseModel> {
    return this.httpClient.post<LoginResponseModel>('auth/login', login);
  }
}
