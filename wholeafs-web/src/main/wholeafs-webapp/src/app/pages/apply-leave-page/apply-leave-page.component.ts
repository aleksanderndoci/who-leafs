import {Component, OnInit} from '@angular/core';
import {LeaveService} from "../../shared/services/leave-service";
import {NotificationService} from "../../shared/components/notification";
import {Router} from "@angular/router";
import {routes} from "../../constant";
import {LeaveRequest} from "../../shared/model/leave-model";

@Component({
  selector: 'app-apply-leave',
  templateUrl: './apply-leave-page.component.html',
  styleUrls: ['./apply-leave-page.component.scss']
})
export class ApplyLeavePageComponent implements OnInit {

  constructor(private leaveService: LeaveService,
              private router: Router,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  applyForLeave(request: LeaveRequest) {
    this.leaveService.applyForLeave(request)
      .subscribe(response => {
        if (response.data == null) {
          this.notificationService.error({
            title: 'Oops!',
            message: 'There was an error applying for leave. Please contact Administrator'
          });
        } else {
          this.notificationService.info({
            title: 'Info',
            message: 'Successfully applied for leave. Please wait for supervisor`s response'
          });
          this.router.navigate([routes.DASHBOARD]);
        }
      })
  }

  goToHome() {
    this.router.navigate([routes.DASHBOARD]);
  }
}
