import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {routes} from "../../constant";
import {PagedResponse} from "../../shared/model/http/paged-response";
import {UserModel, UserSearchRequest} from "../../shared/model/user-model";
import {UserService} from "../../shared/services/user.service";
import {NotificationService} from "../../shared/components/notification";

@Component({
  selector: 'app-admin-users-page',
  templateUrl: './admin-users-page.component.html',
  styleUrls: ['./admin-users-page.component.scss']
})
export class AdminUsersPageComponent implements OnInit {

  userData: PagedResponse<UserModel>;

  constructor(private router: Router,
              private userService: UserService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
  }

  onRequestChanged(request: UserSearchRequest) {
    this.userService.searchUsers(request)
      .subscribe(resp => {
        this.userData = resp;
      })
  }

  addUser() {
    this.router.navigate([routes.ADD_USERS]);
  }

  removeUser(event: UserModel) {
    this.userService.removeUser(event)
      .subscribe(() => {
        this.notificationService.info({
          title: 'Deleted',
          message: `User @${event.username} successfully deleted`
        });
        let newResults = this.userData.results
          .filter(usr => usr.guid !== event.guid);
        this.userData = {
          ...this.userData,
          results: newResults
        }
      })
  }
}
