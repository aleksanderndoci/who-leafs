import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {routes} from "../../constant";
import {DetailedLeaveView, LeaveSearchRequest} from "../../shared/model/leave-model";
import {FinanceService} from "../../shared/services/finance.service";
import {PagedResponse} from "../../shared/model/http/paged-response";
import {TableCol} from "../../shared/components/table/table.component";
import {PageRequest} from "../../shared/model/http/page-request";
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-finance-reports',
  templateUrl: './finance-reports.component.html',
  styleUrls: ['./finance-reports.component.scss']
})
export class FinanceReportsComponent implements OnInit {
  leaveSearchReq: LeaveSearchRequest;
  leaveData: PagedResponse<DetailedLeaveView>;
  tableCols: TableCol[];

  constructor(private router: Router,
              private financeService: FinanceService) {
  }

  ngOnInit(): void {
    this.leaveSearchReq = {
      page: {
        page: 0,
        size: 10
      }
    };
    this.initTableCols();
    this.updateTableData();
  }

  home() {
    this.router.navigate([routes.DASHBOARD]);
  }

  onSearchClick(req: LeaveSearchRequest) {
    this.leaveSearchReq = {
      ...this.leaveSearchReq,
      ...req
    };
    this.updateTableData();
  }

  private updateTableData() {
    this.financeService.searchLeave(this.leaveSearchReq)
      .subscribe(resp => {
        this.leaveData = resp;
      })
  }

  private initTableCols() {
    this.tableCols = [{
      key: 'periodTitle',
      displayAs: 'Period',
      sortKey: 'insertedAt'
    }, {
      key: 'startDate',
      displayAs: 'From',
      dataType: 'dateTime',
      sortKey: 'startDate'
    }, {
      key: 'endDate',
      displayAs: 'To',
      dataType: 'dateTime',
      sortKey: 'endDate'
    }, {
      key: 'fullName',
      displayAs: 'User',
      sortKey: 'userLeavePeriod.user.name'
    }, {
      key: 'hours',
      displayAs: 'Total hours',
      sortKey: 'hours'
    }, {
      key: 'leaveType',
      displayAs: 'Leave type',
      sortKey: 'leaveType'
    }, {
      key: 'reason',
      displayAs: 'Leave reason',
      sortKey: 'reason'
    }];
  }

  onPageSortChange(pageRequest: PageRequest) {
    this.leaveSearchReq = {
      ...this.leaveSearchReq,
      ...pageRequest
    };
    this.updateTableData();
  }

  onExcelDownloadClick() {
    this.financeService.downloadExcelReport(this.leaveSearchReq)
      .subscribe(resp => {
        const file = new File([resp], 'Excel_Report.xlsx', {type: 'application/vnd.ms.excel'});
        saveAs(file);
      })
  }
}
