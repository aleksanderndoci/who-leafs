export class DateUtils {
  public static formatDateTime(strDate: string | Date, defaultIfNull?: string): string {
    if (!strDate) {
      return defaultIfNull ? defaultIfNull : '-';
    }
    let date;
    if (strDate instanceof Date) {
      date = strDate;
    } else {
      date = new Date(strDate);
    }
    return `${DateUtils.padLeft(date.getDate())}-${DateUtils.padLeft(date.getMonth())}-${date.getFullYear()} ${DateUtils.padLeft(date.getHours())}:${DateUtils.padLeft(date.getMinutes())}`;
  }

  static padLeft(nr: number): string {
    const pad = "00";
    const str = nr + '';
    return str ? (pad.substring(0, pad.length - str.length) + str) : pad;
  }
}
