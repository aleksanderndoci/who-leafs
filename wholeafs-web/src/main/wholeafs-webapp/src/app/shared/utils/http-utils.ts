/*
 * Copyright (c) Aleksander Ndoci
 */

export class HttpUtils {

  public static getHostBaseUrl(): string {
    return `http://${window.location.hostname}:${window.location.port}`;
  }

}
