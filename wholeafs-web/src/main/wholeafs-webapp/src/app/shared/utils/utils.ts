/*
 * Copyright (c) Aleksander Ndoci
 */

import {LoggedUser} from "../../pages/login/models";

export class Utils {
  public static getFullName(user: LoggedUser): string {
    return user ? user.name + ' ' + user.surname : null;
  }

  public static enumToArray(input: Object): { id, value }[] {
    return Object.keys(input).map(key => {
      return {
        id: key,
        value: input[key]
      };
    });
  }
}
