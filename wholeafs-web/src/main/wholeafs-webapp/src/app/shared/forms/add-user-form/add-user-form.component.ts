import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountStatus, UserModel} from "../../model/user-model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Utils} from "../../utils/utils";
import {Role} from "../../../constant/role";

@Component({
  selector: 'app-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.scss']
})
export class AddUserFormComponent implements OnInit {
  @Input() user: UserModel;
  @Output() onFormSubmit = new EventEmitter<UserModel>();
  roleForm: FormGroup;
  isEditForm: any;
  _roles: { id; value }[];

  constructor() {
  }

  ngOnInit(): void {
    this._roles = Utils.enumToArray(Role);
    this.roleForm = new FormGroup({
      role: new FormControl(this.user?.role.role || Role.USER, [Validators.required])
    });
  }

  onSubmit(user: UserModel) {
    this.user = {
      ...this.user,
      ...user,
      role: {
        role: this.roleForm.value.role
      },
      accountStatus: AccountStatus.ACTIVE
    };
    this.onFormSubmit.emit(this.user);
  }
}
