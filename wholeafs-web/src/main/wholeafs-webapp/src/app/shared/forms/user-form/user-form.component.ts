import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UserModel} from "../../model/user-model";
import {UsernameValidator} from "./username-validator";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Output() sendUserForm = new EventEmitter<UserModel>();
  @Input() isEditForm = false;

  public form: FormGroup;

  @Input() user: UserModel;

  constructor(public usernameValidator: UsernameValidator) {
  }

  public ngOnInit(): void {
    this.initUser();
    let user = this.user;
    this.form = new FormGroup({
      name: new FormControl(user.name, [Validators.required]),
      surname: new FormControl(user.surname, [Validators.required]),
      email: new FormControl(user.email,
        Validators.compose([Validators.required, Validators.email]),
        this.usernameValidator.checkUsername.bind(this.usernameValidator)
      ),
      username: new FormControl(user.username,
        Validators.compose([Validators.required]),
        this.usernameValidator.checkUsername.bind(this.usernameValidator)),
      password: new FormControl(user.password,
        this.ifNotEdit([Validators.required, Validators.minLength(8), Validators.maxLength(20)]))
    });
  }


  ifNotEdit(validators: Array<ValidatorFn>) {
    return this.isEditForm ? [] : validators;
  }


  public formSubmit(): void {
    if (this.form.valid) {
      this.sendUserForm.emit({
        ...this.user,
        ...this.form.value
      });
    }
  }

  private initUser() {
    if (!this.user) {
      this.user = {accountStatus: null, email: "", name: "", surname: "", username: ""};
    }
  }
}
