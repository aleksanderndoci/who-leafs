import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {ChangePasswordRequest} from "../../model/user-model";

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.scss']
})
export class ChangePasswordFormComponent implements OnInit {
  form: FormGroup;
  changePasswordRequest: ChangePasswordRequest;
  @Output() formSubmit = new EventEmitter<ChangePasswordRequest>();

  constructor() {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      password: new FormControl('', Validators.required),
      newPassword: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
    }, {validators: [checkPasswordsMatch]});
  }

  onFormSubmit() {
    if (this.form.valid) {
      const form = this.form.value;
      this.formSubmit.emit({
        ...this.changePasswordRequest,
        ...form
      })
    }
  }
}

export function checkPasswordsMatch(c: AbstractControl) {
  const pwd = c.get('confirmPassword').value;
  const confPwd = c.get('newPassword').value;
  if (!pwd || !confPwd || pwd !== confPwd) {
    return {passwordsDontMatch: true}
  }
  return {};
}
