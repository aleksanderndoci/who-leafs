import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {LeaveApprover, LeaveRequest, LeaveTypeEnum} from "../../model/leave-model";
import {ENTER} from "@angular/cdk/keycodes";
import {Utils} from "../../utils/utils";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import * as moment from "moment";
import {LeaveService} from "../../services/leave-service";

@Component({
  selector: 'app-apply-leave-form',
  templateUrl: './apply-leave-form.component.html',
  styleUrls: ['./apply-leave-form.component.scss']
})
export class ApplyLeaveFormComponent implements OnInit {
  @Output() onFormSubmit = new EventEmitter<LeaveRequest>();

  form: FormGroup;
  availableApprovers: LeaveApprover[] = [];

  selectedApprovers: LeaveApprover[] = [];

  leaveRequest: LeaveRequest = {
    endDate: this.getDefaultDate(16),
    approverGuids: [],
    leaveType: null,
    reason: "",
    startDate: this.getDefaultDate(8)
  };

  startDateControl: FormControl;
  endDateControl: FormControl;
  leaveApprovers: Array<string> = [];
  readonly separatorKeysCodes: number[] = [ENTER];
  leaveTypes: { id, value }[] = [];

  constructor(private leaveService: LeaveService) {

  }

  ngOnInit(): void {
    this.startDateControl = new FormControl(this.leaveRequest.startDate, Validators.required);
    this.endDateControl = new FormControl(this.leaveRequest.endDate, Validators.required);
    this.form = new FormGroup({
      startDate: this.startDateControl,
      endDate: this.endDateControl,
      supervisorName: new FormControl(''),
      leaveType: new FormControl(this.leaveRequest.leaveType, Validators.required),
      reason: new FormControl(this.leaveRequest.reason, Validators.required)
    }, {validators: [checkValidDates]});
    this.leaveTypes = Utils.enumToArray(LeaveTypeEnum);
  }

  formSubmit() {
    if (!this.form.errors) {
      const form = this.form.value;
      this.leaveRequest = {
        endDate: this.endDateControl.value,
        approverGuids: this.leaveApprovers,
        leaveType: form.leaveType,
        reason: form.reason,
        startDate: this.startDateControl.value
      };
      this.onFormSubmit.emit(this.leaveRequest);
    }
  }

  removeApprover(approver: LeaveApprover) {
    this.leaveApprovers = this.leaveApprovers
      .filter(lap => lap !== approver.guid);
    this.selectedApprovers = this.selectedApprovers
      .filter(lap => lap.guid !== approver.guid);
  }

  selected(event: MatAutocompleteSelectedEvent) {
    const guid = event.option.value;
    if ((guid || '').trim()) {
      const selectedApprover = this.availableApprovers
        .filter(app => app.guid === guid);
      this.selectedApprovers = [
        ...this.selectedApprovers,
        ...selectedApprover
      ];
      this.leaveApprovers = [
        ...this.leaveApprovers,
        guid
      ]
    }
  }

  searchForApprover(event: any) {
    const name = event.value;
    this.leaveService
      .getAvailableApprovers({
        fullName: name,
        page: {
          page: 0,
          size: 10
        }
      })
      .subscribe(resp => {
        if (resp.totalElements === 0) {
          this.form.get('supervisorName').setErrors({noApproverFound: true});
        } else {
          event.input.value = '';
        }
        this.availableApprovers = resp.results;
      })
  }


  getDefaultDate(hour: number): Date {
    let date = moment(new Date());
    date.set('hours', hour);
    date.set('minutes', 0);
    date.set('seconds', 0);
    return date.toDate();
  }
}

export function checkValidDates(c: AbstractControl) {
  const startDate = moment(c.get('startDate').value);
  const endDate = moment(c.get('endDate').value);
  if (startDate.isAfter(endDate)) {
    return {endDateInvalid: true};
  }
  if ((startDate.hour() < 8 || startDate.hour() > 18) ||
    (endDate.hour() < 8 || endDate.hour() > 18)) {
    return {startHourInvalid: true}
  }
  return {};
}
