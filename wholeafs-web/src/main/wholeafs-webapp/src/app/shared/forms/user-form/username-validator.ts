/*
 * Copyright (c) Aleksander Ndoci
 */

import {FormControl} from '@angular/forms';
import {Injectable} from "@angular/core";
import {UserService} from "../../services/user.service";

@Injectable()
export class UsernameValidator {

  debouncer: any;

  constructor(public userService: UserService) {
  }

  checkUsername(control: FormControl): any {
    clearTimeout(this.debouncer);
    return new Promise(resolve => {
      this.debouncer = setTimeout(() => {
        this.userService.userExists(control.value).subscribe((res) => {
          if (res.data.exists) {
            resolve({'usernameInUse': true});
          } else {
            resolve(null);
          }
        }, (err) => {
          resolve({'usernameInUse': true});
        });
      }, 1000);
    });
  }

}
