import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Utils} from "../../utils/utils";
import {FormControl, FormGroup} from "@angular/forms";
import {LeaveSearchRequest, LeaveTypeEnum} from "../../model/leave-model";

@Component({
  selector: 'app-search-leave-form',
  templateUrl: './search-leave-form.component.html',
  styleUrls: ['./search-leave-form.component.scss']
})
export class SearchLeaveFormComponent implements OnInit {
  @Input() leaveSearchReq: LeaveSearchRequest;
  @Output() onSearchClick = new EventEmitter<LeaveSearchRequest>();

  form: FormGroup;

  leaveTypes_: { id; value }[];

  constructor() {
  }

  ngOnInit(): void {
    this.leaveTypes_ = Utils.enumToArray(LeaveTypeEnum);
    this.form = new FormGroup({
      startDate: new FormControl(this.leaveSearchReq.startDate),
      endDate: new FormControl(this.leaveSearchReq.endDate),
      leaveType: new FormControl(this.leaveSearchReq.leaveType),
    });
  }

  onSearchEmit() {
    this.leaveSearchReq = {
      ...this.leaveSearchReq,
      ...this.form.value
    };
    this.onSearchClick.emit(this.leaveSearchReq);
  }

  onClear() {
    this.form.reset();
  }

}
