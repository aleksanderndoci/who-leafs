import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountStatus, UserSearchRequest} from "../../model/user-model";
import {Role} from "../../../constant/role";
import {Utils} from "../../utils/utils";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-search-user-form',
  templateUrl: './search-user-form.component.html',
  styleUrls: ['./search-user-form.component.scss']
})
export class SearchUserFormComponent implements OnInit {
  @Input() userSearch: UserSearchRequest;
  @Input() fullSearch: boolean;
  @Output() onSearchClick = new EventEmitter<UserSearchRequest>();

  form: FormGroup

  _roles: { id; value }[];
  _accStatuses: { id; value }[];

  constructor() {
  }

  ngOnInit(): void {
    this._roles = Utils.enumToArray(Role);
    this._accStatuses = Utils.enumToArray(AccountStatus);
    this.form = new FormGroup({
      fullName: new FormControl(null),
      username: new FormControl(null),
      email: new FormControl(null),
      role: new FormControl(null),
      accountStatus: new FormControl(null)
    });
  }

  onSearchEmit() {
    this.userSearch = {
      ...this.userSearch,
      ...this.form.value
    };
    this.onSearchClick.emit(this.userSearch);
  }

  onClear() {
    this.form.reset();
  }

}
