/*
 * Copyright (c) Aleksander Ndoci
 */

import {NgModule} from '@angular/core';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {CommonModule} from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';

import {HeaderModule} from './components/header/header.module';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {DateMenuComponent, SettingsMenuComponent} from './ui-elements';
import {LayoutComponent} from './layout/layout.component';
import {UserFormComponent} from "./forms/user-form/user-form.component";
import {MatInputModule} from "@angular/material/input";
import {TruncatePipe} from "./pipes/truncate-pipe";
import {UsernameValidator} from "./forms/user-form/username-validator";
import {PieChartComponent} from "./components/pie-chart/pie-chart.component";
import {NgApexchartsModule} from "ng-apexcharts";
import {SingleLeaveComponent} from './components/single-leave/single-leave.component';
import {MatCardModule} from "@angular/material/card";
import {BadgeComponent} from './ui-elements/badge/badge.component';
import {FullnamePipe} from "./pipes/fullname-pipe";
import {DateTimePipe} from "./pipes/date-time-pipe";
import {IsoDatePipe} from "./pipes/date-pipe";
import {ApplyLeaveFormComponent} from './forms/apply-leave-form/apply-leave-form.component';
import {NgxMatDatetimePickerModule} from "@angular-material-components/datetime-picker";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatChipsModule} from "@angular/material/chips";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {ChangePasswordFormComponent} from './forms/change-password-form/change-password-form.component';
import {SimplePieChartComponent} from "./components/simple-pie-chart/simple-pie-chart.component";
import {NgxEchartsModule} from "ngx-echarts";
import {TableComponent} from './components/table/table.component';
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {SearchUserFormComponent} from './forms/search-user-form/search-user-form.component';
import {AddUserFormComponent} from './forms/add-user-form/add-user-form.component';
import {MatStepperModule} from "@angular/material/stepper";
import {MatTooltipModule} from "@angular/material/tooltip";
import {RefuseLeaveModalComponent} from './components/refuse-leave-modal/refuse-leave-modal.component';
import {MatDialogModule} from "@angular/material/dialog";
import {SearchLeaveFormComponent} from "./forms/search-leave-form/search-leave-form.component";

const components = [
  SidebarComponent,
  FooterComponent,
  SettingsMenuComponent,
  DateMenuComponent,
  LayoutComponent,
  PieChartComponent,
  SingleLeaveComponent,
  BadgeComponent,
  ApplyLeaveFormComponent,
  SimplePieChartComponent,
  TableComponent,
  RefuseLeaveModalComponent
];

const pipes = [
  TruncatePipe,
  FullnamePipe,
  DateTimePipe,
  IsoDatePipe
];

const forms = [
  UserFormComponent,
  ChangePasswordFormComponent,
  ApplyLeaveFormComponent,
  SearchUserFormComponent,
  AddUserFormComponent,
  SearchLeaveFormComponent
];

@NgModule({
  declarations: [
    ...components,
    ...pipes,
    ...forms
  ],
  imports: [
    HeaderModule,
    MatListModule,
    MatIconModule,
    RouterModule,
    MatButtonModule,
    MatInputModule,
    CommonModule,
    MatMenuModule,
    MatSelectModule,
    FormsModule,
    MatSidenavModule,
    ReactiveFormsModule,
    MatInputModule,
    NgApexchartsModule,
    MatCardModule,
    NgxMatDatetimePickerModule,
    MatDatepickerModule,
    MatChipsModule,
    MatAutocompleteModule,
    NgxEchartsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatStepperModule,
    MatTooltipModule,
    MatDialogModule
  ],
  exports: [
    HeaderModule,
    SidebarComponent,
    FooterComponent,
    SettingsMenuComponent,
    DateMenuComponent,
    LayoutComponent,
    UserFormComponent,
    TruncatePipe,
    PieChartComponent,
    SingleLeaveComponent,
    ApplyLeaveFormComponent,
    ChangePasswordFormComponent,
    BadgeComponent,
    SimplePieChartComponent,
    TableComponent,
    DateTimePipe,
    IsoDatePipe,
    SearchUserFormComponent,
    AddUserFormComponent,
    SearchLeaveFormComponent,
  ],
  providers: [UsernameValidator]
})
export class SharedModule {
}
