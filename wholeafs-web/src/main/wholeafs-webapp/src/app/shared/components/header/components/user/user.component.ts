import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LoggedUser} from "../../../../../pages/login/models";
import {Utils} from "../../../../utils/utils";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {
  @Input() user: LoggedUser = {enabled: false, email: "", username: "", name: "", role: "", surname: ""};
  @Output() signOut: EventEmitter<void> = new EventEmitter<void>();
  @Output() profileClick: EventEmitter<void> = new EventEmitter<void>();

  public _utils = Utils;

  public signOutEmit(): void {
    this.signOut.emit();
  }

  profileEmit() {
    this.profileClick.emit();
  }
}
