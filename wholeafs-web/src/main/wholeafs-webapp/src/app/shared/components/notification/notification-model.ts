/*
 * Copyright (c) Aleksander Ndoci
 */

export interface NotificationModel {
  title: string,
  message: string,
}
