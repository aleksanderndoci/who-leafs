import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexGrid,
  ApexLegend,
  ApexMarkers,
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexStroke,
  ApexTooltip,
  ApexXAxis
} from 'ng-apexcharts';

import {colors} from '../../../constant';
import {PieChartData} from "../../model/charts/pie-chart-model";

type ChartOptions = {
  series: ApexAxisChartSeries | ApexNonAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
  colors: string[];
  markers: ApexMarkers;
  grid: ApexGrid;
  labels: string[];
  responsive: ApexResponsive[];
  fill: ApexFill;
};

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, OnChanges {

  @Input() pieChartData: PieChartData[];
  @Input() pieChartColors: Array<string>;
  public apexPieChartOptions: Partial<ChartOptions>;
  public colors: typeof colors = colors;

  chartData: {
    labels: Array<string>,
    series: Array<number>
  } = {labels: [], series: []};

  public ngOnInit(): void {
    this.initChart();
  }

  public initChart(): void {
    if (this.pieChartData) {
      const labels = this.pieChartData.map(data => data.key);
      const series = this.pieChartData.map(data => data.value);
      this.chartData = {
        labels: labels,
        series: series
      }
    }
    this.apexPieChartOptions = {
      series: this.chartData.series,
      chart: {
        type: 'donut',
        height: 400
      },
      colors: this.pieChartColors,
      legend: {
        position: 'bottom',
        itemMargin: {
          horizontal: 5,
          vertical: 30
        },
      },
      labels: this.chartData.labels
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.pieChartData = [
      ...this.pieChartData
    ];
    this.initChart();
  }
}
