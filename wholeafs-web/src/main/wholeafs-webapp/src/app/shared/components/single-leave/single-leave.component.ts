import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LeaveModel, LeaveStatusEnum} from "../../model/leave-model";

@Component({
  selector: 'app-single-leave',
  templateUrl: './single-leave.component.html',
  styleUrls: ['./single-leave.component.scss']
})
export class SingleLeaveComponent implements OnInit {
  @Input() cardClass: string;
  @Input() leave: LeaveModel;
  @Output() leaveCancel = new EventEmitter<string>();

  showCancelBtn: boolean;

  constructor() {
  }

  ngOnInit(): void {
    this.showCancelBtn =
      this.leave && this.leave.leaveStatus !== LeaveStatusEnum.ACCEPTED &&
      this.leave.leaveStatus !== LeaveStatusEnum.REFUSED &&
      this.leave.leaveStatus !== LeaveStatusEnum.CANCELED;
  }

  getColor(status: LeaveStatusEnum) {
    switch (status) {
      case LeaveStatusEnum.ACCEPTED:
        return 'green';
      case LeaveStatusEnum.CANCELED:
        return 'pink';
      case LeaveStatusEnum.REFUSED:
        return 'pink';
      case LeaveStatusEnum.PENDING:
        return 'yellow';
      default:
        return 'blue';
    }
  }

  getIcon(status: LeaveStatusEnum) {
    switch (status) {
      case LeaveStatusEnum.ACCEPTED:
        return 'check_circle_outline';
      case LeaveStatusEnum.REFUSED:
        return 'cancel';
      case LeaveStatusEnum.PENDING:
        return 'pending';
      default:
        return 'error_outline';
    }
  }

  onCancel(guid: string) {
    this.leaveCancel.emit(guid);
  }

}
