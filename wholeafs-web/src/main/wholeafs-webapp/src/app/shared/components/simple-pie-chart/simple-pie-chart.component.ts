import {Component, Input, OnInit} from '@angular/core';
import {colors} from 'src/app/constant';
import {PieChartData} from "../../model/charts/pie-chart-model";

@Component({
  selector: 'app-simple-pie-chart',
  templateUrl: './simple-pie-chart.component.html',
  styleUrls: ['./simple-pie-chart.component.scss']
})
export class SimplePieChartComponent implements OnInit {
  @Input() pieChartData: Array<PieChartData>;
  @Input() chartTitle: string;

  public chartData: any;
  public series: { name: string; value: number }[];
  public labels: Array<string>;
  public colors: typeof colors = colors;

  public ngOnInit(): void {
    this.initChart();
  }

  public initChart(): void {
    this.labels = this.pieChartData
      .map(value => value.key);
    this.series = this.pieChartData
      .map(value => {
        return {name: value.key, value: value.value};
      });
    this.chartData = {
      color: this.getColors(),
      tooltip: {
        trigger: 'item'
      },
      legend: {
        top: 'center',
        right: 'right',
        data: this.labels,
        textStyle: {
          color: '#6E6E6E'
        }
      },
      series: [{
        type: 'pie',
        radius: ['50%', '70%'],
        center: ['24%', '50%'],
        label: {
          show: false
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        hoverAnimation: false,
        avoidLabelOverlap: false,
        data: this.series
      }]
    };
  }

  getColors(): string[] {
    const colors = this.colors;
    const availableColors = [colors.GREEN, colors.BLUE, colors.PINK, colors.PINK, colors.LIGHT_BLUE, colors.YELLOW];
    const maxIndex = this.pieChartData?.length;
    return availableColors.filter(((value, index) => {
      return index < maxIndex;
    }));
  }
}
