/*
 * Copyright (c) Aleksander Ndoci
 */

import {Injectable} from "@angular/core";
import {ComponentType, ToastrService} from "ngx-toastr";
import {NotificationModel} from "./notification-model";
import {SuccessToastComponent} from "./success-toast/success-toast.component";
import {ErrorToastrComponent} from "./error-toastr/error-toastr.component";
import {InfoToastrComponent} from "./info-toastr/info-toastr.component";

@Injectable({
    providedIn: 'root'
  }
)
export class NotificationService {
  constructor(private toastrService: ToastrService) {
  }

  private readonly toastrPosition = 'toast-top-right';
  private readonly timeOut = 3600;

  success(data: NotificationModel) {
    this.showToastr(data, SuccessToastComponent);
  }

  error(data: NotificationModel) {
    this.showToastr(data, ErrorToastrComponent);
  }

  info(data: NotificationModel) {
    this.showToastr(data, InfoToastrComponent);
  }

  public showToastr(data: NotificationModel, component: ComponentType<any>): void {
    this.toastrService.show(
      data.message,
      data.title,
      {
        positionClass: this.toastrPosition,
        toastComponent: component,
        timeOut: this.timeOut,
        tapToDismiss: false
      }
    );
  }

}
