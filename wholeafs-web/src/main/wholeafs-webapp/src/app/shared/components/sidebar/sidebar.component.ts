import {Component, OnInit} from '@angular/core';
import {MenuFactory, MenuModel} from "../../model/menu-model";
import {HttpService} from "../../services/http/http.service";
import {Role} from "../../../constant/role";
import {Configs} from "../../../constant";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public menuItems: Array<MenuModel> = [];
  public showLeaveTypes = true;

  constructor(private httpService: HttpService) {
  }


  ngOnInit(): void {
    this.httpService.getLoggedUser()
      .subscribe(user => {
        this.setMenu(user.role);
        this.httpService.storeCookie(Configs.LOGGED_USER, JSON.stringify(user));
      });
  }

  setMenu(role: string) {
    switch (role) {
      case Role.ADMIN:
        this.menuItems = MenuFactory.ADMIN_MENU;
        this.showLeaveTypes = false;
        break;
      case Role.USER:
        this.menuItems = MenuFactory.USER_MENU;
        break;
      case Role.FINANCE:
        this.menuItems = MenuFactory.FINANCE_MENU;
        break;
      case Role.SUPERVISOR:
        this.menuItems = MenuFactory.SUPERVISOR_MENU;
        break;
      default:
        console.error('UNKNOWN ROLE: ', role);
    }
  }
}
