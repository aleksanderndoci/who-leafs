import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-refuse-leave-modal',
  templateUrl: './refuse-leave-modal.component.html',
  styleUrls: ['./refuse-leave-modal.component.scss']
})
export class RefuseLeaveModalComponent {
  constructor(
    public dialogRef: MatDialogRef<RefuseLeaveModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmLeaveRemoved) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface ConfirmLeaveRemoved {
  guid: string,
  reason: string
}
