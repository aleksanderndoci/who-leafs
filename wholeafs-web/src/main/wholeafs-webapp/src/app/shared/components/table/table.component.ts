import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort, Sort} from "@angular/material/sort";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {PageRequest, SortDirection} from "../../model/http/page-request";
import {DateUtils} from "../../utils/date-utils";
import {ThemePalette} from "@angular/material/core";

export interface TableCol {
  key: string,
  displayAs: string,
  dataType?: 'simple' | 'userStatus' | 'userRole' | 'date' | 'dateTime' | 'number';
  sortKey?: string
}

export interface TableBtn {
  id: string,
  label: string,
  icon: string,
  color: ThemePalette,
  disabled?: boolean
}

export interface BtnAction<T> {
  data: T,
  btnId: string
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T> implements OnInit, AfterViewInit, OnChanges {
  @Input() wrapperClass: string;
  @Input() operationBtns: TableBtn[];
  @Input() displayCols: TableCol[];
  @Input() data: Array<T>;
  @Input() extraColName: string;
  @Input() pageRequest: PageRequest = {};
  @Input() totalElements: number;

  @Output() onPageSortChange = new EventEmitter<PageRequest>();
  @Output() onBtnClick = new EventEmitter<BtnAction<T>>();

  dataSource: MatTableDataSource<T>;
  columns: string[];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.setPageAndSort();
  }

  constructor() {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<T>(this.data);
    this.columns = this.displayCols.map(value => value.key);
    if (this.extraColName) {
      this.columns = this.columns.concat('extraCol');
    }
  }

  onPageChange(event: PageEvent) {
    this.pageRequest = {
      ...this.pageRequest,
      page: {
        page: event.pageIndex,
        size: event.pageSize
      }
    };
    this.onPageSortChange.emit(this.pageRequest);
  }

  onSortChange(event: Sort) {
    this.pageRequest = {
      ...this.pageRequest,
      sort: {
        sortBy: event.active,
        sortDirection: SortDirection[event.direction.toUpperCase()]
      }
    };
    this.onPageSortChange.emit(this.pageRequest);
  }

  setPageAndSort() {
    const sort = this.pageRequest.sort;
    if (sort && this.sort) {
      this.sort.active = sort.sortBy;
      if (SortDirection.DESC === sort.sortDirection) {
        this.sort.direction = 'desc';
      } else {
        this.sort.direction = 'asc';
      }
    }
    const page = this.pageRequest.page;
    if (page && this.paginator) {
      this.paginator.pageSize = page.size;
      this.paginator.pageIndex = page.page;
    }
  }

  onBtnClickEmit(element, id: string) {
    this.onBtnClick.emit({
      btnId: id,
      data: element
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = new MatTableDataSource<T>(this.data);
    this.setPageAndSort();
  }

  getFormattedValue(element: any, col: TableCol) {
    if (col.dataType === 'dateTime') {
      return DateUtils.formatDateTime(element[col.key]);
    }
    return element[col.key];
  }
}
