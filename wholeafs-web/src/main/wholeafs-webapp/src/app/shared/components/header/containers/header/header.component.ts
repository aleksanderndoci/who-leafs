import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {routes} from "../../../../../constant";
import {HttpService} from "../../../../services/http/http.service";
import {LoggedUser} from "../../../../../pages/login/models";
import {Role} from "../../../../../constant/role";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public loggedUser: LoggedUser;
  public routers: typeof routes = routes;
  _roles = Role;
  role: Role;

  constructor(private httpService: HttpService,
              private router: Router) {
  }

  public openMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;
    this.isShowSidebar.emit(this.isMenuOpened);
  }

  public signOut(): void {
    this.httpService.clearLoginSession();
    this.router.navigate([this.routers.LOGIN]);
  }

  ngOnInit(): void {
    this.httpService.getLoggedUser().subscribe(user => {
      this.loggedUser = user;
      this.role = Role[this.loggedUser.role];
    });
  }

  goToProfile() {
    this.router.navigate([routes.PROFILE]);
  }
}
