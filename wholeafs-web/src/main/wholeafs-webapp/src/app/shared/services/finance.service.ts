import {Injectable} from "@angular/core";
import {DetailedLeaveView, LeavePieChart, LeaveSearchRequest, PeriodRequest} from "../model/leave-model";
import {Observable} from "rxjs";
import {BaseResponse} from "../model/http/http-response";
import {HttpClient} from "@angular/common/http";
import {PagedResponse} from "../model/http/paged-response";

@Injectable({
  providedIn: 'root'
})
export class FinanceService {

  constructor(private httpClient: HttpClient) {
  }

  getLeaveTypeChart(req: PeriodRequest): Observable<BaseResponse<LeavePieChart>> {
    return this.httpClient.post<BaseResponse<LeavePieChart>>('finance/leave/type/chart', req);
  }

  searchLeave(req: LeaveSearchRequest): Observable<PagedResponse<DetailedLeaveView>> {
    return this.httpClient.post<PagedResponse<DetailedLeaveView>>('finance/leave/search', req);
  }

  downloadExcelReport(request: LeaveSearchRequest): Observable<Blob> {
    return this.httpClient.post('finance/leave/excelReport', request, {
      responseType: 'blob'
    });
  }
}
