/*
 * Copyright (c) Aleksander Ndoci
 */

import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PagedResponse} from "../model/http/paged-response";
import {
  AnnualPeriod,
  DetailedLeaveView,
  LeaveApprover,
  LeaveModel,
  LeavePieChart,
  LeaveRequest,
  LeaveSearchRequest,
  LeaveStatusChangeRequest, PeriodRequest
} from "../model/leave-model";
import {BaseResponse} from "../model/http/http-response";
import {UserSearchRequest} from "../model/user-model";
import {Page} from "../model/http/page-request";

@Injectable({
  providedIn: 'root'
})
export class LeaveService {
  constructor(private httpClient: HttpClient) {
  }

  getCurrentAnnualLeavePeriod(): Observable<BaseResponse<AnnualPeriod>> {
    return this.httpClient.get <BaseResponse<AnnualPeriod>>('annualLeavePeriod/getActivePeriod');
  }

  getLoggedUserLeave(req: Page): Observable<PagedResponse<LeaveModel>> {
    return this.httpClient.post<PagedResponse<LeaveModel>>('leave/get', req);
  }

  getStatusChart(): Observable<BaseResponse<LeavePieChart>> {
    return this.httpClient.get<BaseResponse<LeavePieChart>>('leave/statusChart');
  }

  getAvailableApprovers(req: UserSearchRequest): Observable<PagedResponse<LeaveApprover>> {
    return this.httpClient.post<PagedResponse<LeaveApprover>>('leave/findApprovers', req);
  }

  applyForLeave(req: LeaveRequest): Observable<BaseResponse<LeaveModel>> {
    return this.httpClient.post<BaseResponse<LeaveModel>>('leave/apply', req);
  }

  cancelLeave(guid: string): Observable<BaseResponse<LeaveModel>> {
    return this.httpClient.put<BaseResponse<LeaveModel>>(`leave/cancel/${guid}`, {});
  }

  searchApproverLeaves(req: LeaveSearchRequest): Observable<PagedResponse<DetailedLeaveView>> {
    return this.httpClient.post<PagedResponse<DetailedLeaveView>>('leave/approver/search', req);
  }

  changeLeaveStatsu(leaveApprvGuid: string, req: LeaveStatusChangeRequest): Observable<BaseResponse<LeaveApprover>> {
    return this.httpClient.put<BaseResponse<LeaveApprover>>(`leave/approver/updateStatus/${leaveApprvGuid}`, req);
  }
}

