import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {
  ChangePasswordRequest,
  ResetPasswordRequestModel,
  ResetPasswordResponseModel,
  UserExistsModel,
  UserModel,
  UserSearchRequest
} from "../model/user-model";
import {Observable} from "rxjs";
import {BaseResponse} from "../model/http/http-response";
import {PieChartData} from "../model/charts/pie-chart-model";
import {PagedResponse} from "../model/http/paged-response";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  registerUser(userModel: UserModel): Observable<any> {
    return this.httpClient.post<any>('user/register', userModel);
  }

  userExists(usernameOrEmail: string): Observable<BaseResponse<UserExistsModel>> {
    return this.httpClient.get<BaseResponse<UserExistsModel>>(`user/checkIfExists/${usernameOrEmail}`);
  }

  resetPasswordRequest(req: ResetPasswordRequestModel): Observable<BaseResponse<string>> {
    return this.httpClient.post<BaseResponse<string>>('user/resetPasswordEmailRequest', req);
  }

  resetPassword(guid: string, resetPasswordReq: ResetPasswordRequestModel): Observable<BaseResponse<ResetPasswordResponseModel>> {
    return this.httpClient
      .post<BaseResponse<ResetPasswordResponseModel>>(`user/resetPassword/${guid}`, resetPasswordReq);
  }

  changePassword(req: ChangePasswordRequest): Observable<BaseResponse<ResetPasswordResponseModel>> {
    return this.httpClient.post<BaseResponse<ResetPasswordResponseModel>>('user/profile/changePassword', req);
  }

  editProfileData(req: UserModel): Observable<UserModel> {
    return this.httpClient.post<UserModel>('user/profile/edit', req);
  }

  getCurrentUser(): Observable<BaseResponse<UserModel>> {
    return this.httpClient.get<BaseResponse<UserModel>>('user/getCurrent');
  }

  getUserRolePieChart(): Observable<BaseResponse<PieChartData[]>> {
    return this.httpClient.get<BaseResponse<PieChartData[]>>('user/roles/pieChart');
  }

  searchUsers(req: UserSearchRequest): Observable<PagedResponse<UserModel>> {
    return this.httpClient.post<PagedResponse<UserModel>>('user/search', req);
  }

  createUser(request: UserModel): Observable<BaseResponse<UserModel>> {
    return this.httpClient.post<BaseResponse<UserModel>>('user/create', request);
  }

  removeUser(request: UserModel): Observable<BaseResponse<string>> {
    return this.httpClient.delete<BaseResponse<string>>(`user/delete/${request.guid}`);
  }

}
