/*
 * Copyright (c) Aleksander Ndoci
 */

import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {Configs} from "../../../constant";
import {LoggedUser} from "../../../pages/login/models";
import {Observable} from "rxjs";
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient,
              private router: Router,
              private cookieService: CookieService) {
  }

  storeCookie(key: string, value: string) {
    this.cookieService.set(key, value, this.defaultCookieExpirationDate(), '/');
  }

  private defaultCookieExpirationDate() {
    let now = moment();
    now.add(7, 'days');
    return now.toDate();
  }

  getCookie(key: string): string {
    return this.cookieService.get(key);
  }

  clearCookie(key: string) {
    this.cookieService.delete(key, "/");
  }

  clearLoginSession() {
    this.cookieService.delete(Configs.LOGGED_USER, "/");
    this.cookieService.delete(Configs.AUTH_SCHEME, "/");
    this.cookieService.delete(Configs.TOKEN, "/");
  }

  getLoggedUser(): Observable<LoggedUser> {
    const loggedUserStr = this.getCookie(Configs.LOGGED_USER);
    if (loggedUserStr) {
      return new Observable((observer) => {
        observer.next(JSON.parse(loggedUserStr));
        observer.complete()
      })
    }
    return this.httpClient.post<LoggedUser>('auth/loggedUser', {});
  }

  isAuthenticated() {
    return this.cookieService.check(Configs.TOKEN);
  }

  getAuthorization(): string {
    if (this.cookieService.check(Configs.TOKEN)) {
      return `${this.cookieService.get(Configs.AUTH_SCHEME)} ${this.cookieService.get(Configs.TOKEN)}`;
    }
    return null;
  }

}
