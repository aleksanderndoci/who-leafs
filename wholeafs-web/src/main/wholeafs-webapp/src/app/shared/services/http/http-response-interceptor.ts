/*
 * Copyright (c) Aleksander Ndoci
 */

import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {catchError, tap} from "rxjs/operators";
import {Router} from "@angular/router";
import {NotificationService} from "../../components/notification";

@Injectable({
  providedIn: 'root'
})
export class HttpResponseInterceptor implements HttpInterceptor {

  constructor(private router: Router,
              private notificationService: NotificationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap((resp) => {
        return resp.data
      }), catchError<any, any>(err => {
        if (err instanceof HttpErrorResponse) {
          const isAuthError = (err.status === 401 || err.status === 403);
          const msg = this.getMessage(err);
          const title = isAuthError ? 'Login error' : "Error";
          this.notificationService.error({
            title: title,
            message: msg,
          });
          if (isAuthError) {
            this.router.navigate(['/login']);
          }
          throw err;
        }
      })
    );
  }

  getMessage(err: HttpErrorResponse): string {
    if (err.error.detailedError) {
      return err.error.detailedError.description;
    }
    if (err.error.message) {
      return err.error.message;
    }
    return err.message;
  }
}
