import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {HttpService} from "./http.service";
import {Injectable} from "@angular/core";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HttpRequestInterceptor implements HttpInterceptor {

  constructor(private httpService: HttpService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authorization = this.httpService.getAuthorization();
    if (authorization) {
      req = req.clone({
        setHeaders: {
          Authorization: authorization
        }
      });
    }
    req = req.clone({
      url: `${environment.baseApiUrl}/${req.url}`
    });
    return next.handle(req);
  }
}
