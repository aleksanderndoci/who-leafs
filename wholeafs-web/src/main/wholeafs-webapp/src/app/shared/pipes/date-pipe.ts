/*
 * Copyright (c) Aleksander Ndoci
 */

import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'isoDate'})
export class IsoDatePipe implements PipeTransform {

  transform(strDate: string, defaultIfNull?: string): string {
    if (!strDate) {
      return defaultIfNull ? defaultIfNull : '-';
    }
    const date = new Date(strDate);
    return `${this.padLeft(date.getDate())}-${this.padLeft(date.getMonth())}-${date.getFullYear()}`;
  }

  padLeft(nr: number): string {
    const pad = "00";
    const str = nr + '';
    return str ? (pad.substring(0, pad.length - str.length) + str) : pad;
  }
}
