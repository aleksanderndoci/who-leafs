/*
 * Copyright (c) Aleksander Ndoci
 */

import {Pipe, PipeTransform} from "@angular/core";
import {DateUtils} from "../utils/date-utils";

@Pipe({name: 'dateTime'})
export class DateTimePipe implements PipeTransform {

  transform(strDate: string | Date, defaultIfNull?: string): string {
    return DateUtils.formatDateTime(strDate, defaultIfNull);
  }

}
