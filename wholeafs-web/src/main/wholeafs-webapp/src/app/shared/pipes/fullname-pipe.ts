/*
 * Copyright (c) Aleksander Ndoci
 */

import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'fullName'})
export class FullnamePipe implements PipeTransform {

  transform(name: string, surname?: string): string {
    return this.notNull(name) + ' ' + this.notNull(surname);
  }

  notNull(val: string): string {
    return val ? val : '';
  }
}
