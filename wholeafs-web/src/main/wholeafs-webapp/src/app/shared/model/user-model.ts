/*
 * Copyright (c) Aleksander Ndoci
 */

import {Role} from "../../constant/role";
import {PageRequest} from "./http/page-request";
import {CommonModel} from "./common/common-model";

export interface UserModel extends CommonModel {
  name: string,
  surname: string,
  username: string,
  email: string,
  accountStatus: AccountStatus,
  password?: string
  role?: {
    role: string
  }
}

export enum AccountStatus {
  ACTIVE = 'ACTIVE',
  CANCELED = 'CANCELED',
  NOT_CONFIRMED = 'NOT_CONFIRMED'
}

export interface UserExistsModel {
  exists: boolean
}

export interface ResetPasswordRequestModel {
  baseUrl?: string,
  email?: string,
  password?: string,
  confirmPassword?: string,
  resetPin?: number
}

export interface ResetPasswordResponseModel {
  wasReset: boolean,
  reason: string
}

export interface UserSearchRequest extends PageRequest {
  fullName?: string;
  username?: string;
  email?: string;
  role?: Role
  accountStatus?: AccountStatus
}

export interface ChangePasswordRequest {
  password: string,
  newPassword: string,
  confirmPassword?: string
}
