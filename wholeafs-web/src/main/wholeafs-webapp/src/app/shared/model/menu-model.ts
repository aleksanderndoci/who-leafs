/*
 * Copyright (c) Aleksander Ndoci
 */

import {routes} from "../../constant";

export interface MenuModel {
  url: string,
  title: string,
  icon: string,
  hidden?: boolean,
  submenu?: MenuModel
}

export class MenuFactory {
  public static ADMIN_MENU: MenuModel[] = [
    {
      title: 'Dashboard',
      url: routes.DASHBOARD,
      icon: 'explore',
    },
    {
      title: 'Users',
      url: routes.DASHBOARD_USERS,
      icon: 'supervised_user_circle',
    }
  ];

  public static USER_MENU: MenuModel[] = [
    {
      title: 'Dashboard',
      url: routes.DASHBOARD,
      icon: 'explore',
    },
    {
      title: 'Apply for leave',
      url: routes.APPLY_LEAVE,
      icon: 'dynamic_form'
    },
    {
      title: 'Previous leaves',
      url: routes.LEAVE_HISTORY,
      icon: 'history_toggle_off'
    }
  ];

  public static FINANCE_MENU: MenuModel[] = [
    {
      title: 'Dashboard',
      url: routes.DASHBOARD,
      icon: 'explore',
    },
    {
      title: 'Reports',
      url: routes.LEAVE_REPORTS,
      icon: 'assessment'
    }
  ];

  public static SUPERVISOR_MENU: MenuModel[] = [
    {
      title: 'Dashboard',
      url: routes.DASHBOARD,
      icon: 'explore',
    }, {
      title: 'Pending leave requests',
      url: routes.PENDING_LEAVE,
      icon: 'pending_actions'
    }, {
      title: 'Previous leaves',
      url: routes.LEAVE_HISTORY,
      icon: 'history_toggle_off'
    }
  ];
}
