/*
 * Copyright (c) Aleksander Ndoci
 */

export interface PageRequest {
  page?: Page,
  sort?: {
    sortBy: string,
    sortDirection: SortDirection;
  }
}

export enum SortDirection {
  ASC = 'ASC',
  DESC = 'DESC'
}

export interface Page {
  page: number,
  size: number
}
