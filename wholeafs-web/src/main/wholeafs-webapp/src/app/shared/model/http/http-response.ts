/*
 * Copyright (c) Aleksander Ndoci
 */

export interface BaseResponse<T> {
  status: number,
  message?: string,
  detailedMessages?: Array<string>,
  data: T
}
