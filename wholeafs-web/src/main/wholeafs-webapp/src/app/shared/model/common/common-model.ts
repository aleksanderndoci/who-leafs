/*
 * Copyright (c) Aleksander Ndoci
 */

export interface CommonModel {
  guid?: string,
  insertedAt?: Date,
  lastUpdated?: Date
}
