export interface LoginRequestModel {
  username: string,
  password: string,
  rememberMe?: boolean
}

export interface LoginResponseModel {
  schema?: string,
  token?: string
}
