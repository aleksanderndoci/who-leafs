/*
 * Copyright (c) Aleksander Ndoci
 */

export interface PieChartData {
  key: string,
  value: number
}
