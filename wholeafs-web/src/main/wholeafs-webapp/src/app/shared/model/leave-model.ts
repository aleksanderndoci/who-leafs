/*
 * Copyright (c) Aleksander Ndoci
 */

import {CommonModel} from "./common/common-model";
import {PieChartData} from "./charts/pie-chart-model";
import {PageRequest} from "./http/page-request";

export interface LeaveModel extends CommonModel {
  startDate: string,
  endDate: string,
  hours: number,
  reason: string,
  leaveType: LeaveTypeEnum,
  leaveStatus: LeaveStatusEnum,
  periodTitle: string,
  leaveApprovers: Array<LeaveApprover>
}

export enum LeaveTypeEnum {
  SICK_LEAVE_PAYED = 'SICK_LEAVE_PAYED',
  NOT_PAYED_LEAVE = 'NOT_PAYED_LEAVE',
  MATERNITY = 'MATERNITY',
  PARENTAL_LEAVE = 'PARENTAL_LEAVE',
  EDUCATIONAL_OR_STUDY = 'EDUCATIONAL_OR_STUDY',
  DEPENDENT_CARE = 'DEPENDENT_CARE',
  HOURLY_LEAVE = 'HOURLY_LEAVE',
  DAILY_LEAVE = 'DAILY_LEAVE',
  HOLIDAY_LEAVE = 'HOLIDAY_LEAVE',
}

export enum LeaveStatusEnum {
  DRAFT = 'DRAFT',
  SENT = 'SENT',
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
  REFUSED = 'REFUSED',
  CANCELED = 'CANCELLED'
}

export interface LeaveApprover extends CommonModel {
  name: string,
  surname: string,
  reason?: string,
  status?: LeaveStatusEnum
}

export interface UserLeavePeriod extends CommonModel {
  totalHours: number,
  remainingHours?: number,
  notes: string,
  period: AnnualPeriod
}

export interface AnnualPeriod {
  periodTitle: string,
  startDate: string,
  endDate: string
  defaultTotalHours: number
}

export interface LeavePieChart {
  periodTitle: string,
  values: Array<PieChartData>
}

export interface LeaveRequest {
  startDate: Date,
  endDate: Date,
  leaveType: LeaveTypeEnum,
  reason: string,
  approverGuids: Array<string>
}

export interface LeaveSearchRequest extends PageRequest {
  startDate?: Date,
  endDate?: Date,
  leaveType?: Array<LeaveTypeEnum>,
  leaveStatus?: Array<LeaveStatusEnum>,
  userGuid?: string,
  periodGuid?: string,
  approverGuid?: string
}

export interface DetailedLeaveView extends CommonModel {
  periodTitle: string,
  fullName?: string,
  startDate: string,
  endDate: string,
  hours: string,
  leaveType: string,
  overallStatus: string,
  approverStatus: string,
  reason: string
  rejectReason: string
}

export interface LeaveStatusChangeRequest {
  leaveStatus: LeaveStatusEnum,
  reason?: string
}

export interface PeriodRequest {
  from: Date,
  to: Date
}
