/*
 * Copyright (c) Aleksander Ndoci
 */

export interface PagedResponse<T> {
  page: number;
  size: number;
  totalElements: number;
  results: Array<T>
}
