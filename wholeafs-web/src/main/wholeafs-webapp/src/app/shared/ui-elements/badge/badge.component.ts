import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {

  @Input() color: string = 'blue';
  @Input() type: 'solid' | 'transparent' | 'transparent-circle' = 'solid';
  @Input() icon: string;

  public badgeTransparentClass = '';
  public badgeTransparentCircleClass = '';
  public badgeSolidClass = '';

  constructor() {
  }

  ngOnInit(): void {
    this.badgeTransparentClass = `app-badge__icon-wrapper app-badge__icon-wrapper_solid-${this.color}`;
    this.badgeSolidClass = `app-badge app-badge_solid-${this.color}`;
    this.badgeTransparentCircleClass = `app-badge__icon-wrapper app-badge__icon-wrapper_solid-${this.color}`;
  }

}
