/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.swagger.Documented;
import com.demo.wholeafs.core.model.pojo.AnnualLeavePeriodModel;
import com.demo.wholeafs.core.service.AnnualLeavePeriodService;
import com.demo.wholeafs.rest.model.common.HttpResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Documented
@RestController
@RequestMapping("/annualLeavePeriod")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AnnualPeriodController extends CommonController {

    private final AnnualLeavePeriodService annualLeavePeriodService;

    public AnnualPeriodController(AnnualLeavePeriodService annualLeavePeriodService) {
        this.annualLeavePeriodService = annualLeavePeriodService;
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<AnnualLeavePeriodModel>> create(@RequestBody AnnualLeavePeriodModel periodModel) {
        return ok(this.annualLeavePeriodService.createPeriod(periodModel));
    }

    @GetMapping("/getActivePeriod")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<AnnualLeavePeriodModel>> getActivePeriod() {
        return ok(this.annualLeavePeriodService.getActualPeriod());
    }
}
