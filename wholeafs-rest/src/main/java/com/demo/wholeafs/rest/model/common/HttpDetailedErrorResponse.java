/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.model.common;

import com.demo.wholeafs.core.exception.common.ErrorType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class HttpDetailedErrorResponse extends HttpErrorResponse {

    private DetailedError detailedError;

    @Builder
    @Data
    public static class DetailedError {
        private String errorViewCode;
        private ErrorType errorType;
        private String error;
        private String description;
        private String exceptionThrown;
    }
}
