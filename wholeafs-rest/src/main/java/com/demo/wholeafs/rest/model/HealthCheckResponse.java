/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.model;

import lombok.Data;

@Data
public class HealthCheckResponse {
    private String url;
    private String health;
}
