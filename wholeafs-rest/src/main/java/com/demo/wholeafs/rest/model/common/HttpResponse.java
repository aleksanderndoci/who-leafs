/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.model.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpResponse<T> {
    private int status;
    private String message;
    private List<String> detailedMessages;
    private T data;
}
