/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.security.jwt.authentication.JwtAuthentication;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.util.func.Callable;
import com.demo.wholeafs.core.util.func.Executable;
import com.demo.wholeafs.rest.model.common.HttpResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public class CommonController {

    public <T> ResponseEntity<HttpResponse<T>> ok(T data) {
        HttpResponse<T> httpResponse = new HttpResponse<>();
        httpResponse.setData(data);
        httpResponse.setStatus(HttpStatus.OK.value());
        httpResponse.setMessage("200 OK");
        return ResponseEntity.ok(httpResponse);
    }

    public ResponseEntity<Resource> media(Resource resource, String fileName) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    public <T> ResponseEntity<HttpResponse<T>> ok() {
        HttpResponse<T> httpResponse = new HttpResponse<>();
        httpResponse.setStatus(HttpStatus.OK.value());
        httpResponse.setMessage("200 OK");
        return ResponseEntity.ok(httpResponse);
    }


    public <T> ResponseEntity<HttpResponse<T>> created(T data) {
        HttpResponse<T> httpResponse = new HttpResponse<>();
        httpResponse.setData(data);
        httpResponse.setStatus(HttpStatus.CREATED.value());
        httpResponse.setMessage("Created successfuly!");
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(httpResponse);
    }

    public <T> ResponseEntity<PageableResponse<T>> ok(PageableResponse<T> data) {
        return ResponseEntity.ok(data);
    }

    public <T> ResponseEntity<HttpResponse<T>> ok(Callable callable) {
        callable.call();
        return ok();
    }

    public <T> ResponseEntity<HttpResponse<T>> ok(Executable<T> executable) {
        return ok(executable.execute());
    }

    public WhoLeafsUserDetails getUser(HttpServletRequest request) {
        return ((JwtAuthentication) request.getUserPrincipal()).getPrincipal();
    }

    protected Integer getUserId(HttpServletRequest request) {
        return getUser(request).getId();
    }

}
