/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.handler.util;

import com.demo.wholeafs.core.exception.common.WhoLeafsBaseRuntimeException;
import com.demo.wholeafs.rest.model.common.HttpDetailedErrorResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Locale;
import java.util.ResourceBundle;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorHandlerUtil {
    public static HttpDetailedErrorResponse.DetailedError generateDetailedError(WhoLeafsBaseRuntimeException ex,
                                                                                boolean showExeption) {
        String key = ex.getErrorCode() != null ? ex.getErrorCode() : "error.generic";
        Locale locale = Locale.getDefault();
        ResourceBundle resourceBundle = ResourceBundle
                .getBundle("templates/exception/exception-msg", locale);
        return HttpDetailedErrorResponse.DetailedError
                .builder()
                .errorViewCode(getKeySafe(resourceBundle, key.concat(".viewCode")))
                .description(getKeySafe(resourceBundle, key.concat(".desc")))
                .error(getKeySafe(resourceBundle, key.concat(".error")))
                .errorType(ex.getErrorType())
                .exceptionThrown(showExeption ? ex.toString() : "[hidden]")
                .build();
    }

    private static String getKeySafe(ResourceBundle resourceBundle, String key) {
        return resourceBundle.containsKey(key) ? resourceBundle.getString(key) : null;
    }
}
