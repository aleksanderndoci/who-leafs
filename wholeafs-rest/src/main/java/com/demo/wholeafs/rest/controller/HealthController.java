/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.swagger.Documented;
import com.demo.wholeafs.rest.model.HealthCheckResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Documented
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/health")
public class HealthController {

    @GetMapping("/check")
    public ResponseEntity<HealthCheckResponse> checkHealth(HttpServletRequest request) {
        HealthCheckResponse healthCheckResponse = new HealthCheckResponse();
        healthCheckResponse.setUrl(request.getRequestURI());
        healthCheckResponse.setHealth("Pretty good ;)");
        return ResponseEntity.ok(healthCheckResponse);
    }


    @GetMapping("/check/auth")
    public ResponseEntity<HealthCheckResponse> checkHealthWithAuth(HttpServletRequest request) {
        HealthCheckResponse healthCheckResponse = new HealthCheckResponse();
        healthCheckResponse.setUrl(request.getRequestURI());
        healthCheckResponse.setHealth("Rocks!");
        return ResponseEntity.ok(healthCheckResponse);
    }

}
