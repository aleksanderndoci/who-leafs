/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import com.demo.wholeafs.configs.swagger.Documented;
import com.demo.wholeafs.core.exception.EntityNotFoundException;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.pojo.user.UserExistsModel;
import com.demo.wholeafs.core.model.pojo.user.UserModel;
import com.demo.wholeafs.core.model.pojo.user.password.ChangePasswordDto;
import com.demo.wholeafs.core.model.pojo.user.password.PasswordResetResponse;
import com.demo.wholeafs.core.model.pojo.user.password.ResetPasswordDto;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.view.PieChartView;
import com.demo.wholeafs.core.service.UserService;
import com.demo.wholeafs.rest.model.common.HttpResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/user")
@Documented
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController extends CommonController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public UserController(UserService userService,
                          PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("checkIfExists/{usernameOrEmail}")
    public ResponseEntity<HttpResponse<UserExistsModel>> checkIfUserExists(@PathVariable String usernameOrEmail) {
        return ok(userService.checkIfUserExists(usernameOrEmail));
    }

    @PostMapping("register")
    public ResponseEntity<HttpResponse<UserModel>> register(@RequestBody UserModel userModel) {
        return ok(() -> {
            userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
            // Safety set/ prevent other roles (except USER) from registering
            userModel.setRole(null);
            this.userService.registerUser(userModel);
        });
    }

    @PostMapping("sendConfirmationEmail")
    public ResponseEntity<HttpResponse<String>> sendConfirmationEmail(HttpServletRequest request) {
        return ok(() -> {
            WhoLeafsUserDetails userDetails = super.getUser(request);
            this.userService.sendConfirmationEmail(userDetails.getEmail(), request.getRemoteHost());
        });
    }

    @PostMapping("confirmEmail/{guid}")
    public ResponseEntity<HttpResponse<String>> confirmEmail(@PathVariable String guid) {
        return ok(() -> this.userService.confirmEmail(guid));
    }

    @PostMapping("resetPasswordEmailRequest")
    public ResponseEntity<HttpResponse<String>> resetPasswordEmailRequest(@RequestBody ResetPasswordDto request) {
        return ok(() -> this.userService.sendPasswordResetRequest(request));
    }

    @PostMapping("resetPassword/{guid}")
    public ResponseEntity<HttpResponse<PasswordResetResponse>> resetPassword(@PathVariable String guid,
                                                                             @RequestBody ResetPasswordDto request) {
        return ok(() -> {
            request.setPassword(passwordEncoder.encode(request.getPassword()));
            return this.userService.changePassword(guid, request);
        });
    }

    @PostMapping("profile/changePassword")
    public ResponseEntity<HttpResponse<PasswordResetResponse>> changePassword(HttpServletRequest httpRequest,
                                                                              @RequestBody ChangePasswordDto changePasswordDto) {
        return ok(() -> {
            changePasswordDto.setNewPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
            return this.userService.changePassword(getUserId(httpRequest), changePasswordDto,
                    userModel -> passwordEncoder.matches(changePasswordDto.getPassword(), userModel.getPassword()));
        });
    }

    @PostMapping("profile/edit")
    public ResponseEntity<HttpResponse<UserModel>> editUser(HttpServletRequest httpRequest,
                                                            @RequestBody UserModel userModel) {
        return ok(() -> {
            userModel.setId(getUserId(httpRequest));
            return this.userService.editAccount(userModel, false);
        });
    }

    @PostMapping("search")
    public ResponseEntity<PageableResponse<UserModel>> searchUsers(@RequestBody UserSearchDto searchDto) {
        return ok(this.userService.searchUsers(searchDto));
    }

    @GetMapping("getCurrent")
    public ResponseEntity<HttpResponse<UserModel>> getCurrentUser(HttpServletRequest request) {
        return ok(() -> this.userService.findById(getUserId(request))
                .orElseThrow(() -> new EntityNotFoundException("No user found from session")));
    }

    @PostMapping("create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<UserModel>> createUser(@RequestBody UserModel userModel) {
        return ok(() -> {
            userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
            return this.userService.registerUser(userModel);
        });
    }

    @PostMapping("edit/{guid}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<UserModel>> editUser(@RequestBody UserModel userModel) {
        return ok(() -> this.userService.editAccount(userModel, true));
    }

    @GetMapping("roles/pieChart")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<List<PieChartView>>> getUserRolePieChart() {
        return ok(this.userService.getUsersByRole());
    }

    @DeleteMapping("delete/{guid}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpResponse<String>> deleteByGuid(@PathVariable String guid,
                                                             HttpServletRequest request) {
        return ok(() -> this.userService.deleteByGuid(guid, getUserId(request)));
    }
}
