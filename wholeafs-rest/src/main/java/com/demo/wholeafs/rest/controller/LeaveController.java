/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.swagger.Documented;
import com.demo.wholeafs.core.model.pojo.LeaveApproverModel;
import com.demo.wholeafs.core.model.pojo.LeaveModel;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.request.LeaveRequestDto;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.request.LeaveStatusUpdateDto;
import com.demo.wholeafs.core.model.request.UserSearchDto;
import com.demo.wholeafs.core.model.request.common.PageDto;
import com.demo.wholeafs.core.model.view.LeaveApproverView;
import com.demo.wholeafs.core.model.view.LeaveDetailedApproverView;
import com.demo.wholeafs.core.model.view.LeavePieChartView;
import com.demo.wholeafs.core.service.LeaveApproverService;
import com.demo.wholeafs.core.service.LeaveService;
import com.demo.wholeafs.rest.model.common.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Documented
@RestController
@RequestMapping("/leave")
@CrossOrigin(origins = "*", maxAge = 3600)
public class LeaveController extends CommonController {

    private final LeaveService leaveService;
    private final LeaveApproverService leaveApproverService;

    @Autowired
    public LeaveController(LeaveService leaveService,
                           LeaveApproverService leaveApproverService) {
        this.leaveService = leaveService;
        this.leaveApproverService = leaveApproverService;
    }

    @PostMapping("/get")
    public ResponseEntity<PageableResponse<LeaveModel>> getLeave(@RequestBody PageDto pageDto,
                                                                 HttpServletRequest request) {
        return ok(leaveService.getUserLeave(super.getUser(request).getId(), pageDto));
    }

    @GetMapping("/statusChart")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<HttpResponse<LeavePieChartView>> getStatusChart(HttpServletRequest request) {
        return ok(leaveService.getLeavePieChart(super.getUser(request).getId()));
    }

    @PostMapping("/apply")
    public ResponseEntity<HttpResponse<LeaveModel>> applyForLeave(@RequestBody LeaveRequestDto requestDto,
                                                                  HttpServletRequest request) {
        return ok(leaveService.applyForLeave(getUserId(request), requestDto));
    }

    @PostMapping("findApprovers")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<PageableResponse<LeaveApproverView>> getLeaveApprovers(@RequestBody UserSearchDto userSearchDto) {
        return ok(leaveApproverService.getAvailableApprovers(userSearchDto));
    }

    @PutMapping("cancel/{guid}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<HttpResponse<LeaveModel>> cancelLeave(@PathVariable String guid) {
        return ok(this.leaveService.cancelLeave(guid));
    }

    @PutMapping("/approver/updateStatus/{guid}")
    @PreAuthorize("hasAuthority('SUPERVISOR')")
    public ResponseEntity<HttpResponse<LeaveApproverModel>> updateLeaveStatus(@PathVariable String guid,
                                                                              @RequestBody LeaveStatusUpdateDto dto) {
        return ok(this.leaveService.changeStatus(guid, dto));
    }

    @PostMapping("/approver/search")
    @PreAuthorize("hasAuthority('SUPERVISOR')")
    public ResponseEntity<PageableResponse<LeaveDetailedApproverView>> searchApproverLeaves(@RequestBody LeaveSearchDto leaveSearchDto) {
        return ok(this.leaveApproverService.searchLeaveApprover(leaveSearchDto));
    }
}
