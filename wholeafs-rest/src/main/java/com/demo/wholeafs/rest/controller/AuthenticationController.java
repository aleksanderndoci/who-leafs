/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.security.jwt.authentication.JwtAuthentication;
import com.demo.wholeafs.configs.security.jwt.model.JwtLoginRequest;
import com.demo.wholeafs.configs.security.jwt.model.JwtTokenResponse;
import com.demo.wholeafs.configs.security.service.AuthenticationService;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import com.demo.wholeafs.configs.swagger.Documented;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Documented
@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public ResponseEntity<JwtTokenResponse> login(@RequestBody JwtLoginRequest loginRequest) {
        return ResponseEntity.ok(authenticationService.authenticate(loginRequest));
    }

    @PostMapping("/loggedUser")
    public WhoLeafsUserDetails getLoggedUser(HttpServletRequest request) {
        return ((JwtAuthentication) request.getUserPrincipal()).getPrincipal();
    }
}
