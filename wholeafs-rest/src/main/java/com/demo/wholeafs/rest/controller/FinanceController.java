/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.controller;

import com.demo.wholeafs.configs.swagger.Documented;
import com.demo.wholeafs.core.model.pojo.common.PageableResponse;
import com.demo.wholeafs.core.model.request.LeaveSearchDto;
import com.demo.wholeafs.core.model.request.PeriodDto;
import com.demo.wholeafs.core.model.view.LeaveDetailedApplierView;
import com.demo.wholeafs.core.model.view.LeavePieChartView;
import com.demo.wholeafs.core.service.ExcelService;
import com.demo.wholeafs.core.service.LeaveService;
import com.demo.wholeafs.rest.model.common.HttpResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@Documented
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/finance")
public class FinanceController extends CommonController {

    private final LeaveService leaveService;
    private final ExcelService excelService;

    public FinanceController(LeaveService leaveService,
                             ExcelService excelService) {
        this.leaveService = leaveService;
        this.excelService = excelService;
    }

    @PostMapping("/leave/type/chart")
    @PreAuthorize("hasAuthority('FINANCE')")
    public ResponseEntity<HttpResponse<LeavePieChartView>> getLeaveTypeChart(@RequestBody PeriodDto periodDto) {
        return ok(this.leaveService.getLeaveChartGroupByType(periodDto));
    }

    @PostMapping("leave/search")
    @PreAuthorize("hasAuthority('FINANCE')")
    public ResponseEntity<PageableResponse<LeaveDetailedApplierView>> searchLeaves(@RequestBody LeaveSearchDto leaveSearchDto) {
        return ok(this.leaveService.searchLeaveApplier(leaveSearchDto));
    }

    @PostMapping("leave/excelReport")
    @PreAuthorize("hasAuthority('FINANCE')")
    public ResponseEntity<Resource> generateExcelReport(@RequestBody LeaveSearchDto leaveSearchDto) {
        return media(excelService.generateLeaveExcel(leaveSearchDto), "Leave_Report.xlsx");
    }
}
