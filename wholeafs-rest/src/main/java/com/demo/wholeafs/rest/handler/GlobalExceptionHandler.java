/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.handler;

import com.demo.wholeafs.core.exception.common.WhoLeafsBaseRuntimeException;
import com.demo.wholeafs.rest.handler.util.ErrorHandlerUtil;
import com.demo.wholeafs.rest.model.common.HttpDetailedErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * Global exception handler for wholeafs application
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @Value("${wholeafs.exception.handling.enable:true}")
    private boolean enableExceptionHandling;

    @Value("${wholeafs.exception.handling.printException:false}")
    private boolean showException;

    @ExceptionHandler(WhoLeafsBaseRuntimeException.class)
    public ResponseEntity<HttpDetailedErrorResponse> handleExceptions(WhoLeafsBaseRuntimeException ex,
                                                                      HttpServletRequest req) {
        log.error("WholeafsException", ex);
        HttpStatus errorStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        HttpDetailedErrorResponse errorResponse = new HttpDetailedErrorResponse();
        errorResponse.setErrorCode(errorStatus.value());
        errorResponse.setPath(req.getRequestURI());
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setMessage(errorStatus.getReasonPhrase());
        if (enableExceptionHandling)
            errorResponse.setDetailedError(ErrorHandlerUtil.generateDetailedError(ex, showException));
        return ResponseEntity.status(errorStatus).body(errorResponse);
    }
}
