/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.rest.model.common;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HttpErrorResponse {
    private int errorCode;
    private LocalDateTime timestamp;
    private String path;
    private String message;

    public HttpErrorResponse() {
        timestamp = LocalDateTime.now();
    }
}
