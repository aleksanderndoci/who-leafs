/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.constants;

public class SecurityConstant {
    public static final String AUTHORIZATION_SCHEMA = "Bearer";
    public static final String AUTHENTICATION_ENDPOINT = "/auth/login";
    public static final String USER_REGISTRATION_ENDPOINT = "/user/register";

    public static final String[] SWAGGER_ENDPOINTS = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/**",
            "/swagger-ui/**",
            "/webjars/**"
    };

    public static class JwtClaims {
        public static final String NAME_ = "name";
        public static final String SURNAME_ = "surname";
        public static final String ROLE_ = "authorities";
        public static final String EMAIL_ = "email";
        public static final String USER_ID_ = "id";
    }
}
