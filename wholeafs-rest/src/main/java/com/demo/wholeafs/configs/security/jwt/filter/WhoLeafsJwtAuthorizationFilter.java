package com.demo.wholeafs.configs.security.jwt.filter;

import com.demo.wholeafs.configs.security.constants.SecurityConstant;
import com.demo.wholeafs.configs.security.exception.WhoLeafsAuthenticationException;
import com.demo.wholeafs.configs.security.exception.WhoLeafsTokenException;
import com.demo.wholeafs.configs.security.exception.handler.WhoLeafsAuthHandler;
import com.demo.wholeafs.configs.security.jwt.authentication.JwtAuthentication;
import com.demo.wholeafs.configs.security.jwt.util.JwtUtil;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WhoLeafsJwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final RequestMatcher ignoreMatcher;

    private final JwtUtil jwtUtil;

    private final AuthenticationManager authenticationManager;

    private final WhoLeafsAuthHandler whoLeafsAuthHandler;

    public WhoLeafsJwtAuthorizationFilter(RequestMatcher ignoreMatcher,
                                          JwtUtil jwtUtil,
                                          AuthenticationManager authenticationManager,
                                          WhoLeafsAuthHandler whoLeafsAuthHandler) {
        super(authenticationManager);
        this.ignoreMatcher = ignoreMatcher;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.whoLeafsAuthHandler = whoLeafsAuthHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            String token = this.validateAndReturnToken(request);
            Authentication authentication = this.getAuthenticationFromToken(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request, response);
        } catch (AuthenticationException e) {
            SecurityContextHolder.clearContext();
            whoLeafsAuthHandler.onAuthenticationFailure(request, response, e);
        }
    }

    private Authentication getAuthenticationFromToken(String token) {
        WhoLeafsUserDetails userDetails = jwtUtil.extractUserDetails(token);
        return authenticationManager.authenticate(new JwtAuthentication(userDetails));
    }

    private String validateAndReturnToken(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token == null) {
            throw new WhoLeafsAuthenticationException("Authentication token is not present");
        }
        if (!token.contains(SecurityConstant.AUTHORIZATION_SCHEMA)) {
            throw new WhoLeafsTokenException("Authorization schema not present or invalid.");
        }
        token = token.substring(SecurityConstant.AUTHORIZATION_SCHEMA.length()).trim();
        return token;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return ignoreMatcher.matches(request);
    }
}
