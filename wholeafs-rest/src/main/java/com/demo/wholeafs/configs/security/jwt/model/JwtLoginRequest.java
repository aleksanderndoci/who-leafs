/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.jwt.model;

import lombok.Data;

@Data
public class JwtLoginRequest {
    private String username;
    private String password;
    private boolean rememberMe;
}
