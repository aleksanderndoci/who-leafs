/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.user;

import com.demo.wholeafs.configs.security.exception.WhoLeafsAuthenticationException;
import com.demo.wholeafs.core.model.entity.User;
import com.demo.wholeafs.core.repository.UserRepository;
import com.demo.wholeafs.core.util.DateUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.demo.wholeafs.core.model.enums.AccountStatusEnum.ACTIVE;

@Component
@Primary
public class WhoLeafsUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public WhoLeafsUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsernameOrEmail(s, s)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        if (user.getRole() == null) {
            throw new WhoLeafsAuthenticationException("No role found for user: " + s);
        }
        return WhoLeafsUserDetails.builder()
                .id(user.getId())
                .isEnabled(ACTIVE.equals(user.getAccountStatus()))
                .isAccountLocked(this.isAccountLocked(user))
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .email(user.getEmail())
                .role(user.getRole().getRole().name())
                .password(user.getPassword())
                .build();
    }

    /**
     * @param user user to be evaluated
     * @return true if user has passed the probation period of 90 days/otherwise account is locked
     */
    private boolean isAccountLocked(User user) {
        return DateUtils.findDaysBetween(user.getInsertedAt(), new Date()) < 90;
    }
}
