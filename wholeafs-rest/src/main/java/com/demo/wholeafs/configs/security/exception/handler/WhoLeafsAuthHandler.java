/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.exception.handler;

import com.demo.wholeafs.rest.model.common.HttpErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class WhoLeafsAuthHandler implements AuthenticationFailureHandler {

    private final ObjectMapper objectMapper;

    public WhoLeafsAuthHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        HttpErrorResponse httpErrorResponse = new HttpErrorResponse();
        httpErrorResponse.setErrorCode(HttpServletResponse.SC_UNAUTHORIZED);
        httpErrorResponse.setPath(httpServletRequest.getRequestURI());
        httpErrorResponse.setMessage("Authorization Error: " + e.getMessage());
        httpServletResponse.setStatus(httpErrorResponse.getErrorCode());
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(httpServletResponse.getWriter(), httpErrorResponse);
    }

}
