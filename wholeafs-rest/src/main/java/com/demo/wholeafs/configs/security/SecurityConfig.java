/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security;

import com.demo.wholeafs.configs.security.constants.SecurityConstant;
import com.demo.wholeafs.configs.security.exception.handler.WhoLeafesAuthEntryPoint;
import com.demo.wholeafs.configs.security.exception.handler.WhoLeafsAuthHandler;
import com.demo.wholeafs.configs.security.jwt.filter.WhoLeafsJwtAuthorizationFilter;
import com.demo.wholeafs.configs.security.jwt.util.JwtUtil;
import com.demo.wholeafs.configs.security.utils.OpenEndpointConfigProps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private WhoLeafesAuthEntryPoint whoLeafesAuthEntryPoint;

    private JwtUtil jwtUtil;

    private AuthenticationManager authenticationManager;

    private WhoLeafsAuthHandler whoLeafsAuthHandler;

    private AuthenticationProvider whoLeafsAuthProvider;

    private OpenEndpointConfigProps openEndpointConfigProps;

    @Autowired
    public void setWhoLeafesAuthEntryPoint(WhoLeafesAuthEntryPoint whoLeafesAuthEntryPoint) {
        this.whoLeafesAuthEntryPoint = whoLeafesAuthEntryPoint;
    }

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setWhoLeafsAuthenticationHandler(WhoLeafsAuthHandler whoLeafsAuthHandler) {
        this.whoLeafsAuthHandler = whoLeafsAuthHandler;
    }

    @Autowired
    public void setWhoLeafsAuthenticationProvider(AuthenticationProvider whoLeafsAuthProvider) {
        this.whoLeafsAuthProvider = whoLeafsAuthProvider;
    }

    @Autowired
    public void setOpenEndpointConfigProps(OpenEndpointConfigProps openEndpointConfigProps) {
        this.openEndpointConfigProps = openEndpointConfigProps;
    }

    @Bean
    public OpenEndpointConfigProps openEndpointConfigProps() {
        return new OpenEndpointConfigProps();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(5);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(whoLeafsAuthProvider);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(SecurityConstant.SWAGGER_ENDPOINTS);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors();
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .requestMatchers(openEndpointConfigProps.getPermittedPathMatchers())
                .permitAll()
                .anyRequest().authenticated()

                .and()
                .exceptionHandling()
                .authenticationEntryPoint(whoLeafesAuthEntryPoint)

                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        httpSecurity.addFilterBefore(getWhoLeafsJwtFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    private WhoLeafsJwtAuthorizationFilter getWhoLeafsJwtFilter() {
        RequestMatcher ignoreMatcher = new OrRequestMatcher(openEndpointConfigProps.getPermittedPathMatchers());
        return new WhoLeafsJwtAuthorizationFilter(ignoreMatcher, jwtUtil, authenticationManager, whoLeafsAuthHandler);
    }
}
