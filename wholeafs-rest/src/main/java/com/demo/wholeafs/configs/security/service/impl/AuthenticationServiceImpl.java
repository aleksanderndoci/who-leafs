/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.service.impl;

import com.demo.wholeafs.configs.security.constants.SecurityConstant;
import com.demo.wholeafs.configs.security.jwt.authentication.JwtAuthentication;
import com.demo.wholeafs.configs.security.jwt.model.JwtLoginRequest;
import com.demo.wholeafs.configs.security.jwt.model.JwtTokenResponse;
import com.demo.wholeafs.configs.security.jwt.util.JwtUtil;
import com.demo.wholeafs.configs.security.service.AuthenticationService;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;

    private final JwtUtil jwtUtil;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, JwtUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public JwtTokenResponse authenticate(JwtLoginRequest jwtRequest) {
        Authentication authentication =
                authenticationManager.authenticate(new JwtAuthentication(jwtRequest));
        Assert.isInstanceOf(JwtAuthentication.class, authentication,
                "Authentication object is not " + JwtAuthentication.class.getName());

        JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;

        return JwtTokenResponse.builder()
                .schema(SecurityConstant.AUTHORIZATION_SCHEMA)
                .token(jwtAuthentication.getJwt())
                .build();
    }

    @Override
    public WhoLeafsUserDetails getUser(String token) {
        return jwtUtil.extractUserDetails(token);
    }
}
