package com.demo.wholeafs.configs.security.utils;

import com.demo.wholeafs.configs.security.constants.SecurityConstant;
import com.demo.wholeafs.configs.security.model.Endpoint;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "wholeafs.security.permit")
@Slf4j
public class OpenEndpointConfigProps {

    @Getter
    @Setter
    private Map<String, Endpoint> endpoints;

    private static final AntPathRequestMatcher[] defaultPermittedMatchers = new AntPathRequestMatcher[]
            {
                    new AntPathRequestMatcher(SecurityConstant.AUTHENTICATION_ENDPOINT, HttpMethod.POST.toString()),
                    new AntPathRequestMatcher(SecurityConstant.USER_REGISTRATION_ENDPOINT, HttpMethod.POST.toString())
            };

    private AntPathRequestMatcher[] configuredEndpoints;


    public AntPathRequestMatcher[] getPermittedPathMatchers() {
        if (configuredEndpoints != null) {
            return configuredEndpoints;
        }
        Map<String, Endpoint> endpoints = this.getEndpoints();
        if (endpoints == null || endpoints.isEmpty()) {
            log.info("WhoLeafs security: Permit endpoint configurer - No permitted endpoint is configured by user");
            return defaultPermittedMatchers;
        }
        List<AntPathRequestMatcher> permittedEndpoints = new ArrayList<>();
        for (Map.Entry<String, Endpoint> entry : endpoints.entrySet()) {
            Endpoint endpoint = entry.getValue();
            if (this.isValidHttpMethod(endpoint)) {
                permittedEndpoints.add(new AntPathRequestMatcher(endpoint.getUrl(), endpoint.getMethod()));
                log.warn("WhoLeafs security: Permit endpoint configurer - Permitted method: {} in endpoint: {}",
                        endpoint.getMethod(), endpoint.getUrl());
            }
        }
        if (permittedEndpoints.isEmpty()) {
            return defaultPermittedMatchers;
        }
        this.configuredEndpoints = permittedEndpoints.toArray(new AntPathRequestMatcher[0]);
        return configuredEndpoints;
    }

    private boolean isValidHttpMethod(Endpoint endpoint) {
        try {
            HttpMethod.valueOf(endpoint.getMethod());
            return true;
        } catch (IllegalArgumentException e) {
            log.error("IECS Permit endpoint configurer: invalid http method: {} on endpoint {}",
                    endpoint.getMethod(), endpoint.getUrl());
            return false;
        }
    }
}
