package com.demo.wholeafs.configs.security.exception.handler;

import com.demo.wholeafs.rest.model.common.HttpErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class WhoLeafesAuthEntryPoint implements AuthenticationEntryPoint,
        AccessDeniedHandler {

    private final ObjectMapper objectMapper;

    public WhoLeafesAuthEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        log.error("[SECURITY ERROR]", e);
        HttpErrorResponse httpErrorResponse = new HttpErrorResponse();
        httpErrorResponse.setErrorCode(HttpServletResponse.SC_UNAUTHORIZED);
        httpErrorResponse.setPath(httpServletRequest.getRequestURI());
        httpErrorResponse.setMessage(e.getMessage());
        this.setResponse(httpServletResponse, httpErrorResponse);
    }

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException {
        log.error("[SECURITY ERROR]", e);
        HttpErrorResponse httpErrorResponse = new HttpErrorResponse();
        httpErrorResponse.setErrorCode(HttpServletResponse.SC_UNAUTHORIZED);
        httpErrorResponse.setPath(httpServletRequest.getRequestURI());
        httpErrorResponse.setMessage("Forbidden: " + e.getMessage());
        this.setResponse(httpServletResponse, httpErrorResponse);
    }

    private void setResponse(HttpServletResponse servletResponse, HttpErrorResponse errorResponse) throws IOException {
        servletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        servletResponse.setStatus(errorResponse.getErrorCode());
        this.objectMapper.writeValue(servletResponse.getWriter(), errorResponse);
    }
}
