package com.demo.wholeafs.configs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class WhoLeafsTokenException extends AuthenticationException {
    public WhoLeafsTokenException(String msg) {
        super(msg);
    }
}
