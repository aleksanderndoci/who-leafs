/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security;

import com.demo.wholeafs.configs.security.exception.WhoLeafsAuthenticationException;
import com.demo.wholeafs.configs.security.jwt.authentication.JwtAuthentication;
import com.demo.wholeafs.configs.security.jwt.model.JwtLoginRequest;
import com.demo.wholeafs.configs.security.jwt.util.JwtUtil;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class WhoLeafsAuthProvider implements AuthenticationProvider {

    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;
    private final UserDetailsService whoLeafsUserDetailsService;

    public WhoLeafsAuthProvider(PasswordEncoder passwordEncoder,
                                JwtUtil jwtUtil,
                                UserDetailsService whoLeafsUserDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.jwtUtil = jwtUtil;
        this.whoLeafsUserDetailsService = whoLeafsUserDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        Assert.isInstanceOf(JwtAuthentication.class, authentication,
                "Authentication method unknown.");
        JwtAuthentication whoLeafsJwtAuthentication = (JwtAuthentication) authentication;
        if (whoLeafsJwtAuthentication.isAuthenticated()) {
            return authentication;
        }
        JwtLoginRequest jwtLoginRequest = whoLeafsJwtAuthentication.getJwtRequest();
        return this.getNewAuthentication(jwtLoginRequest);
    }

    private JwtAuthentication getNewAuthentication(JwtLoginRequest jwtRequest) {
        WhoLeafsUserDetails whoLeafsUserDetails = this.getUserDetails(jwtRequest.getUsername());
        if (whoLeafsUserDetails == null
                || whoLeafsUserDetails.getUsername() == null || whoLeafsUserDetails.getPassword() == null) {
            throw new WhoLeafsAuthenticationException("Invalid user details.");
        }
        this.validateUserDetails(jwtRequest.getPassword(), whoLeafsUserDetails);
        String accessToken = jwtUtil.generateToken(whoLeafsUserDetails);
        return new JwtAuthentication(whoLeafsUserDetails, accessToken);
    }


    private WhoLeafsUserDetails getUserDetails(String userName) {
        try {
            UserDetails userDetails = whoLeafsUserDetailsService.loadUserByUsername(userName);
            Assert.isInstanceOf(WhoLeafsUserDetails.class, userDetails,
                    "Unknown user details provided");
            return (WhoLeafsUserDetails) userDetails;
        } catch (UsernameNotFoundException e) {
            throw new WhoLeafsAuthenticationException("Authentication error. " + e.getMessage(), e);
        }
    }

    private void validateUserDetails(String rawPassword, WhoLeafsUserDetails userDetails) {
        if (!passwordEncoder.matches(rawPassword, userDetails.getPassword())) {
            throw new WhoLeafsAuthenticationException("Bad credentials for user: " + userDetails.getUsername());
        }
        if (userDetails.isAccountLocked()) {
            throw new WhoLeafsAuthenticationException("User has not passed the probation period(90 days)");
        }
        if (!userDetails.isEnabled()) {
            throw new WhoLeafsAuthenticationException("Please confirm your account (Check your email)");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return JwtAuthentication.class.isAssignableFrom(aClass);
    }
}
