/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.model;

import lombok.Data;

@Data
public class Endpoint {
    private String method;
    private String url;
}
