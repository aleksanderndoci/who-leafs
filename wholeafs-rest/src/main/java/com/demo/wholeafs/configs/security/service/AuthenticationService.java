package com.demo.wholeafs.configs.security.service;

import com.demo.wholeafs.configs.security.jwt.model.JwtLoginRequest;
import com.demo.wholeafs.configs.security.jwt.model.JwtTokenResponse;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;

public interface AuthenticationService {
    JwtTokenResponse authenticate(JwtLoginRequest jwtRequest);

    WhoLeafsUserDetails getUser(String token);
}
