package com.demo.wholeafs.configs.security.jwt.authentication;

import com.demo.wholeafs.configs.security.jwt.model.JwtLoginRequest;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class JwtAuthentication extends AbstractAuthenticationToken {

    private String jwt;

    private WhoLeafsUserDetails userDetails;

    private JwtLoginRequest jwtLoginRequest;

    public JwtAuthentication(JwtLoginRequest jwtLoginRequest) {
        super(null);
        this.jwtLoginRequest = jwtLoginRequest;
        this.setAuthenticated(false);
    }

    public JwtAuthentication(WhoLeafsUserDetails whoLeafsUserDetails, String accessToken) {
        super(whoLeafsUserDetails.getAuthorities());
        this.userDetails = whoLeafsUserDetails;
        this.setAuthenticated(true);
        this.setDetails(whoLeafsUserDetails);
        this.jwt = accessToken;
    }

    public JwtAuthentication(WhoLeafsUserDetails whoLeafsUserDetails) {
        super(whoLeafsUserDetails.getAuthorities());
        this.userDetails = whoLeafsUserDetails;
        this.setAuthenticated(true);
        this.setDetails(whoLeafsUserDetails);
    }


    @Override
    public Object getCredentials() {
        return jwt;
    }

    @Override
    public WhoLeafsUserDetails getPrincipal() {
        return userDetails;
    }

    public JwtLoginRequest getJwtRequest() {
        return jwtLoginRequest;
    }

    public String getJwt() {
        return jwt;
    }
}
