/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class GeneralConfig {
}
