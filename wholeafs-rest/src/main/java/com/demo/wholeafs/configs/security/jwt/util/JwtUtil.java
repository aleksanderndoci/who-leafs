package com.demo.wholeafs.configs.security.jwt.util;

import com.demo.wholeafs.configs.security.constants.SecurityConstant.JwtClaims;
import com.demo.wholeafs.configs.security.exception.WhoLeafsAuthenticationException;
import com.demo.wholeafs.configs.security.user.WhoLeafsUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {
    @Value("${wholeafes.security.jwt.secret:whoLeafesSecretKey}")
    private String jwtSecret;

    @Value("${wholeafes.security.jwt.tokenValidity:36000000}")
    private Long tokenValidity;

    private Claims extractAlClaims(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateToken(WhoLeafsUserDetails whoLeafsUserDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaims.NAME_, whoLeafsUserDetails.getName());
        claims.put(JwtClaims.SURNAME_, whoLeafsUserDetails.getSurname());
        claims.put(JwtClaims.EMAIL_, whoLeafsUserDetails.getEmail());
        claims.put(JwtClaims.ROLE_, whoLeafsUserDetails.getRole());
        claims.put(JwtClaims.USER_ID_, whoLeafsUserDetails.getId());
        return createToken(claims, whoLeafsUserDetails.getUsername());
    }


    private String createToken(Map<String, Object> claims, String subject) {
        long currentTimeMillis = System.currentTimeMillis();
        return Jwts.builder().setClaims(claims).setSubject(subject)
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + tokenValidity))
                .signWith(SignatureAlgorithm.HS256, jwtSecret)
                .compact();
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAlClaims(token);
        return claimsResolver.apply(claims);
    }

    public WhoLeafsUserDetails extractUserDetails(String token) {
        if (!this.validateToken(token)) {
            throw new WhoLeafsAuthenticationException("Token expired");
        }
        Claims claims = this.extractAlClaims(token);
        String username = claims.getSubject();
        String email = claims.get(JwtClaims.EMAIL_, String.class);
        String role = claims.get(JwtClaims.ROLE_, String.class);
        String name = claims.get(JwtClaims.NAME_, String.class);
        String surname = claims.get(JwtClaims.SURNAME_, String.class);
        Integer id = claims.get(JwtClaims.USER_ID_, Integer.class);
        return WhoLeafsUserDetails.builder()
                .id(id)
                .name(name)
                .surname(surname)
                .username(username)
                .email(email)
                .role(role)
                .build();
    }

    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date(System.currentTimeMillis()));
    }
}
