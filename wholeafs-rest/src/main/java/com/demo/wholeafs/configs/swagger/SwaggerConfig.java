package com.demo.wholeafs.configs.swagger;

import com.demo.wholeafs.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.withClassAnnotation;

@Configuration
public class SwaggerConfig {

    private static final String AUTH_METHOD = "JWT Access Token";
    private static final String API_KEY_NAME = "Authorization";
    private static final String API_KEY_PASS_AS = "header";

    private RoleService roleService;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Value("${wholeafs.api.version:1.0.0-SNAPSHOT}")
    private String apiVersion;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(this.apiInfo())
                .securityContexts(this.securityContext())
                .securitySchemes(this.securityScheme())
                .select()
                .apis(withClassAnnotation(Documented.class))
                .paths(any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("WhoLeafs -Rest API")
                .version(apiVersion)
                .description("WhoLeafs - Annual Leave Web Application. Developed by:")
                .contact(new Contact("Aleksander Ndoci",
                        "https://github.com/aleksanderndoci",
                        "ndocialex@gmail.com")
                ).build();
    }

    private List<SecurityContext> securityContext() {
        SecurityContext securityContext = SecurityContext.builder()
                .securityReferences(Collections.singletonList(
                        SecurityReference.builder()
                                .reference(AUTH_METHOD)
                                .scopes(scopes())
                                .build()))
                .build();
        return Collections.singletonList(securityContext);
    }

    private List<SecurityScheme> securityScheme() {
        return Collections.singletonList(new ApiKey(AUTH_METHOD, API_KEY_NAME, API_KEY_PASS_AS));
    }

    private AuthorizationScope[] scopes() {
        return roleService.findAll()
                .stream().map(a -> new AuthorizationScope(a.getRole().name(), a.getRole().name()))
                .toArray(AuthorizationScope[]::new);
    }
}
