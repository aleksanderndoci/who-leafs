/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.jwt.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JwtTokenResponse {
    private String schema;
    private String token;
}
