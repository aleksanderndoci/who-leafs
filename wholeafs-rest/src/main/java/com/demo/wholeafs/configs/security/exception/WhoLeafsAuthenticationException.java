/*
 * Copyright (c) Aleksander Ndoci
 */

package com.demo.wholeafs.configs.security.exception;

import org.springframework.security.core.AuthenticationException;

public class WhoLeafsAuthenticationException extends AuthenticationException {

    public WhoLeafsAuthenticationException(String s) {
        super(s);
    }

    public WhoLeafsAuthenticationException(String s, Throwable e) {
        super(s, e);
    }
}
