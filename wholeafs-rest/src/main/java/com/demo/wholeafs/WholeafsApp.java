package com.demo.wholeafs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WholeafsApp {
    public static void main(String[] args) {
        SpringApplication.run(WholeafsApp.class);
    }
}
